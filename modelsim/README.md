
#Respecto de las versiones de los proyectos

El proyecto en la carpeta Modelsim PE es para utilizar con la versión Modelsim StudentEdition que se puede obtener directametne del sitio de Mentor:

https://www.mentor.com/company/higher_ed/modelsim-student-edition

El proyecto en la carpeta Modelsim Altera es para utilizar con la versión de Modelsim Starter Edition 10.0c que se puede obtener a través de la página de Altera:

https://www.altera.com/downloads/software/archives/arc-index.html
https://www.altera.com/downloads/software/modelsim-starter/111.html