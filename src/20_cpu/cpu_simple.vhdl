--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingeniería, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * Año 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_CPU_SIMPLE is
	port(
		-- Señales de control
		CLOCK                   : in  STD_LOGIC; -- Activo en flanco ascendente.
		ASYNC_RESET             : in  STD_LOGIC; -- Activo en nivel alto.

		-- Acceso a la memoria de programa
		PROG_MEM_ADDR_BUS       : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones de acceso a la RAM
		PROG_MEM_INSTR_BUS      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos de acceso a la RAM, para leer la instrucción siguiente. 

		-- Acceso de lectura a la memoria de datos
		DATA_MEM_READ_ADDR_BUS  : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones LOADs desde RAM.
		DATA_MEM_READ_DATA_BUS  : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para LOADs desde RAM.

		-- Acceso de escritura a la memoria de datos
		DATA_MEM_WRITE_ADDR_BUS : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones para STOREs a la RAM.
		DATA_MEM_WRITE_DATA_BUS : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para STOREs a la RAM. 
		DATA_MEM_WRITE_ENABLE   : out STD_LOGIC; -- Señal de habilitación de escritura para STOREs a la RAM.

		-- Señales para monitoreo de la ejecución
		MONITOR_CLOCK_BEATS     : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		MONITOR_PC_VALUE        : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		MONITOR_INSTR           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

		-- Puertos de monitoreo de estado de los primeros 8 registros
		MONITOR_RB_R0           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R1           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R2           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R3           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R4           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R5           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R6           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R7           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
	);
end entity E_CPU_SIMPLE;

-- --------------------------------
-- Definición de la Arquitectura --
-- --------------------------------

architecture A_CPU_SIMPLE of E_CPU_SIMPLE is
	for all : C_PROGRAM_COUNTER use entity E_PROGRAM_COUNTER;
	for all : C_COUNTER use entity E_COUNTER;

	for all : C_STAGE_FETCH use entity E_STAGE_FETCH;
	for all : C_STAGE_DECODE use entity E_STAGE_DECODE;
	for all : C_STAGE_EXECUTE use entity E_STAGE_EXECUTE;
	for all : C_STAGE_WRITEBACK use entity E_STAGE_WRITEBACK;

	for all : C_REGISTER_BANK_2R_1W use entity E_REGISTER_BANK_2R_1W;

	signal REGBANK_PORT_RA_SELECTOR    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal REGBANK_PORT_RA_DATA        : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal REGBANK_PORT_RB_SELECTOR    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal REGBANK_PORT_RB_DATA        : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal REGBANK_PORT_WA_WRITEENABLE : STD_LOGIC;
	signal REGBANK_PORT_WA_SELECTOR    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal REGBANK_PORT_WA_DATA        : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	--------------------

	signal PC_CLOCK_BEATS    : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
	signal PC_PC_VALUE       : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal PC_SYNC_SET_VALUE : STD_LOGIC;
	signal PC_NEW_VALUE      : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

	--------------------

	signal STFE_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
	signal STFE_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal STFE_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	--------------------

	signal STDE_CLOCK_BEATS            : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal STDE_PC_VALUE               : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal STDE_INSTR                  : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.
	signal STDE_OP_IS_ALU_RR           : STD_LOGIC; -- La clase de instrucción es ALU registro-registro
	signal STDE_OP_IS_ALU_RI           : STD_LOGIC; -- La clase de instrucción es ALU registro-inmediato
	signal STDE_OP_IS_LOAD_STORE       : STD_LOGIC; -- La clase de instrucción es LOAD o es STORE
	signal STDE_OP_IS_LOAD             : STD_LOGIC; -- La clase de instrucción es LOAD.
	signal STDE_OP_IS_STORE            : STD_LOGIC; -- La clase de instrucción es STORE.
	signal STDE_OP_IS_CONDITIONAL_JUMP : STD_LOGIC; -- La clase de instrucción es JUMP.
	signal STDE_ALU_OPERATION_CODE     : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del código de operación ALU (si corresponde).
	signal STDE_JUMP_CONDITION_CODE    : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del código de condición de salto condicional (si corresponde).
	signal STDE_OP_DESTINATION_REG_ID  : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal STDE_OP_OPERAND_1_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal STDE_OP_OPERAND_2_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
	signal STDE_OP_OPERAND_1_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
	signal STDE_OP_OPERAND_2_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros.
	signal STDE_OP_IMM_VALUE           : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucción).

	--------------------

	signal STEX_CLOCK_BEATS             : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal STEX_PC_VALUE                : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal STEX_INSTR                   : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.
	signal STEX_OP_IS_ALU_RR            : STD_LOGIC; -- La clase de instrucción es ALU registro-registro
	signal STEX_OP_IS_ALU_RI            : STD_LOGIC; -- La clase de instrucción es ALU registro-inmediato
	signal STEX_OP_IS_LOAD_STORE        : STD_LOGIC; -- La clase de instrucción es LOAD o es STORE
	signal STEX_OP_IS_LOAD              : STD_LOGIC; -- La clase de instrucción es LOAD.
	signal STEX_OP_IS_STORE             : STD_LOGIC; -- La clase de instrucción es STORE.
	signal STEX_OP_IS_CONDITIONAL_JUMP  : STD_LOGIC; -- La clase de instrucción es JUMP.
	signal STEX_OP_DESTINATION_REG_ID   : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal STEX_OP_OPERAND_1_REG_ID     : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal STEX_OP_OPERAND_2_REG_ID     : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
	signal STEX_ALU_RESULT              : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal STEX_BRANCH_TAKEN            : STD_LOGIC;
	signal STEX_BRANCH_DESTINATION_ADDR : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal STEX_OP_OPERAND_1_VALUE      : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.

	--------------------

	signal STWR_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal STWR_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal STWR_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.


begin

	-- Control del contador de programa desde la unidad de evaluación de saltos condicionales. 
	PC_SYNC_SET_VALUE <= STEX_BRANCH_TAKEN;
	PC_NEW_VALUE      <= STEX_BRANCH_DESTINATION_ADDR;

	rRegisterBank : C_REGISTER_BANK_2R_1W
		generic map(ASYNC_VALUE_UPDATE => false)
		port map(CLOCK               => CLOCK,
			     RESET               => ASYNC_RESET,
			     PORT_RA_SELECTOR    => REGBANK_PORT_RA_SELECTOR,
			     PORT_RA_DATA        => REGBANK_PORT_RA_DATA,
			     PORT_RB_SELECTOR    => REGBANK_PORT_RB_SELECTOR,
			     PORT_RB_DATA        => REGBANK_PORT_RB_DATA,
			     PORT_WA_WRITEENABLE => REGBANK_PORT_WA_WRITEENABLE,
			     PORT_WA_SELECTOR    => REGBANK_PORT_WA_SELECTOR,
			     PORT_WA_DATA        => REGBANK_PORT_WA_DATA,
			     -- Puertos de monitoreo de estado de los primeros 8 registros.
			     -- Estos se acarrean a la interfaz del CPU para que sean vistos desde afuera. 
			     MONITOR_RB_R0       => MONITOR_RB_R0,
			     MONITOR_RB_R1       => MONITOR_RB_R1,
			     MONITOR_RB_R2       => MONITOR_RB_R2,
			     MONITOR_RB_R3       => MONITOR_RB_R3,
			     MONITOR_RB_R4       => MONITOR_RB_R4,
			     MONITOR_RB_R5       => MONITOR_RB_R5,
			     MONITOR_RB_R6       => MONITOR_RB_R6,
			     MONITOR_RB_R7       => MONITOR_RB_R7
		);

	rProgramCounter : C_PROGRAM_COUNTER port map(
			PC_VALUE       => PC_PC_VALUE,
			NEW_VALUE      => PC_NEW_VALUE,
			SYNC_SET_VALUE => PC_SYNC_SET_VALUE,
			SYNC_INHIBIT   => '0',
			ASYNC_RESET    => ASYNC_RESET,
			CLOCK          => CLOCK
		);

	rBeatsCounter : C_COUNTER port map(
			VALUE       => PC_CLOCK_BEATS,
			ASYNC_RESET => ASYNC_RESET,
			CLOCK       => CLOCK
		);

	rStageFetch : C_STAGE_FETCH port map(
			SP_PROG_MEM_ADDR  => PROG_MEM_ADDR_BUS,
			SP_PROG_MEM_INSTR => PROG_MEM_INSTR_BUS,
			--
			BP_CLOCK_BEATS    => PC_CLOCK_BEATS,
			BP_PC_VALUE       => PC_PC_VALUE,
			--
			FP_CLOCK_BEATS    => STFE_CLOCK_BEATS,
			FP_PC_VALUE       => STFE_PC_VALUE,
			FP_INSTR          => STFE_INSTR
		);

	rStageDecode : C_STAGE_DECODE port map(
			SP_REGISTER_RS_SELECTOR   => REGBANK_PORT_RA_SELECTOR,
			SP_REGISTER_RT_SELECTOR   => REGBANK_PORT_RB_SELECTOR,
			SP_REGISTER_RS_VALUE      => REGBANK_PORT_RA_DATA,
			SP_REGISTER_RT_VALUE      => REGBANK_PORT_RB_DATA,
			--
			BP_CLOCK_BEATS            => STFE_CLOCK_BEATS,
			BP_PC_VALUE               => STFE_PC_VALUE,
			BP_INSTR                  => STFE_INSTR,
			--
			FP_CLOCK_BEATS            => STDE_CLOCK_BEATS,
			FP_PC_VALUE               => STDE_PC_VALUE,
			FP_INSTR                  => STDE_INSTR,
			FP_OP_IS_ALU_RR           => STDE_OP_IS_ALU_RR,
			FP_OP_IS_ALU_RI           => STDE_OP_IS_ALU_RI,
			FP_OP_IS_LOAD_STORE       => STDE_OP_IS_LOAD_STORE,
			FP_OP_IS_LOAD             => STDE_OP_IS_LOAD,
			FP_OP_IS_STORE            => STDE_OP_IS_STORE,
			FP_OP_IS_CONDITIONAL_JUMP => STDE_OP_IS_CONDITIONAL_JUMP,
			FP_ALU_OPERATION_CODE     => STDE_ALU_OPERATION_CODE,
			FP_JUMP_CONDITION_CODE    => STDE_JUMP_CONDITION_CODE,
			FP_OP_DESTINATION_REG_ID  => STDE_OP_DESTINATION_REG_ID,
			FP_OP_OPERAND_1_REG_ID    => STDE_OP_OPERAND_1_REG_ID,
			FP_OP_OPERAND_2_REG_ID    => STDE_OP_OPERAND_2_REG_ID,
			FP_OP_OPERAND_1_VALUE     => STDE_OP_OPERAND_1_VALUE,
			FP_OP_OPERAND_2_VALUE     => STDE_OP_OPERAND_2_VALUE,
			FP_OP_IMM_VALUE           => STDE_OP_IMM_VALUE
		);

	rStageExecute : C_STAGE_EXECUTE port map(
			BP_CLOCK_BEATS                => STDE_CLOCK_BEATS,
			BP_PC_VALUE                   => STDE_PC_VALUE,
			BP_INSTR                      => STDE_INSTR,
			BP_OP_IS_ALU_RR               => STDE_OP_IS_ALU_RR,
			BP_OP_IS_ALU_RI               => STDE_OP_IS_ALU_RI,
			BP_OP_IS_LOAD_STORE           => STDE_OP_IS_LOAD_STORE,
			BP_OP_IS_LOAD                 => STDE_OP_IS_LOAD,
			BP_OP_IS_STORE                => STDE_OP_IS_STORE,
			BP_OP_IS_CONDITIONAL_JUMP     => STDE_OP_IS_CONDITIONAL_JUMP,
			BP_ALU_OPERATION_CODE         => STDE_ALU_OPERATION_CODE,
			BP_JUMP_CONDITION_CODE        => STDE_JUMP_CONDITION_CODE,
			BP_OP_DESTINATION_REG_ID      => STDE_OP_DESTINATION_REG_ID,
			BP_OP_OPERAND_1_REG_ID        => STDE_OP_OPERAND_1_REG_ID,
			BP_OP_OPERAND_2_REG_ID        => STDE_OP_OPERAND_2_REG_ID,
			BP_OP_OPERAND_1_VALUE         => STDE_OP_OPERAND_1_VALUE,
			BP_OP_OPERAND_2_VALUE         => STDE_OP_OPERAND_2_VALUE,
			BP_OP_IMM_VALUE               => STDE_OP_IMM_VALUE,
			--
			FP_CLOCK_BEATS                => STEX_CLOCK_BEATS,
			FP_PC_VALUE                   => STEX_PC_VALUE,
			FP_INSTR                      => STEX_INSTR,
			FP_OP_IS_ALU_RR               => STEX_OP_IS_ALU_RR,
			FP_OP_IS_ALU_RI               => STEX_OP_IS_ALU_RI,
			FP_OP_IS_LOAD_STORE           => STEX_OP_IS_LOAD_STORE,
			FP_OP_IS_LOAD                 => STEX_OP_IS_LOAD,
			FP_OP_IS_STORE                => STEX_OP_IS_STORE,
			FP_OP_IS_CONDITIONAL_JUMP     => STEX_OP_IS_CONDITIONAL_JUMP,
			FP_OP_DESTINATION_REG_ID      => STEX_OP_DESTINATION_REG_ID,
			FP_OP_OPERAND_1_REG_ID        => STEX_OP_OPERAND_1_REG_ID,
			FP_OP_OPERAND_2_REG_ID        => STEX_OP_OPERAND_2_REG_ID,
			FP_OP_OPERAND_1_VALUE         => STEX_OP_OPERAND_1_VALUE,
			FP_ALU_RESULT                 => STEX_ALU_RESULT,
			FP_BRANCH_TAKEN               => STEX_BRANCH_TAKEN,
			FP_OP_BRANCH_DESTINATION_ADDR => STEX_BRANCH_DESTINATION_ADDR
		);

	rStageWriteback : C_STAGE_WRITEBACK port map(
			SP_RAM_READ_ADDR_BUS       => DATA_MEM_READ_ADDR_BUS,
			SP_RAM_READ_DATA_BUS       => DATA_MEM_READ_DATA_BUS,
			SP_RAM_WRITE_ADDR_BUS      => DATA_MEM_WRITE_ADDR_BUS,
			SP_RAM_WRITE_DATA_BUS      => DATA_MEM_WRITE_DATA_BUS,
			SP_RAM_WRITE_ENABLE        => DATA_MEM_WRITE_ENABLE,
			SP_REGISTER_WRITE_SELECTOR => REGBANK_PORT_WA_SELECTOR,
			SP_REGISTER_WRITE_DATA     => REGBANK_PORT_WA_DATA,
			SP_REGISTER_WRITE_ENABLE   => REGBANK_PORT_WA_WRITEENABLE,
			--
			BP_CLOCK_BEATS             => STEX_CLOCK_BEATS,
			BP_PC_VALUE                => STEX_PC_VALUE,
			BP_INSTR                   => STEX_INSTR,
			BP_OP_IS_ALU_RR            => STEX_OP_IS_ALU_RR,
			BP_OP_IS_ALU_RI            => STEX_OP_IS_ALU_RI,
			BP_OP_IS_LOAD_STORE        => STEX_OP_IS_LOAD_STORE,
			BP_OP_IS_LOAD              => STEX_OP_IS_LOAD,
			BP_OP_IS_STORE             => STEX_OP_IS_STORE,
			BP_OP_IS_CONDITIONAL_JUMP  => STEX_OP_IS_CONDITIONAL_JUMP,
			BP_OP_DESTINATION_REG_ID   => STEX_OP_DESTINATION_REG_ID,
			BP_OP_OPERAND_1_REG_ID     => STEX_OP_OPERAND_1_REG_ID,
			BP_OP_OPERAND_2_REG_ID     => STEX_OP_OPERAND_2_REG_ID,
			BP_OP_OPERAND_1_VALUE      => STEX_OP_OPERAND_1_VALUE,
			BP_ALU_RESULT              => STEX_ALU_RESULT,
			BP_BRANCH_TAKEN            => STEX_BRANCH_TAKEN,
			--
			FP_CLOCK_BEATS             => STWR_CLOCK_BEATS,
			FP_PC_VALUE                => STWR_PC_VALUE,
			FP_INSTR                   => STWR_INSTR
		);

	-- Conecto las señales de monitoreo externo
	MONITOR_CLOCK_BEATS <= STWR_CLOCK_BEATS;
	MONITOR_PC_VALUE    <= STWR_PC_VALUE;
	MONITOR_INSTR       <= STWR_INSTR;

end architecture A_CPU_SIMPLE;
