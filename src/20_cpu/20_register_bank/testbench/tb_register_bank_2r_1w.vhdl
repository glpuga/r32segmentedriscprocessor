--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------
-- Definici�n de la Entidad      --
-- --------------------------------

entity TB_E_REGISTERBANK2R1W is
-- body
end entity TB_E_REGISTERBANK2R1W;

-- --------------------------------
-- Definici�n de la Arquitectura --
-- --------------------------------

architecture TB_A_REGISTERBANK2R1W of TB_E_REGISTERBANK2R1W is
	for all : C_REGISTER_BANK_2R_1W use entity E_REGISTER_BANK_2R_1W;

	-- Se�ales locales

	constant WORD_WIDTH     : integer := 4;
	constant SELECTOR_WIDTH : integer := 3;

	signal CLOCK               : STD_LOGIC;
	signal RESET               : STD_LOGIC;
	signal PORT_RA_SELECTOR    : STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
	signal PORT_RB_SELECTOR    : STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
	signal PORT_WA_WRITEENABLE : STD_LOGIC;
	signal PORT_WA_SELECTOR    : STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
	signal PORT_WA_DATA        : STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);

begin
	CLOCK <= '0',                       --
		'1' after 0.5 us,               --
		'0' after 1.0 us,               --
		'1' after 1.5 us,               --
		'0' after 2.0 us,               --
		'1' after 2.5 us,               --
		'0' after 3.0 us,               --
		'1' after 3.5 us,               --
		'0' after 4.0 us,               --
		'1' after 4.5 us,               --
		'0' after 5.0 us,               --
		'1' after 5.5 us,               --
		'0' after 6.0 us,               --
		'1' after 6.5 us,               --
		'0' after 7.0 us,               --
		'1' after 7.5 us,               --
		'0' after 10 us;                --

	RESET <= '0',                       --
		'1' after 8.5 us,               --
		'0' after 8.6 us;               --

	PORT_RA_SELECTOR <= "000",          --
		"001" after 1.25 us,            --
		"010" after 2.25 us,            --
		"011" after 3.25 us,            --
		"100" after 4.25 us,            --
		"110" after 5.25 us,            --
		"111" after 6.25 us;            --

	PORT_RB_SELECTOR <= "011";

	PORT_WA_SELECTOR <= "000",          --
		"001" after 1.5 us,             --
		"010" after 2.5 us,             --
		"011" after 3.5 us,             --
		"100" after 4.5 us,             --
		"110" after 5.5 us,             --
		"111" after 6.5 us;             --	

	-- Puerto de escritura
	PORT_WA_WRITEENABLE <= '1' after 0.4 us, --
		'0' after 1.4 us,               --
		'1' after 2.4 us,               --
		'0' after 3.4 us,               --
		'1' after 4.4 us,               --
		'0' after 5.4 us,               --
		'1' after 6.4 us,               --
		'0' after 7.4 us,               --
		'1' after 8.4 us,               --
		'0' after 9.4 us;               --

	PORT_WA_DATA <= "0000",             -- 
		"0001" after 1.25 us,           --
		"0010" after 2.25 us,           --
		"0011" after 3.25 us,           --
		"0100" after 4.25 us,           --
		"0110" after 5.25 us,           --
		"0111" after 6.25 us,           --
		"1000" after 7.25 us,           --
		"1001" after 8.25 us,           --
		"1010" after 9.25 us;           --

	m1 : C_REGISTER_BANK_2R_1W
		generic map(
			ASYNC_VALUE_UPDATE => true,
			WORD_WIDTH         => WORD_WIDTH,
			SELECTOR_WIDTH     => SELECTOR_WIDTH
		)
		port map(
			CLOCK               => CLOCK,
			RESET               => RESET,
			PORT_RA_SELECTOR    => PORT_RA_SELECTOR,
			PORT_RA_DATA        => open,
			PORT_RB_SELECTOR    => PORT_RB_SELECTOR,
			PORT_RB_DATA        => open,
			PORT_WA_WRITEENABLE => PORT_WA_WRITEENABLE,
			PORT_WA_SELECTOR    => PORT_WA_SELECTOR,
			PORT_WA_DATA        => PORT_WA_DATA
		);

end architecture TB_A_REGISTERBANK2R1W;
