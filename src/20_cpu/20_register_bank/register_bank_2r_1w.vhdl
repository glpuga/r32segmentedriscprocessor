--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingeniería, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * Año 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_REGISTER_BANK_2R_1W is
	generic(
		ASYNC_VALUE_UPDATE : boolean;   -- Debe ser true para el procesador segmentado, y falso para el no segmentado. 
		WORD_WIDTH         : integer := 32;
		SELECTOR_WIDTH     : integer := 5;
		DELAY              : time    := TIME_DELAY_MEMORY
	);
	port(
		-- Entradas de control
		CLOCK               : in  STD_LOGIC; -- Activo en flanco ascendente.
		RESET               : in  STD_LOGIC; -- Activo en nivel alto.
		-- Puerto de A de lectura
		PORT_RA_SELECTOR    : in  STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
		PORT_RA_DATA        : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		-- Puerto B de lectura
		PORT_RB_SELECTOR    : in  STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
		PORT_RB_DATA        : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		-- Puerto de escritura
		PORT_WA_WRITEENABLE : in  STD_LOGIC;
		PORT_WA_SELECTOR    : in  STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
		PORT_WA_DATA        : in  STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);

		-- Puertos de monitoreo de estado de los primeros 8 registros
		MONITOR_RB_R0       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_RB_R1       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_RB_R2       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_RB_R3       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_RB_R4       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_RB_R5       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_RB_R6       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_RB_R7       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0)
	);
end entity E_REGISTER_BANK_2R_1W;

-- --------------------------------
-- Definición de la Arquitectura --
-- --------------------------------

architecture A_REGISTER_BANK_2R_1W of E_REGISTER_BANK_2R_1W is
	constant RegisterCount : integer                                     := (2 ** SELECTOR_WIDTH);
	constant RegisterZero  : STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0) := (others => '0');

	type MEMORY_TYPE is array ((RegisterCount - 1) downto 0) of STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);

begin
	process(CLOCK, RESET, PORT_RA_SELECTOR, PORT_RB_SELECTOR, PORT_WA_DATA, PORT_WA_SELECTOR, PORT_WA_WRITEENABLE)

		-- Creo el banco de registro pre-inicializado
		variable register_bank : MEMORY_TYPE := (others => RegisterZero);

	begin
		if (RESET = '1') then

			-- Pongo todos los registros a cero
			for regindex in (RegisterCount - 1) downto 0 loop
				register_bank(regIndex) := std_logic_vector(to_unsigned(0, WORD_WIDTH));
			end loop;

		elsif (CLOCK'event and CLOCK = '1') then

			-- Si hay un flanco de reloj, verifico si hay que realizar una escritura.
			if (PORT_WA_WRITEENABLE = '1') then
				register_bank(to_integer(unsigned(PORT_WA_SELECTOR))) := PORT_WA_DATA;
			end if;

		end if;

		-- Independientemente de todo, actualizo los puertos de lectura (estos puertos
		-- funcionan de forma asincrónica). Los if son necesarios para manejar 
		-- correctamente el caso donde se realiza una escritura y una lectura en un 
		-- mismo registro dentro de un mismo ciclo de reloj. 

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_RA_SELECTOR = PORT_WA_SELECTOR) then
			PORT_RA_DATA <= PORT_WA_DATA after DELAY;
		else
			PORT_RA_DATA <= register_bank(to_integer(unsigned(PORT_RA_SELECTOR))) after DELAY;
		end if;

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_RB_SELECTOR = PORT_WA_SELECTOR) then
			PORT_RB_DATA <= PORT_WA_DATA after DELAY;
		else
			PORT_RB_DATA <= register_bank(to_integer(unsigned(PORT_RB_SELECTOR))) after DELAY;
		end if;

		-- --
		--
		-- Señales de monitoreo para ver el contenido de los primeros 8 registros
		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_WA_SELECTOR = std_logic_vector(to_unsigned(0, REG_SEL_SIZE))) then
			MONITOR_RB_R0 <= PORT_WA_DATA;
		else
			MONITOR_RB_R0 <= register_bank(0);
		end if;

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_WA_SELECTOR = std_logic_vector(to_unsigned(1, REG_SEL_SIZE))) then
			MONITOR_RB_R1 <= PORT_WA_DATA;
		else
			MONITOR_RB_R1 <= register_bank(1);
		end if;

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_WA_SELECTOR = std_logic_vector(to_unsigned(2, REG_SEL_SIZE))) then
			MONITOR_RB_R2 <= PORT_WA_DATA;
		else
			MONITOR_RB_R2 <= register_bank(2);
		end if;

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_WA_SELECTOR = std_logic_vector(to_unsigned(3, REG_SEL_SIZE))) then
			MONITOR_RB_R3 <= PORT_WA_DATA;
		else
			MONITOR_RB_R3 <= register_bank(3);
		end if;

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_WA_SELECTOR = std_logic_vector(to_unsigned(4, REG_SEL_SIZE))) then
			MONITOR_RB_R4 <= PORT_WA_DATA;
		else
			MONITOR_RB_R4 <= register_bank(4);
		end if;

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_WA_SELECTOR = std_logic_vector(to_unsigned(5, REG_SEL_SIZE))) then
			MONITOR_RB_R5 <= PORT_WA_DATA;
		else
			MONITOR_RB_R5 <= register_bank(5);
		end if;

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_WA_SELECTOR = std_logic_vector(to_unsigned(6, REG_SEL_SIZE))) then
			MONITOR_RB_R6 <= PORT_WA_DATA;
		else
			MONITOR_RB_R6 <= register_bank(6);
		end if;

		if (ASYNC_VALUE_UPDATE = true) and (PORT_WA_WRITEENABLE = '1') and (PORT_WA_SELECTOR = std_logic_vector(to_unsigned(7, REG_SEL_SIZE))) then
			MONITOR_RB_R7 <= PORT_WA_DATA;
		else
			MONITOR_RB_R7 <= register_bank(7);
		end if;

	end process;

end architecture A_REGISTER_BANK_2R_1W;
