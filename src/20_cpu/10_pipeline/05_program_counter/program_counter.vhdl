--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_PROGRAM_COUNTER is
	generic(
		COUNTER_WIDTH     : integer := ADDR_BUS_SIZE;
		COUNTER_INCREMENT : integer := PC_INCREMENT;
		DELAY             : time    := TIME_DELAY_PROGRAM_COUNTER
	);
	port(
		-- Puertos sincr�nicos
		PC_VALUE       : out STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0);
		NEW_VALUE      : in  STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0);
		SYNC_SET_VALUE : in  STD_LOGIC; -- Establece el valor del contador en el valor de VALUE.
		SYNC_INHIBIT   : in  STD_LOGIC; -- Inhibidor de cuenta, activo en alto.
		-- Puertos asincr�nicos
		ASYNC_RESET    : in  STD_LOGIC; -- Activo en alto
		-- Reloj
		CLOCK          : in  STD_LOGIC
	);
end entity E_PROGRAM_COUNTER;

-- --------------------------------

architecture A_PROGRAM_COUNTER of E_PROGRAM_COUNTER is
begin
	process(CLOCK, ASYNC_RESET)
		variable internalCounter : STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0) := (others => '0');
		variable selector        : STD_LOGIC_VECTOR(1 downto 0);

	begin
		if (ASYNC_RESET = '1') then
			internalCounter := (others => '0');

		elsif (CLOCK'event and CLOCK = '1') then
			selector := SYNC_INHIBIT & SYNC_SET_VALUE;
			case selector is
				when "00"   => internalCounter := std_logic_vector(unsigned(internalCounter) + to_unsigned(COUNTER_INCREMENT, COUNTER_WIDTH));
				when "01"   => internalCounter := NEW_VALUE;
				when others => null;
			end case;

		end if;

		PC_VALUE <= internalCounter after DELAY;
	end process;

end architecture A_PROGRAM_COUNTER;
