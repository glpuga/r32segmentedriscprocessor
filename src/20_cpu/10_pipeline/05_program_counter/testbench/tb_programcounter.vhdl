--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_PROGRAM_COUNTER is
-- body
end entity TB_E_PROGRAM_COUNTER;

-- ------------------------------- -

architecture TB_A_PROGRAM_COUNTER of TB_E_PROGRAM_COUNTER is
	for all : C_PROGRAM_COUNTER use entity E_PROGRAM_COUNTER;

	-- Para simplificar el testbench voy a hacer instanciar el contador de s�lo 4 bits
	constant COUNTER_WIDTH : integer := 4;

	signal CLOCK          : STD_LOGIC;
	signal ASYNC_RESET    : STD_LOGIC;
	signal SYNC_SET_VALUE : STD_LOGIC;
	signal SYNC_INHIBIT   : STD_LOGIC;
	signal NEW_VALUE      : STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0);
	signal PC_VALUE       : STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0);

begin
	program_counter : C_PROGRAM_COUNTER
		generic map(
			COUNTER_WIDTH => COUNTER_WIDTH
		)
		port map(
			PC_VALUE       => PC_VALUE,
			NEW_VALUE      => NEW_VALUE,
			SYNC_SET_VALUE => SYNC_SET_VALUE,
			SYNC_INHIBIT   => SYNC_INHIBIT,
			ASYNC_RESET    => ASYNC_RESET,
			CLOCK          => CLOCK
		);

	test_process : process
	begin
		report "Comienzo en ensayo de contador de programa" severity note;
		wait for 1 us;

		-- Inicializo las entradas
		CLOCK          <= '0';
		SYNC_SET_VALUE <= '0';
		SYNC_INHIBIT   <= '0';
		ASYNC_RESET    <= '1';
		NEW_VALUE      <= "1010";
		wait for 1 us;

		-- Aplico un reset
		ASYNC_RESET <= '0';
		wait for 1 us;
		ASYNC_RESET <= '1';
		wait for 1 us;
		ASYNC_RESET <= '0';
		wait for 1 us;

		-- Verifico que el contador est� en cero
		assert PC_VALUE = "0000" report "Error (1)" severity error;
		wait for 1 us;

		-- Incremento el contador
		CLOCK <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		assert PC_VALUE = "0100" report "Error (2)" severity error;
		wait for 1 us;
		CLOCK <= '0';
		wait for 1 us;
		assert PC_VALUE = "0100" report "Error (3)" severity error;
		wait for 1 us;

		CLOCK <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		assert PC_VALUE = "1000" report "Error (4)" severity error;
		wait for 1 us;
		CLOCK <= '0';
		wait for 1 us;
		assert PC_VALUE = "1000" report "Error (5)" severity error;
		wait for 1 us;

		CLOCK <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		assert PC_VALUE = "1100" report "Error (6)" severity error;
		wait for 1 us;
		CLOCK <= '0';
		wait for 1 us;
		assert PC_VALUE = "1100" report "Error (7)" severity error;
		wait for 1 us;

		CLOCK <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		assert PC_VALUE = "0000" report "Error (8)" severity error;
		wait for 1 us;
		CLOCK <= '0';
		wait for 1 us;
		assert PC_VALUE = "0000" report "Error (9)" severity error;
		wait for 1 us;

		-- Fuerzo la carga de un nuevo valor no consecutivo
		CLOCK          <= '0';
		ASYNC_RESET    <= '0';
		SYNC_SET_VALUE <= '1';
		SYNC_INHIBIT   <= '0';
		NEW_VALUE      <= "1100";
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		assert PC_VALUE = "1100" report "Error (A)" severity error;
		wait for 1 us;
		CLOCK <= '0';
		wait for 1 us;
		assert PC_VALUE = "1100" report "Error (B)" severity error;
		wait for 1 us;

		-- Vuelvo al avance consecutivo
		CLOCK          <= '0';
		ASYNC_RESET    <= '0';
		SYNC_SET_VALUE <= '0';
		NEW_VALUE      <= "1010";
		wait for 1 us;

		-- Incremento el contador
		CLOCK <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		assert PC_VALUE = "0000" report "Error (C)" severity error;
		wait for 1 us;
		CLOCK <= '0';
		wait for 1 us;
		assert PC_VALUE = "0000" report "Error (D)" severity error;
		wait for 1 us;

		CLOCK <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		assert PC_VALUE = "0100" report "Error (E)" severity error;
		wait for 1 us;
		CLOCK <= '0';
		wait for 1 us;
		assert PC_VALUE = "0100" report "Error (F)" severity error;
		wait for 1 us;

		-- Inhibo el incremento
		SYNC_INHIBIT <= '1';

		CLOCK <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		assert PC_VALUE = "0100" report "Error (G)" severity error;
		wait for 1 us;
		CLOCK <= '0';
		wait for 1 us;
		assert PC_VALUE = "0100" report "Error (G)" severity error;
		wait for 1 us;

		report "Fin en ensayo de contador de programa" severity note;
		wait;

	end process;

end architecture TB_A_PROGRAM_COUNTER;
