--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_JEU is                         -- In case you are wondering, "Jump Evaluation Unit"
	generic(
		DELAY     : time    := TIME_DELAY_JEU;
		JEU_WIDTH : integer := DATA_BUS_SIZE
	);
	port(
		TEST_CONDITION : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
		TEST_VALUE     : in  STD_LOGIC_VECTOR((JEU_WIDTH - 1) downto 0);
		TEST_RESULT    : out STD_LOGIC
	);

end entity E_JEU;

-- --------------------------------

architecture A_JEU of E_JEU is
begin
	process(TEST_VALUE, TEST_CONDITION)
		variable vIntegerValue : integer;
		variable vG, vZ, vL    : STD_LOGIC;
		variable vResult       : STD_LOGIC;
	begin
		vIntegerValue := to_integer(signed(TEST_VALUE));

		-- Evaluo la condici�n de "Igual a cero"
		if (vIntegerValue = 0) then
			vZ := '1';
		else
			vZ := '0';
		end if;

		-- Evaluo la condici�n de "Mayor que cero"
		if (vIntegerValue > 0) then
			vG := '1';
		else
			vG := '0';
		end if;

		-- Evaluo "Menor que cero"
		-- Si no es mayor o igual a cero, es menor.
		vL := not (vZ or vG);

		-- Si se cumple cualquiera de las condiciones, la condici�n de salto se eval�a verdadera
		vResult := (vG and TEST_CONDITION(2)) or (vL and TEST_CONDITION(1)) or (vZ and TEST_CONDITION(0));

		TEST_RESULT <= vResult after DELAY;
	end process;

end architecture A_JEU;
