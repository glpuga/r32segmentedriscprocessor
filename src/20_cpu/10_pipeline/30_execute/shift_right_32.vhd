--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingeniería, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * Año 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.all;

-- --------------------------------

entity E_SHIFT_RIGHT_32 is
	port(
		INPUT    : in  STD_LOGIC_VECTOR(31 downto 0);
		BITCOUNT : in  STD_LOGIC_VECTOR(4 downto 0);

		UROTATE  : out STD_LOGIC_VECTOR(31 downto 0);
		USHIFT   : out STD_LOGIC_VECTOR(31 downto 0);
		SSHIFT   : out STD_LOGIC_VECTOR(31 downto 0)
	);
end entity E_SHIFT_RIGHT_32;

-- --------------------------------

architecture A_SHIFT_RIGHT_32 of E_SHIFT_RIGHT_32 is
	signal I_STAGE_32 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_16 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_08 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_04 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_02 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_01 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_MASK     : STD_LOGIC_VECTOR(31 downto 0);

	signal I_NEGATIVE : STD_LOGIC;

begin
	I_STAGE_32 <= INPUT;

	I_NEGATIVE <= INPUT(31);

	-- --
	--
	-- Genero la palabra rotada en la cantidad de bits deseada

	-- Rotaci�n en unidad de 8 bits
	I_STAGE_16 <= I_STAGE_32 when BITCOUNT(4) = '0' else I_STAGE_32(15 downto 0) & I_STAGE_32(31 downto 16);
	-- Rotaci�n en unidad de 8 bits
	I_STAGE_08 <= I_STAGE_16 when BITCOUNT(3) = '0' else I_STAGE_16(7 downto 0) & I_STAGE_16(31 downto 8);
	-- Rotaci�n en unidad de 4 bits
	I_STAGE_04 <= I_STAGE_08 when BITCOUNT(2) = '0' else I_STAGE_08(3 downto 0) & I_STAGE_08(31 downto 4);
	-- Rotaci�n en unidad de 2 bits
	I_STAGE_02 <= I_STAGE_04 when BITCOUNT(1) = '0' else I_STAGE_04(1 downto 0) & I_STAGE_04(31 downto 2);
	-- Rotaci�n en unidad de 1 bits
	I_STAGE_01 <= I_STAGE_02 when BITCOUNT(0) = '0' else I_STAGE_02(0) & I_STAGE_02(31 downto 1);

	-- --
	--
	-- Genero la m�scara que me sirve para generar el unsigned_shift a partir del rotate
	--
	-- La m�scara indica que bits se protegen.
	with BITCOUNT select I_MASK <=
		"1111111111111111" & "1111111111111111" when "00000",
		"0111111111111111" & "1111111111111111" when "00001",
		"0011111111111111" & "1111111111111111" when "00010",
		"0001111111111111" & "1111111111111111" when "00011",
		"0000111111111111" & "1111111111111111" when "00100",
		"0000011111111111" & "1111111111111111" when "00101",
		"0000001111111111" & "1111111111111111" when "00110",
		"0000000111111111" & "1111111111111111" when "00111",
		"0000000011111111" & "1111111111111111" when "01000",
		"0000000001111111" & "1111111111111111" when "01001",
		"0000000000111111" & "1111111111111111" when "01010",
		"0000000000011111" & "1111111111111111" when "01011",
		"0000000000001111" & "1111111111111111" when "01100",
		"0000000000000111" & "1111111111111111" when "01101",
		"0000000000000011" & "1111111111111111" when "01110",
		"0000000000000001" & "1111111111111111" when "01111",
		"0000000000000000" & "1111111111111111" when "10000",
		"0000000000000000" & "0111111111111111" when "10001",
		"0000000000000000" & "0011111111111111" when "10010",
		"0000000000000000" & "0001111111111111" when "10011",
		"0000000000000000" & "0000111111111111" when "10100",
		"0000000000000000" & "0000011111111111" when "10101",
		"0000000000000000" & "0000001111111111" when "10110",
		"0000000000000000" & "0000000111111111" when "10111",
		"0000000000000000" & "0000000011111111" when "11000",
		"0000000000000000" & "0000000001111111" when "11001",
		"0000000000000000" & "0000000000111111" when "11010",
		"0000000000000000" & "0000000000011111" when "11011",
		"0000000000000000" & "0000000000001111" when "11100",
		"0000000000000000" & "0000000000000111" when "11101",
		"0000000000000000" & "0000000000000011" when "11110",
		"0000000000000000" & "0000000000000001" when others;

	-- --
	--
	-- Genero las tres salidas.

	-- Rotaci�n a derecha, sin signo 
	UROTATE <= I_STAGE_01;

	-- Desplazamiento a derecha, sin signo
	USHIFT <= I_STAGE_01 and I_MASK;

	-- Desplazamiento a derecha, con signo
	with I_NEGATIVE select SSHIFT <=
		(I_STAGE_01 and I_MASK) or (not I_MASK) when '1',
		(I_STAGE_01 and I_MASK) when others;

end architecture A_SHIFT_RIGHT_32;
