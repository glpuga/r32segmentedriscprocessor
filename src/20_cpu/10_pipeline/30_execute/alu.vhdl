--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_ALU is
	generic(
		DELAY     : time    := TIME_DELAY_ALU;
		ALU_WIDTH : integer := DATA_BUS_SIZE
	);
	port(
		OP_CODE   : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
		OPERAND_A : in  STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
		OPERAND_B : in  STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);

		RESULT    : out STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0)
	);
end entity E_ALU;

-- --------------------------------

architecture A_ALU of E_ALU is
	for all : C_SHIFT_LEFT_32 use entity E_SHIFT_LEFT_32;
	for all : C_SHIFT_RIGHT_32 use entity E_SHIFT_RIGHT_32;

	signal I_SUBSTRACTION : STD_LOGIC;

	signal I_ADDER_INPUT_A  : STD_LOGIC_VECTOR((ALU_WIDTH - 1 + 2) downto 0);
	signal I_ADDER_INPUT_B  : STD_LOGIC_VECTOR((ALU_WIDTH - 1 + 2) downto 0);
	signal I_AUX_INPUT_B    : STD_LOGIC_VECTOR((ALU_WIDTH - 1 + 2) downto 0);
	signal I_INVERSION_MASK : STD_LOGIC_VECTOR((ALU_WIDTH - 1 + 2) downto 0);
	signal I_ADDER_OUTPUT   : STD_LOGIC_VECTOR((ALU_WIDTH - 1 + 2) downto 0);

	signal I_LOGIC_AND_RESULT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_LOGIC_OR_RESULT  : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_LOGIC_NOR_RESULT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_LOGIC_XOR_RESULT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);

	signal I_SUBGROUP_00_RESULT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_SUBGROUP_01_RESULT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_SUBGROUP_02_RESULT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_SUBGROUP_03_RESULT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);

	signal I_UNSIGNED_SHIFT_LEFT     : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_UNSIGNED_ROTATION_LEFT  : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_UNSIGNED_SHIFT_RIGHT    : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
	signal I_UNSIGNED_ROTATION_RIGHT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);

	signal I_ALU_OUTPUT : STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);

begin

	-- --
	--
	-- Subgrupo 00: suma y resta
	-- 
	-- COD 00-00 Suma
	-- COD 00-01 Resta
	-- COD 00-10 ?? indefinido
	-- COD 00-11 ?? indefinido

	-- Toda esta jugada es para hacer la suma y la resta con un �nico sumador. La idea base es hacer la resta
	-- haciendo la suma del operando A con el INVERSO del operando B + 1 (o sea, sumar con el complemento-2 del
	-- operando B).
	-- Determino si hay que hacer una resta. 
	I_SUBSTRACTION   <= not OP_CODE(1) and OP_CODE(0);
	-- Extiendo el bit indicador de resta hasta el ancho de las entradas al sumador, para utilizarlo
	-- luego para invertir el operador B mediante la operaci�n XOR con esta m�scara.
	I_INVERSION_MASK <= (others => I_SUBSTRACTION);

	-- Extiendo el operando A hasta el ancho del sumador. Se agrego un bit antes y otro despu�s. 
	-- El bit de menor orden agregado va a garantizar que el bit de menor orden haga acarreo si el 
	-- operando B es negativo. 
	I_AUX_INPUT_B   <= "0" & OPERAND_B & "0";
	I_ADDER_INPUT_A <= "0" & OPERAND_A & "1";
	-- Aplico la m�scara de inversi�n. El resultado es que si la operaci�n es una resta I_ADDER_INPUT_B va a 
	-- tener el complemento-1 del operando B, y un 1 en el bit de menor orden que garantiza un acarreo en el
	-- bit de menor orden que hace la parte del +1 en el resultado.
	I_ADDER_INPUT_B <= I_AUX_INPUT_B xor I_INVERSION_MASK;

	-- Invoco la suma. Con esto se debiera sintetizar un �nico sumador. 
	I_ADDER_OUTPUT <= std_logic_vector(unsigned(I_ADDER_INPUT_A) + unsigned(I_ADDER_INPUT_B));

	-- Extraigo el resultado. La parte que me interesa son los bits que quedaron en el rango (ALU_WIDTH downto 1)
	I_SUBGROUP_00_RESULT <= I_ADDER_OUTPUT(ALU_WIDTH downto 1);

	-- --------------------------------------------------------------------------
	-- --------------------------------------------------------------------------
	-- --------------------------------------------------------------------------

	-- --
	--
	-- Subgrupo 01: and, or, nor y xor
	-- 	
	-- COD 01-00 AND
	-- COD 01-01 OR
	-- COD 01-10 NOR
	-- COD 01-11 XOR
	I_LOGIC_AND_RESULT <= OPERAND_A and OPERAND_B;
	I_LOGIC_OR_RESULT  <= OPERAND_A or OPERAND_B;
	I_LOGIC_NOR_RESULT <= OPERAND_A nor OPERAND_B;
	I_LOGIC_XOR_RESULT <= OPERAND_A xor OPERAND_B;

	with OP_CODE(1 downto 0) select I_SUBGROUP_01_RESULT <=
		I_LOGIC_AND_RESULT when "00",
		I_LOGIC_OR_RESULT when "01",
		I_LOGIC_NOR_RESULT when "10",
		I_LOGIC_XOR_RESULT when others;

	-- --------------------------------------------------------------------------
	-- --------------------------------------------------------------------------
	-- --------------------------------------------------------------------------

	-- --	
	--
	-- Subgrupo 10: sin asignar
	-- 
	-- COD 10-00 ???
	-- COD 10-01 ???
	-- COD 10-10 ???
	-- COD 10-11 ???
	I_SUBGROUP_02_RESULT <= std_logic_vector(to_unsigned(0, ALU_WIDTH));

	-- --------------------------------------------------------------------------
	-- --------------------------------------------------------------------------
	-- --------------------------------------------------------------------------

	-- --
	--
	-- Subgrupo 11: corrimientos y rotaciones de bit
	-- 
	-- COD 11-00 SHIFT LEFT
	-- COD 11-01 SHIFT RIGHT
	-- COD 11-10 ROTATE LEFT
	-- COD 11-11 ROTATE RIGHT
	left_shifter : C_SHIFT_LEFT_32
		port map(
			INPUT    => OPERAND_A,
			BITCOUNT => OPERAND_B(4 downto 0),
			USHIFT   => I_UNSIGNED_SHIFT_LEFT,
			SSHIFT   => open,
			UROTATE  => I_UNSIGNED_ROTATION_LEFT
		);

	righ_shifter : C_SHIFT_RIGHT_32
		port map(
			INPUT    => OPERAND_A,
			BITCOUNT => OPERAND_B(4 downto 0),
			USHIFT   => I_UNSIGNED_SHIFT_RIGHT,
			SSHIFT   => open,
			UROTATE  => I_UNSIGNED_ROTATION_RIGHT
		);

	-- --
	--
	-- Selecci�n de la salida correcta del rotador de bits
	with OP_CODE(1 downto 0) select I_SUBGROUP_03_RESULT <=
		I_UNSIGNED_SHIFT_LEFT when "00",
		I_UNSIGNED_SHIFT_RIGHT when "01",
		I_UNSIGNED_ROTATION_LEFT when "10",
		I_UNSIGNED_ROTATION_RIGHT when others;

	-- --------------------------------------------------------------------------
	-- --------------------------------------------------------------------------
	-- --------------------------------------------------------------------------

	-- --
	--
	-- Cierre, asigno a la salida el resultado del subgrupo que corresponde
	--
	with OP_CODE(3 downto 2) select I_ALU_OUTPUT <=
		I_SUBGROUP_00_RESULT when "00",
		I_SUBGROUP_01_RESULT when "01",
		I_SUBGROUP_02_RESULT when "10",
		I_SUBGROUP_03_RESULT when others;

	RESULT <= I_ALU_OUTPUT after DELAY;

end architecture A_ALU;
