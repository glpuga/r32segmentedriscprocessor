--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingeniería, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * Año 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.all;

-- --------------------------------

entity E_SHIFT_LEFT_32 is
	port(
		INPUT    : in  STD_LOGIC_VECTOR(31 downto 0);
		BITCOUNT : in  STD_LOGIC_VECTOR(4 downto 0);

		UROTATE  : out STD_LOGIC_VECTOR(31 downto 0);
		USHIFT   : out STD_LOGIC_VECTOR(31 downto 0);
		SSHIFT   : out STD_LOGIC_VECTOR(31 downto 0)
	);
end entity E_SHIFT_LEFT_32;

-- --------------------------------

architecture A_SHIFT_LEFT_32 of E_SHIFT_LEFT_32 is
	signal I_STAGE_32 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_16 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_08 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_04 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_02 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_STAGE_01 : STD_LOGIC_VECTOR(31 downto 0);
	signal I_MASK     : STD_LOGIC_VECTOR(31 downto 0);

begin
	I_STAGE_32 <= INPUT;

	-- --
	--
	-- Genero la palabra rotada en la cantidad de bits deseada

	-- Rotaci�n en unidad de 8 bits
	I_STAGE_16 <= I_STAGE_32 when BITCOUNT(4) = '0' else I_STAGE_32(15 downto 0) & I_STAGE_32(31 downto 16);
	-- Rotaci�n en unidad de 8 bits
	I_STAGE_08 <= I_STAGE_16 when BITCOUNT(3) = '0' else I_STAGE_16(23 downto 0) & I_STAGE_16(31 downto 24);
	-- Rotaci�n en unidad de 4 bits
	I_STAGE_04 <= I_STAGE_08 when BITCOUNT(2) = '0' else I_STAGE_08(27 downto 0) & I_STAGE_08(31 downto 28);
	-- Rotaci�n en unidad de 2 bits
	I_STAGE_02 <= I_STAGE_04 when BITCOUNT(1) = '0' else I_STAGE_04(29 downto 0) & I_STAGE_04(31 downto 30);
	-- Rotaci�n en unidad de 1 bits
	I_STAGE_01 <= I_STAGE_02 when BITCOUNT(0) = '0' else I_STAGE_02(30 downto 0) & I_STAGE_02(31);

	-- --
	--
	-- Genero la m�scara que me sirve para generar el unsigned_shift a partir del rotate
	--
	-- La m�scara indica que bits se protegen.
	with BITCOUNT select I_MASK <=
		"1111111111111111" & "1111111111111111" when "00000",
		"1111111111111111" & "1111111111111110" when "00001",
		"1111111111111111" & "1111111111111100" when "00010",
		"1111111111111111" & "1111111111111000" when "00011",
		"1111111111111111" & "1111111111110000" when "00100",
		"1111111111111111" & "1111111111100000" when "00101",
		"1111111111111111" & "1111111111000000" when "00110",
		"1111111111111111" & "1111111110000000" when "00111",
		"1111111111111111" & "1111111100000000" when "01000",
		"1111111111111111" & "1111111000000000" when "01001",
		"1111111111111111" & "1111110000000000" when "01010",
		"1111111111111111" & "1111100000000000" when "01011",
		"1111111111111111" & "1111000000000000" when "01100",
		"1111111111111111" & "1110000000000000" when "01101",
		"1111111111111111" & "1100000000000000" when "01110",
		"1111111111111111" & "1000000000000000" when "01111",
		"1111111111111111" & "0000000000000000" when "10000",
		"1111111111111110" & "0000000000000000" when "10001",
		"1111111111111100" & "0000000000000000" when "10010",
		"1111111111111000" & "0000000000000000" when "10011",
		"1111111111110000" & "0000000000000000" when "10100",
		"1111111111100000" & "0000000000000000" when "10101",
		"1111111111000000" & "0000000000000000" when "10110",
		"1111111110000000" & "0000000000000000" when "10111",
		"1111111100000000" & "0000000000000000" when "11000",
		"1111111000000000" & "0000000000000000" when "11001",
		"1111110000000000" & "0000000000000000" when "11010",
		"1111100000000000" & "0000000000000000" when "11011",
		"1111000000000000" & "0000000000000000" when "11100",
		"1110000000000000" & "0000000000000000" when "11101",
		"1100000000000000" & "0000000000000000" when "11110",
		"1000000000000000" & "0000000000000000" when others;
	 -- --
	 --
	 -- Genero las tres salidas.

	 -- Rotaci�n a izquierda, sin signo 
	 UROTATE <= I_STAGE_01;

	-- Desplazamiento a izquierda, sin signo
	USHIFT <= I_STAGE_01 and I_MASK;

	-- Desplazamiento a izquierda, con signo (es equivalente a sin signo)
	SSHIFT <= I_STAGE_01 and I_MASK;

end architecture A_SHIFT_LEFT_32;
