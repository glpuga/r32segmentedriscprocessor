--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

entity TB_E_SHIFT_LEFT_32 is
--
end entity TB_E_SHIFT_LEFT_32;

architecture TB_A_SHIFT_LEFT_32 of TB_E_SHIFT_LEFT_32 is
	for all : C_SHIFT_LEFT_32 use entity E_SHIFT_LEFT_32;

	signal INPUT    : STD_LOGIC_VECTOR(31 downto 0);
	signal BITCOUNT : STD_LOGIC_VECTOR(4 downto 0);

	signal UROTATE : STD_LOGIC_VECTOR(31 downto 0);
	signal USHIFT  : STD_LOGIC_VECTOR(31 downto 0);
	signal SSHIFT  : STD_LOGIC_VECTOR(31 downto 0);

begin
	unit_under_test : C_SHIFT_LEFT_32 port map(
			INPUT    => INPUT,
			BITCOUNT => BITCOUNT,
			UROTATE  => UROTATE,
			USHIFT   => USHIFT,
			SSHIFT   => SSHIFT
		);

	test_process : process
	begin
		report "Comienzo en ensayo de shift_left_32" severity note;
		wait for 1 us;

		--
		-- Coloco una se�al de prueba sin signo a la entrada 
		INPUT <= "00000000" & "11000000" & "10000000" & "00000001";

		for i in 0 to 31 loop
			BITCOUNT <= std_logic_vector(to_unsigned(i, 5));
			wait for 1 us;

			assert UROTATE = std_logic_vector(rotate_left(unsigned(INPUT), i))
				report "Error (1)" severity error;
			assert USHIFT = std_logic_vector(shift_left(unsigned(INPUT), i))
				report "Error (2)" severity error;
			assert SSHIFT = std_logic_vector(shift_left(signed(INPUT), i))
				report "Error (3)" severity error;

		end loop;

		--
		-- Coloco una se�al de prueba con signo a la entrada 
		INPUT <= "10000000" & "11000000" & "10000000" & "00000001";

		for i in 0 to 31 loop
			BITCOUNT <= std_logic_vector(to_unsigned(i, 5));
			wait for 1 us;

			assert UROTATE = std_logic_vector(rotate_left(unsigned(INPUT), i))
				report "Error (1)" severity error;
			assert USHIFT = std_logic_vector(shift_left(unsigned(INPUT), i))
				report "Error (2)" severity error;
			assert SSHIFT = std_logic_vector(shift_left(signed(INPUT), i))
				report "Error (3)" severity error;

		end loop;

		report "Fin en ensayo de shift_left_32" severity note;
		wait;

	end process;
end architecture TB_A_SHIFT_LEFT_32;
