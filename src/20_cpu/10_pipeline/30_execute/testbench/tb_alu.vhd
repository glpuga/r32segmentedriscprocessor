--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_ALU is
-- body
end entity TB_E_ALU;

-- --------------------------------

architecture TB_A_ALU of TB_E_ALU is
	for all : C_ALU use entity E_ALU;

	signal OP_CODE   : STD_LOGIC_VECTOR(3 downto 0);
	signal OPERAND_A : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal OPERAND_B : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal RESULT    : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

begin
	unit_under_test : C_ALU port map(
			OP_CODE   => OP_CODE,
			OPERAND_A => OPERAND_A,
			OPERAND_B => OPERAND_B,
			RESULT    => RESULT
		);

	test_process : process
	begin
		report "Comienzo en ensayo de la ALU" severity note;
		wait for 1 us;

		-- Verifico la operaci�n de suma 
		OP_CODE   <= "0000";
		-- POSITIVO + POSITIVO
		OPERAND_A <= std_logic_vector(to_signed(99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(90, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(189, DATA_BUS_SIZE)) report "Error (11)" severity error;
		wait for 1 us;
		-- POSITIVO + NEGATIVO
		OPERAND_A <= std_logic_vector(to_signed(99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(-90, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(9, DATA_BUS_SIZE)) report "Error (12)" severity error;
		wait for 1 us;
		-- NEGATIVO + POSITIVO
		OPERAND_A <= std_logic_vector(to_signed(-99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(90, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(-9, DATA_BUS_SIZE)) report "Error (13)" severity error;
		wait for 1 us;
		-- NEGATIVO + NEGATIVO
		OPERAND_A <= std_logic_vector(to_signed(-99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(-90, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(-189, DATA_BUS_SIZE)) report "Error (14)" severity error;
		wait for 1 us;
		-- POSITIVO + CERO
		OPERAND_A <= std_logic_vector(to_signed(99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(99, DATA_BUS_SIZE)) report "Error (15)" severity error;
		wait for 1 us;
		-- NEGATIVO + CERO
		OPERAND_A <= std_logic_vector(to_signed(-99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(-99, DATA_BUS_SIZE)) report "Error (16)" severity error;
		wait for 1 us;
		-- CERO + POSITIVO
		OPERAND_A <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(99, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(99, DATA_BUS_SIZE)) report "Error (17)" severity error;
		wait for 1 us;
		-- CERO + NEGATIVO
		OPERAND_A <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(-99, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(-99, DATA_BUS_SIZE)) report "Error (18)" severity error;
		wait for 1 us;
		-- CERO + CERO
		OPERAND_A <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(00, DATA_BUS_SIZE)) report "Error (19)" severity error;
		wait for 1 us;
		
		-- Verifico la operaci�n de resta 
		OP_CODE   <= "0001";
		-- POSITIVO - POSITIVO
		OPERAND_A <= std_logic_vector(to_signed(99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(90, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(9, DATA_BUS_SIZE)) report "Error (21)" severity error;
		wait for 1 us;
		-- POSITIVO - NEGATIVO
		OPERAND_A <= std_logic_vector(to_signed(99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(-90, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(189, DATA_BUS_SIZE)) report "Error (22)" severity error;
		wait for 1 us;
		-- NEGATIVO - POSITIVO
		OPERAND_A <= std_logic_vector(to_signed(-99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(90, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(-189, DATA_BUS_SIZE)) report "Error (23)" severity error;
		wait for 1 us;
		-- NEGATIVO - NEGATIVO
		OPERAND_A <= std_logic_vector(to_signed(-99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(-90, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(-9, DATA_BUS_SIZE)) report "Error (24)" severity error;
		wait for 1 us;
		-- POSITIVO - CERO
		OPERAND_A <= std_logic_vector(to_signed(99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(99, DATA_BUS_SIZE)) report "Error (25)" severity error;
		wait for 1 us;
		-- NEGATIVO - CERO
		OPERAND_A <= std_logic_vector(to_signed(-99, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(-99, DATA_BUS_SIZE)) report "Error (26)" severity error;
		wait for 1 us;
		-- CERO - POSITIVO
		OPERAND_A <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(99, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(-99, DATA_BUS_SIZE)) report "Error (27)" severity error;
		wait for 1 us;
		-- CERO - NEGATIVO
		OPERAND_A <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(-99, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(99, DATA_BUS_SIZE)) report "Error (28)" severity error;
		wait for 1 us;
		-- CERO - CERO
		OPERAND_A <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		OPERAND_B <= std_logic_vector(to_signed(00, DATA_BUS_SIZE));
		wait for 1 us;
		assert RESULT = std_logic_vector(to_signed(00, DATA_BUS_SIZE)) report "Error (29)" severity error;
		wait for 1 us;

		-- Verifico la operaci�n AND
		OP_CODE   <= "0100";
		OPERAND_A <= x"F0F0F0F0";
		OPERAND_B <= x"AAAAAAAA";
		wait for 1 us;
		assert RESULT = x"A0A0A0A0" report "Error (30)" severity error;
		wait for 1 us;

		-- Verifico la operaci�n OR
		OP_CODE   <= "0101";
		OPERAND_A <= x"F0F0F0F0";
		OPERAND_B <= x"AAAAAAAA";
		wait for 1 us;
		assert RESULT = x"FAFAFAFA" report "Error (40)" severity error;
		wait for 1 us;

		-- Verifico la operaci�n NOR
		OP_CODE   <= "0110";
		OPERAND_A <= x"F0F0F0F0";
		OPERAND_B <= x"AAAAAAAA";
		wait for 1 us;
		assert RESULT = x"05050505" report "Error (50)" severity error;
		wait for 1 us;

		-- Verifico la operaci�n XOR
		OP_CODE   <= "0111";
		OPERAND_A <= x"F0F0F0F0";
		OPERAND_B <= x"AAAAAAAA";
		wait for 1 us;
		assert RESULT = x"5A5A5A5A" report "Error (60)" severity error;
		wait for 1 us;

		-- Verifico la operaci�n de desplazamiento a izquierda
		OP_CODE   <= "1100";
		OPERAND_A <= x"12345678";
		OPERAND_B <= x"00000010";       -- 16 bits a izquierda
		wait for 1 us;
		assert RESULT = x"56780000" report "Error (70)" severity error;
		wait for 1 us;

		-- Verifico la operaci�n de desplazamiento a derecha
		OP_CODE   <= "1101";
		OPERAND_A <= x"12345678";
		OPERAND_B <= x"00000010";       -- 16 bits a derecha
		wait for 1 us;
		assert RESULT = x"00001234" report "Error (80)" severity error;
		wait for 1 us;

		-- Verifico la operaci�n de rotaci�n a izquierda
		OP_CODE   <= "1110";
		OPERAND_A <= x"12345678";
		OPERAND_B <= x"0000000C";       -- 12 bits a izquierda
		wait for 1 us;
		assert RESULT = x"45678123" report "Error (90)" severity error;
		wait for 1 us;

		-- Verifico la operaci�n de rotaci�n a derecha
		OP_CODE   <= "1111";
		OPERAND_A <= x"12345678";
		OPERAND_B <= x"0000000C";       -- 12 bits a derecha
		wait for 1 us;
		assert RESULT = x"67812345" report "Error (A0)" severity error;
		wait for 1 us;

		report "Fin en ensayo de la ALU" severity note;
		wait;

	end process;

end architecture TB_A_ALU;
