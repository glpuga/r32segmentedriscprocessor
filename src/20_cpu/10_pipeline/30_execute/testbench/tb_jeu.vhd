--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_JEU is
-- body
end entity TB_E_JEU;

-- --------------------------------

architecture TB_A_JEU of TB_E_JEU is
	for all : C_JEU use entity E_JEU;

	signal TEST_CONDITION : STD_LOGIC_VECTOR(3 downto 0);
	signal TEST_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal TEST_RESULT    : STD_LOGIC;

begin
	unit_under_test : C_JEU port map(
			TEST_CONDITION => TEST_CONDITION,
			TEST_VALUE     => TEST_VALUE,
			TEST_RESULT    => TEST_RESULT
		);

	test_process : process
	begin

		-- En el modelsim hay un warning inocuo que ocurre al comienzo de la ejecuci�n
		-- del testbench porque se eval�a to_integer dentro de la entidad de la JEU cuando
		-- todav�a la entrada TEST_VALUE vale "xxxxxxxx"

		report "Comienzo en ensayo de la JEU" severity note;
		wait for 1 us;

		-- --
		--
		-- C�digo para (no-salta-nunca)
		TEST_CONDITION <= "0000";

		-- Verifico para positivo
		TEST_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (11)" severity error;
		wait for 1 us;
		-- Verifico para cero
		TEST_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (12)" severity error;
		wait for 1 us;
		-- Verifico para negativo
		TEST_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (13)" severity error;
		wait for 1 us;

		-- ---------------------------------------------------
		-- ---------------------------------------------------
		-- ---------------------------------------------------

		-- --
		--
		-- C�digo para (V = 0)
		TEST_CONDITION <= "0001";

		-- Verifico para positivo
		TEST_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (21)" severity error;
		wait for 1 us;
		-- Verifico para cero
		TEST_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (22)" severity error;
		wait for 1 us;
		-- Verifico para negativo
		TEST_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (23)" severity error;
		wait for 1 us;

		-- ---------------------------------------------------
		-- ---------------------------------------------------
		-- ---------------------------------------------------

		-- --
		--
		-- C�digo para (V < 0)
		TEST_CONDITION <= "0010";

		-- Verifico para positivo
		TEST_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (31)" severity error;
		wait for 1 us;
		-- Verifico para cero
		TEST_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (32)" severity error;
		wait for 1 us;
		-- Verifico para negativo
		TEST_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (33)" severity error;
		wait for 1 us;

		-- ---------------------------------------------------
		-- ---------------------------------------------------
		-- ---------------------------------------------------

		-- --
		--
		-- C�digo para (V > 0)
		TEST_CONDITION <= "0100";

		-- Verifico para positivo
		TEST_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (41)" severity error;
		wait for 1 us;
		-- Verifico para cero
		TEST_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (42)" severity error;
		wait for 1 us;
		-- Verifico para negativo
		TEST_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (43)" severity error;
		wait for 1 us;

		-- ---------------------------------------------------
		-- ---------------------------------------------------
		-- ---------------------------------------------------

		-- --
		--
		-- C�digo para (V <= 0)
		TEST_CONDITION <= "0011";

		-- Verifico para positivo
		TEST_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (51)" severity error;
		wait for 1 us;
		-- Verifico para cero
		TEST_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (52)" severity error;
		wait for 1 us;
		-- Verifico para negativo
		TEST_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (53)" severity error;
		wait for 1 us;

		-- ---------------------------------------------------
		-- ---------------------------------------------------
		-- ---------------------------------------------------

		-- --
		--
		-- C�digo para (V >= 0)
		TEST_CONDITION <= "0101";

		-- Verifico para positivo
		TEST_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (61)" severity error;
		wait for 1 us;
		-- Verifico para cero
		TEST_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (62)" severity error;
		wait for 1 us;
		-- Verifico para negativo
		TEST_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (63)" severity error;
		wait for 1 us;

		-- ---------------------------------------------------
		-- ---------------------------------------------------
		-- ---------------------------------------------------

		-- --
		--
		-- C�digo para (V != 0)
		TEST_CONDITION <= "0110";

		-- Verifico para positivo
		TEST_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (71)" severity error;
		wait for 1 us;
		-- Verifico para cero
		TEST_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '0' report "Error (72)" severity error;
		wait for 1 us;
		-- Verifico para negativo
		TEST_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (73)" severity error;
		wait for 1 us;

		-- ---------------------------------------------------
		-- ---------------------------------------------------
		-- ---------------------------------------------------

		-- --
		--
		-- C�digo para (salta-siempre)
		TEST_CONDITION <= "0111";

		-- Verifico para positivo
		TEST_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (81)" severity error;
		wait for 1 us;
		-- Verifico para cero
		TEST_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (82)" severity error;
		wait for 1 us;
		-- Verifico para negativo
		TEST_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
		wait for 1 us;
		assert TEST_RESULT = '1' report "Error (83)" severity error;
		wait for 1 us;

		-- ---------------------------------------------------
		-- ---------------------------------------------------
		-- ---------------------------------------------------

		report "Fin en ensayo de la JEU" severity note;
		wait;

	end process;

end architecture TB_A_JEU;
