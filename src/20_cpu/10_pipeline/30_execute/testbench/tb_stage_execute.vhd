--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_STAGE_EXECUTE is
-- body
end entity TB_E_STAGE_EXECUTE;

-- --------------------------------

architecture TB_A_STAGE_EXECUTE of TB_E_STAGE_EXECUTE is
	for all : C_STAGE_EXECUTE use entity E_STAGE_EXECUTE;

	signal BP_OP_IS_ALU_RR           : STD_LOGIC;
	signal BP_OP_IS_ALU_RI           : STD_LOGIC;
	signal BP_OP_IS_LOAD_STORE       : STD_LOGIC;
	signal BP_OP_IS_LOAD             : STD_LOGIC;
	signal BP_OP_IS_STORE            : STD_LOGIC;
	signal BP_OP_IS_CONDITIONAL_JUMP : STD_LOGIC;
	signal BP_ALU_OPERATION_CODE     : STD_LOGIC_VECTOR(3 downto 0);
	signal BP_JUMP_CONDITION_CODE    : STD_LOGIC_VECTOR(3 downto 0);

	signal BP_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
	signal BP_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.  
	signal BP_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

	signal BP_OP_DESTINATION_REG_ID : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal BP_OP_OPERAND_1_REG_ID   : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal BP_OP_OPERAND_2_REG_ID   : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

	signal BP_OP_OPERAND_1_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
	signal BP_OP_OPERAND_2_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros. 
	signal BP_OP_IMM_VALUE       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucci�n). 

	-- ---

	signal FP_OP_IS_ALU_RR           : STD_LOGIC;
	signal FP_OP_IS_ALU_RI           : STD_LOGIC;
	signal FP_OP_IS_LOAD_STORE       : STD_LOGIC;
	signal FP_OP_IS_LOAD             : STD_LOGIC;
	signal FP_OP_IS_STORE            : STD_LOGIC;
	signal FP_OP_IS_CONDITIONAL_JUMP : STD_LOGIC;

	signal FP_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
	signal FP_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.  
	signal FP_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

	signal FP_OP_DESTINATION_REG_ID : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal FP_OP_OPERAND_1_REG_ID   : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal FP_OP_OPERAND_2_REG_ID   : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

	signal FP_OP_OPERAND_1_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.

	signal FP_ALU_RESULT                 : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valore resultado de la ALU 
	signal FP_BRANCH_TAKEN               : STD_LOGIC; -- Efectuar o no el salto. 
	signal FP_OP_BRANCH_DESTINATION_ADDR : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

	-- ----------------------------------
	signal BP_CONTROL_SIGNALS : STD_LOGIC_VECTOR(5 downto 0);
	signal FP_CONTROL_SIGNALS : STD_LOGIC_VECTOR(5 downto 0);

begin

	-- Utilizo estas se�ales para manejar las se�ales de control de forma m�s pr�ctica
	BP_OP_IS_ALU_RR           <= BP_CONTROL_SIGNALS(5);
	BP_OP_IS_ALU_RI           <= BP_CONTROL_SIGNALS(4);
	BP_OP_IS_LOAD_STORE       <= BP_CONTROL_SIGNALS(3);
	BP_OP_IS_LOAD             <= BP_CONTROL_SIGNALS(2);
	BP_OP_IS_STORE            <= BP_CONTROL_SIGNALS(1);
	BP_OP_IS_CONDITIONAL_JUMP <= BP_CONTROL_SIGNALS(0);
	FP_CONTROL_SIGNALS        <= FP_OP_IS_ALU_RR & FP_OP_IS_ALU_RI & FP_OP_IS_LOAD_STORE & FP_OP_IS_LOAD & FP_OP_IS_STORE & FP_OP_IS_CONDITIONAL_JUMP;

	unit_under_test : C_STAGE_EXECUTE port map(
			BP_CLOCK_BEATS                => BP_CLOCK_BEATS,
			BP_PC_VALUE                   => BP_PC_VALUE,
			BP_INSTR                      => BP_INSTR,
			BP_OP_IS_ALU_RR               => BP_OP_IS_ALU_RR,
			BP_OP_IS_ALU_RI               => BP_OP_IS_ALU_RI,
			BP_OP_IS_LOAD_STORE           => BP_OP_IS_LOAD_STORE,
			BP_OP_IS_LOAD                 => BP_OP_IS_LOAD,
			BP_OP_IS_STORE                => BP_OP_IS_STORE,
			BP_OP_IS_CONDITIONAL_JUMP     => BP_OP_IS_CONDITIONAL_JUMP,
			BP_ALU_OPERATION_CODE         => BP_ALU_OPERATION_CODE,
			BP_JUMP_CONDITION_CODE        => BP_JUMP_CONDITION_CODE,
			BP_OP_DESTINATION_REG_ID      => BP_OP_DESTINATION_REG_ID,
			BP_OP_OPERAND_1_REG_ID        => BP_OP_OPERAND_1_REG_ID,
			BP_OP_OPERAND_2_REG_ID        => BP_OP_OPERAND_2_REG_ID,
			BP_OP_OPERAND_1_VALUE         => BP_OP_OPERAND_1_VALUE,
			BP_OP_OPERAND_2_VALUE         => BP_OP_OPERAND_2_VALUE,
			BP_OP_IMM_VALUE               => BP_OP_IMM_VALUE,
			FP_CLOCK_BEATS                => FP_CLOCK_BEATS,
			FP_PC_VALUE                   => FP_PC_VALUE,
			FP_INSTR                      => FP_INSTR,
			FP_OP_IS_ALU_RR               => FP_OP_IS_ALU_RR,
			FP_OP_IS_ALU_RI               => FP_OP_IS_ALU_RI,
			FP_OP_IS_LOAD_STORE           => FP_OP_IS_LOAD_STORE,
			FP_OP_IS_LOAD                 => FP_OP_IS_LOAD,
			FP_OP_IS_STORE                => FP_OP_IS_STORE,
			FP_OP_IS_CONDITIONAL_JUMP     => FP_OP_IS_CONDITIONAL_JUMP,
			FP_OP_DESTINATION_REG_ID      => FP_OP_DESTINATION_REG_ID,
			FP_OP_OPERAND_1_REG_ID        => FP_OP_OPERAND_1_REG_ID,
			FP_OP_OPERAND_2_REG_ID        => FP_OP_OPERAND_2_REG_ID,
			FP_OP_OPERAND_1_VALUE         => FP_OP_OPERAND_1_VALUE,
			FP_ALU_RESULT                 => FP_ALU_RESULT,
			FP_BRANCH_TAKEN               => FP_BRANCH_TAKEN,
			FP_OP_BRANCH_DESTINATION_ADDR => FP_OP_BRANCH_DESTINATION_ADDR);

	test_process : process
	begin
		report "Comienzo en ensayo de la etapa EXECUTE" severity note;
		wait for 1 us;

		-- -- 
		-- 
		-- Verifico las se�ales que tienen que atravesar la etapa
		for I in 0 to (2 ** 6 - 1) loop
			BP_CONTROL_SIGNALS <= std_logic_vector(to_unsigned(I, 6));
			wait for 1 us;
			assert FP_CONTROL_SIGNALS = BP_CONTROL_SIGNALS report "Error (10)" severity error;
			wait for 1 us;

		end loop;

		BP_CLOCK_BEATS           <= std_logic_vector(to_unsigned(0, BEATS_COUNTER_SIZE));
		BP_PC_VALUE              <= std_logic_vector(to_unsigned(0, ADDR_BUS_SIZE));
		BP_INSTR                 <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE));
		BP_ALU_OPERATION_CODE    <= "0000";
		BP_JUMP_CONDITION_CODE   <= "0000";
		BP_OP_DESTINATION_REG_ID <= std_logic_vector(to_unsigned(0, REG_SEL_SIZE));
		BP_OP_OPERAND_1_REG_ID   <= std_logic_vector(to_unsigned(0, REG_SEL_SIZE));
		BP_OP_OPERAND_2_REG_ID   <= std_logic_vector(to_unsigned(0, REG_SEL_SIZE));
		BP_OP_OPERAND_1_VALUE    <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE));
		BP_OP_OPERAND_2_VALUE    <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE));
		BP_OP_IMM_VALUE          <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE));

		wait for 1 us;
		assert (BP_CLOCK_BEATS = FP_CLOCK_BEATS) and (BP_PC_VALUE = FP_PC_VALUE) and (BP_INSTR = FP_INSTR) and (BP_OP_DESTINATION_REG_ID = FP_OP_DESTINATION_REG_ID) and (BP_OP_OPERAND_1_REG_ID = FP_OP_OPERAND_1_REG_ID) and (
			BP_OP_OPERAND_2_REG_ID = FP_OP_OPERAND_2_REG_ID) and (BP_OP_OPERAND_1_VALUE = FP_OP_OPERAND_1_VALUE) and (BP_OP_IMM_VALUE((ADDR_BUS_SIZE - 1) downto 0) = FP_OP_BRANCH_DESTINATION_ADDR)
			report "Error (21)"
			severity error;
		wait for 1 us;

		BP_CLOCK_BEATS           <= std_logic_vector(to_unsigned(1, BEATS_COUNTER_SIZE));
		BP_PC_VALUE              <= std_logic_vector(to_unsigned(3, ADDR_BUS_SIZE));
		BP_INSTR                 <= std_logic_vector(to_unsigned(5, DATA_BUS_SIZE));
		BP_ALU_OPERATION_CODE    <= "1010";
		BP_JUMP_CONDITION_CODE   <= "0101";
		BP_OP_DESTINATION_REG_ID <= std_logic_vector(to_unsigned(9, REG_SEL_SIZE));
		BP_OP_OPERAND_1_REG_ID   <= std_logic_vector(to_unsigned(17, REG_SEL_SIZE));
		BP_OP_OPERAND_2_REG_ID   <= std_logic_vector(to_unsigned(18, REG_SEL_SIZE));
		BP_OP_OPERAND_1_VALUE    <= std_logic_vector(to_unsigned(65, DATA_BUS_SIZE));
		BP_OP_OPERAND_2_VALUE    <= std_logic_vector(to_unsigned(129, DATA_BUS_SIZE));
		BP_OP_IMM_VALUE          <= std_logic_vector(to_unsigned(257, DATA_BUS_SIZE));

		wait for 1 us;
		assert (BP_CLOCK_BEATS = FP_CLOCK_BEATS) and (BP_PC_VALUE = FP_PC_VALUE) and (BP_INSTR = FP_INSTR) and (BP_OP_DESTINATION_REG_ID = FP_OP_DESTINATION_REG_ID) and (BP_OP_OPERAND_1_REG_ID = FP_OP_OPERAND_1_REG_ID) and (
			BP_OP_OPERAND_2_REG_ID = FP_OP_OPERAND_2_REG_ID) and (BP_OP_OPERAND_1_VALUE = FP_OP_OPERAND_1_VALUE) and (BP_OP_IMM_VALUE((ADDR_BUS_SIZE - 1) downto 0) = FP_OP_BRANCH_DESTINATION_ADDR)
			report "Error (22)"
			severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n RR 
		BP_CONTROL_SIGNALS     <= "100000";
		BP_JUMP_CONDITION_CODE <= "0000";
		BP_OP_OPERAND_1_VALUE  <= std_logic_vector(to_unsigned(10, DATA_BUS_SIZE));
		BP_OP_OPERAND_2_VALUE  <= std_logic_vector(to_unsigned(1, DATA_BUS_SIZE));
		BP_OP_IMM_VALUE        <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE));
		-- Suma
		BP_ALU_OPERATION_CODE  <= "0000";
		wait for 1 us;
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(11, DATA_BUS_SIZE)) report "Error (31)" severity error;
		wait for 1 us;
		-- Resta
		BP_ALU_OPERATION_CODE <= "0001";
		wait for 1 us;
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(9, DATA_BUS_SIZE)) report "Error (32)" severity error;
		wait for 1 us;
		-- Desplazamiento a izquierda
		BP_ALU_OPERATION_CODE <= "1100";
		wait for 1 us;
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(20, DATA_BUS_SIZE)) report "Error (33)" severity error;
		wait for 1 us;
		-- Desplazamiento a izquierda
		BP_ALU_OPERATION_CODE <= "1101";
		wait for 1 us;
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(5, DATA_BUS_SIZE)) report "Error (34)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n RI
		BP_CONTROL_SIGNALS     <= "010000";
		BP_JUMP_CONDITION_CODE <= "0000";
		BP_OP_OPERAND_1_VALUE  <= std_logic_vector(to_unsigned(10, DATA_BUS_SIZE));
		BP_OP_OPERAND_2_VALUE  <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE));
		BP_OP_IMM_VALUE        <= std_logic_vector(to_unsigned(1, DATA_BUS_SIZE));
		-- Suma
		BP_ALU_OPERATION_CODE  <= "0000";
		wait for 1 us;
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(11, DATA_BUS_SIZE)) report "Error (36)" severity error;
		wait for 1 us;
		-- Resta
		BP_ALU_OPERATION_CODE <= "0001";
		wait for 1 us;
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(9, DATA_BUS_SIZE)) report "Error (37)" severity error;
		wait for 1 us;
		-- Desplazamiento a izquierda
		BP_ALU_OPERATION_CODE <= "1100";
		wait for 1 us;
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(20, DATA_BUS_SIZE)) report "Error (38)" severity error;
		wait for 1 us;
		-- Desplazamiento a izquierda
		BP_ALU_OPERATION_CODE <= "1101";
		wait for 1 us;
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(5, DATA_BUS_SIZE)) report "Error (39)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n LOAD
		BP_CONTROL_SIGNALS     <= "001100";
		BP_ALU_OPERATION_CODE  <= "1111";
		BP_JUMP_CONDITION_CODE <= "0000";
		BP_OP_OPERAND_1_VALUE  <= std_logic_vector(to_unsigned(10, DATA_BUS_SIZE));
		BP_OP_OPERAND_2_VALUE  <= std_logic_vector(to_unsigned(20, DATA_BUS_SIZE));
		BP_OP_IMM_VALUE        <= std_logic_vector(to_unsigned(40, DATA_BUS_SIZE));
		wait for 1 us;
		-- En este caso la ALU tiene que calcular el valor del registro RS + IMM 
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(10 + 40, DATA_BUS_SIZE)) report "Error (41)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n STORE
		BP_CONTROL_SIGNALS     <= "001010";
		BP_ALU_OPERATION_CODE  <= "1111";
		BP_JUMP_CONDITION_CODE <= "0000";
		BP_OP_OPERAND_1_VALUE  <= std_logic_vector(to_unsigned(10, DATA_BUS_SIZE));
		BP_OP_OPERAND_2_VALUE  <= std_logic_vector(to_unsigned(20, DATA_BUS_SIZE));
		BP_OP_IMM_VALUE        <= std_logic_vector(to_unsigned(40, DATA_BUS_SIZE));
		wait for 1 us;
		-- En este caso la ALU tiene que calcular el valor del registro RD + IMM (para las instrucciones de LOAD/STORE el registro RT tiene tambi�n la funci�n de RD)
		assert FP_ALU_RESULT = std_logic_vector(to_unsigned(20 + 40, DATA_BUS_SIZE)) report "Error (51)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n JUMP
		BP_CONTROL_SIGNALS    <= "000001";
		BP_ALU_OPERATION_CODE <= "0000";
		BP_OP_OPERAND_2_VALUE <= std_logic_vector(to_unsigned(20, DATA_BUS_SIZE));
		BP_OP_IMM_VALUE       <= std_logic_vector(to_unsigned(40, DATA_BUS_SIZE));

		for I in 0 to (2 ** 3 - 1) loop
			BP_JUMP_CONDITION_CODE <= std_logic_vector(to_unsigned(I, 4));

			-- Verifico que detecte la condici�n de V > 0
			BP_OP_OPERAND_1_VALUE <= std_logic_vector(to_signed(1, DATA_BUS_SIZE));
			wait for 1 us;
			assert FP_BRANCH_TAKEN = BP_JUMP_CONDITION_CODE(2) report "Error (61)" severity error;
			wait for 1 us;
			-- Verifico que detecte la condici�n de V < 0
			BP_OP_OPERAND_1_VALUE <= std_logic_vector(to_signed(-1, DATA_BUS_SIZE));
			wait for 1 us;
			assert FP_BRANCH_TAKEN = BP_JUMP_CONDITION_CODE(1) report "Error 62)" severity error;
			wait for 1 us;
			-- Verifico que detecte la condici�n de V = 0
			BP_OP_OPERAND_1_VALUE <= std_logic_vector(to_signed(0, DATA_BUS_SIZE));
			wait for 1 us;
			assert FP_BRANCH_TAKEN = BP_JUMP_CONDITION_CODE(0) report "Error (63)" severity error;
			wait for 1 us;
		end loop;

		report "Fin en ensayo de la etapa EXECUTE" severity note;
		wait;

	end process;

end architecture TB_A_STAGE_EXECUTE;
