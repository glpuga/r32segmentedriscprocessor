--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_STAGE_EXECUTE is
	port(
		-- Puertos laterales (side ports)
		-- ** Esta etapa no tiene **

		-- Puertos anteriores (backward facing ports)
		BP_CLOCK_BEATS                : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci�n en esta etapa.
		BP_PC_VALUE                   : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci�n en esta etapa.
		BP_INSTR                      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucci�n a la instrucci�n en esta etapa.

		BP_OP_IS_ALU_RR               : in  STD_LOGIC; -- La clase de instrucci�n es ALU registro-registro
		BP_OP_IS_ALU_RI               : in  STD_LOGIC; -- La clase de instrucci�n es ALU registro-inmediato
		BP_OP_IS_LOAD_STORE           : in  STD_LOGIC; -- La clase de instrucci�n es LOAD o es STORE
		BP_OP_IS_LOAD                 : in  STD_LOGIC; -- La clase de instrucci�n es LOAD.
		BP_OP_IS_STORE                : in  STD_LOGIC; -- La clase de instrucci�n es STORE.
		BP_OP_IS_CONDITIONAL_JUMP     : in  STD_LOGIC; -- La clase de instrucci�n es JUMP.

		BP_ALU_OPERATION_CODE         : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c�digo de operaci�n ALU (si corresponde).
		BP_JUMP_CONDITION_CODE        : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c�digo de condici�n de salto condicional (si corresponde).

		BP_OP_DESTINATION_REG_ID      : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
		BP_OP_OPERAND_1_REG_ID        : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
		BP_OP_OPERAND_2_REG_ID        : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

		BP_OP_OPERAND_1_VALUE         : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
		BP_OP_OPERAND_2_VALUE         : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros.
		BP_OP_IMM_VALUE               : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucci�n).

		-- Puertos frontales  (forward facing ports)
		FP_CLOCK_BEATS                : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
		FP_PC_VALUE                   : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
		FP_INSTR                      : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

		FP_OP_IS_ALU_RR               : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
		FP_OP_IS_ALU_RI               : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
		FP_OP_IS_LOAD_STORE           : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
		FP_OP_IS_LOAD                 : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
		FP_OP_IS_STORE                : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
		FP_OP_IS_CONDITIONAL_JUMP     : out STD_LOGIC; -- Valor acarreado de la etapa anterior.

		FP_OP_DESTINATION_REG_ID      : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
		FP_OP_OPERAND_1_REG_ID        : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
		FP_OP_OPERAND_2_REG_ID        : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

		FP_OP_OPERAND_1_VALUE         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

		FP_ALU_RESULT                 : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Resultado de la operaci�n realizada en la ALU.
		FP_BRANCH_TAKEN               : out STD_LOGIC; -- Resultado de evaluar la condici�n de salto (si vale 1, se debe tomar la bifurcaci�n).
		FP_OP_BRANCH_DESTINATION_ADDR : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0)
	);
end entity E_STAGE_EXECUTE;

-- --------------------------------

architecture A_STAGE_EXECUTE of E_STAGE_EXECUTE is
	for all : C_ALU use entity E_ALU;
	for all : C_JEU use entity E_JEU;
	for all : C_MULTIPLEXER2 use entity E_MULTIPLEXER2;

	signal I_ALU_OPERAND_A      : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal I_ALU_OPERAND_B      : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal I_ALU_RESULT         : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal I_BRANCH_TAKEN       : STD_LOGIC;
	signal I_ALU_OPERATION_CODE : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);

	signal I_OPERAND_B_MUX_SEL : STD_LOGIC;

begin

	-- Conexi�n de las se�ales acarreadas de las etapas anteriores.
	FP_CLOCK_BEATS            <= BP_CLOCK_BEATS after TIME_DELAY_PROPAGATION;
	FP_PC_VALUE               <= BP_PC_VALUE after TIME_DELAY_PROPAGATION;
	FP_INSTR                  <= BP_INSTR after TIME_DELAY_PROPAGATION;
	FP_OP_IS_ALU_RR           <= BP_OP_IS_ALU_RR after TIME_DELAY_PROPAGATION;
	FP_OP_IS_ALU_RI           <= BP_OP_IS_ALU_RI after TIME_DELAY_PROPAGATION;
	FP_OP_IS_LOAD_STORE       <= BP_OP_IS_LOAD_STORE after TIME_DELAY_PROPAGATION;
	FP_OP_IS_LOAD             <= BP_OP_IS_LOAD after TIME_DELAY_PROPAGATION;
	FP_OP_IS_STORE            <= BP_OP_IS_STORE after TIME_DELAY_PROPAGATION;
	FP_OP_IS_CONDITIONAL_JUMP <= BP_OP_IS_CONDITIONAL_JUMP after TIME_DELAY_PROPAGATION;
	FP_OP_DESTINATION_REG_ID  <= BP_OP_DESTINATION_REG_ID after TIME_DELAY_PROPAGATION;
	FP_OP_OPERAND_1_REG_ID    <= BP_OP_OPERAND_1_REG_ID after TIME_DELAY_PROPAGATION;
	FP_OP_OPERAND_2_REG_ID    <= BP_OP_OPERAND_2_REG_ID after TIME_DELAY_PROPAGATION;
	FP_OP_OPERAND_1_VALUE     <= BP_OP_OPERAND_1_VALUE after TIME_DELAY_PROPAGATION;

	-- Esta es acarreada, pero cambia de nombre, ya que en la siguiente etapa s�lo tiene funci�n de 
	-- direcci�n destino del salto.
	FP_OP_BRANCH_DESTINATION_ADDR <= BP_OP_IMM_VALUE((ADDR_BUS_SIZE - 1) downto 0);

	-- --
	--
	-- Los operandos de la ALU son:
	--
	-- RR    : Rs + Rt
	-- RI    : Rs + IMM
	-- LOAD  : Rs + IMM
	-- STORE : Rt + IMM
	-- JUMP  : No importa, no se usa el resultado.

	I_OPERAND_B_MUX_SEL <= BP_OP_IS_ALU_RI or BP_OP_IS_LOAD_STORE;

	operand_a_multiplexer : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => DATA_BUS_SIZE
		) port map(
			-- Entradas
			IPORT00 => BP_OP_OPERAND_1_VALUE,
			IPORT01 => BP_OP_OPERAND_2_VALUE,
			SEL => BP_OP_IS_STORE,
			-- Salidas
			OPORT => I_ALU_OPERAND_A
		);

	operand_b_multiplexer : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => DATA_BUS_SIZE
		) port map(
			-- Entradas
			IPORT00 => BP_OP_OPERAND_2_VALUE,
			IPORT01 => BP_OP_IMM_VALUE,
			SEL => I_OPERAND_B_MUX_SEL,
			-- Salidas
			OPORT => I_ALU_OPERAND_B
		);

	-- --
	-- 
	-- Conexi�n de la ALU. 
	-- 
	alu : C_ALU
		port map(
			-- Entradas
			OP_CODE   => I_ALU_OPERATION_CODE,
			OPERAND_A => I_ALU_OPERAND_A,
			OPERAND_B => I_ALU_OPERAND_B,
			-- Salidas
			RESULT    => I_ALU_RESULT
		);

	-- --
	--
	-- Para las instrucciones LOAD/STORE la alu se utiliza para sumar el valor del registro RS al desplazamiento, por lo que
	-- en ese caso hay que forzar el c�digo de operaci�n que se le alimenta para que haga una suma.
	--  
	alu_opcode_multiplexer : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => 4
		) port map(
			-- Entradas
			IPORT00 => BP_ALU_OPERATION_CODE,
			IPORT01 => "0000",          -- C�digo correspondientes a "SUMA".
			SEL => BP_OP_IS_LOAD_STORE,
			-- Salidas
			OPORT => I_ALU_OPERATION_CODE
		);

	-- --
	--
	-- Conexi�n de la JEU
	-- 
	jeu : C_JEU
		port map(
			-- Entradas
			TEST_CONDITION => BP_JUMP_CONDITION_CODE,
			TEST_VALUE     => BP_OP_OPERAND_1_VALUE,
			-- Salidas
			TEST_RESULT    => I_BRANCH_TAKEN
		);

	FP_ALU_RESULT   <= I_ALU_RESULT after TIME_DELAY_PROPAGATION;
	FP_BRANCH_TAKEN <= I_BRANCH_TAKEN after TIME_DELAY_PROPAGATION;

end architecture A_STAGE_EXECUTE;
