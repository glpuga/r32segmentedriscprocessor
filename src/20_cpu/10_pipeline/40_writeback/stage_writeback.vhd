--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_STAGE_WRITEBACK is
	port(
		-- Puertos laterales (side ports)
		SP_RAM_READ_ADDR_BUS       : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones LOADs desde RAM.
		SP_RAM_READ_DATA_BUS       : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para LOADs desde RAM.

		SP_RAM_WRITE_ADDR_BUS      : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones para STOREs a la RAM.
		SP_RAM_WRITE_DATA_BUS      : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para STOREs a la RAM. 
		SP_RAM_WRITE_ENABLE        : out STD_LOGIC; -- Se�al de habilitaci�n de escritura para STOREs a la RAM.

		SP_REGISTER_WRITE_SELECTOR : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Selector del registro para escrituras al banco de registros. 
		SP_REGISTER_WRITE_DATA     : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus da datos para escrituras al banco de registros.
		SP_REGISTER_WRITE_ENABLE   : out STD_LOGIC; -- Habilitaci�n de escritura al banco de registros.

		-- Puertos anteriores (backward facing ports)
		BP_CLOCK_BEATS             : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci�n en esta etapa.
		BP_PC_VALUE                : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci�n en esta etapa.
		BP_INSTR                   : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucci�n a la instrucci�n en esta etapa.

		BP_OP_IS_ALU_RR            : in  STD_LOGIC; -- La clase de instrucci�n es ALU registro-registro
		BP_OP_IS_ALU_RI            : in  STD_LOGIC; -- La clase de instrucci�n es ALU registro-inmediato
		BP_OP_IS_LOAD_STORE        : in  STD_LOGIC; -- La clase de instrucci�n es LOAD o es STORE
		BP_OP_IS_LOAD              : in  STD_LOGIC; -- La clase de instrucci�n es LOAD.
		BP_OP_IS_STORE             : in  STD_LOGIC; -- La clase de instrucci�n es STORE.
		BP_OP_IS_CONDITIONAL_JUMP  : in  STD_LOGIC; -- La clase de instrucci�n es JUMP.

		BP_OP_DESTINATION_REG_ID   : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
		BP_OP_OPERAND_1_REG_ID     : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
		BP_OP_OPERAND_2_REG_ID     : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

		BP_OP_OPERAND_1_VALUE      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.

		BP_ALU_RESULT              : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Resultado de la operaci�n realizada en la ALU.
		BP_BRANCH_TAKEN            : in  STD_LOGIC; -- Resultado de evaluar la condici�n de salto (si vale 1, se debe tomar la bifurcaci�n).

		-- Puertos frontales  (forward facing ports)
		FP_CLOCK_BEATS             : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		FP_PC_VALUE                : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		FP_INSTR                   : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
	);
end entity E_STAGE_WRITEBACK;

-- --------------------------------
-- Definici�n de la Arquitectura --
-- --------------------------------

architecture A_STAGE_WRITEBACK of E_STAGE_WRITEBACK is
	for all : C_MULTIPLEXER2 use entity E_MULTIPLEXER2;

	signal I_REGISTER_WRITE_DATA : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

begin

	-- Conexi�n de las se�ales propagadas desde las etapas anteriores.
	FP_CLOCK_BEATS <= BP_CLOCK_BEATS after TIME_DELAY_PROPAGATION;
	FP_PC_VALUE    <= BP_PC_VALUE after TIME_DELAY_PROPAGATION;
	FP_INSTR       <= BP_INSTR after TIME_DELAY_PROPAGATION;

	-- Conexiones correspondientes a las operaciones de escritura en la memoria RAM
	SP_RAM_WRITE_ADDR_BUS <= BP_ALU_RESULT((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT) after TIME_DELAY_PROPAGATION;
	SP_RAM_WRITE_DATA_BUS <= BP_OP_OPERAND_1_VALUE after TIME_DELAY_PROPAGATION;
	SP_RAM_WRITE_ENABLE   <= BP_OP_IS_STORE after TIME_DELAY_PROPAGATION;

	-- Conexiones correspondientes a las operaciones de lectura de la RAM
	SP_RAM_READ_ADDR_BUS <= BP_ALU_RESULT((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT) after TIME_DELAY_PROPAGATION;

	-- La escritura en el banco de registros puede tener dos fuentes, dependiendo de la 
	-- instrucci�n de que se trate:
	-- * Si es RR o RI, desde el resultado de la ALU. 
	-- * Si es LOAD, desde la RAM. 
	-- * Si es un STORE o un JUMP no se va a escribir nada en el banco de registros (se controla
	--   a trav�s de la se�al de habilitaci�n de escritura), por lo que no importa qu� se ponga 
	--   en el bus de datos de escritura.
	write_multiplexer : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => DATA_BUS_SIZE
		) port map(
			IPORT00 => BP_ALU_RESULT,
			IPORT01 => SP_RAM_READ_DATA_BUS,
			OPORT => I_REGISTER_WRITE_DATA,
			SEL => BP_OP_IS_LOAD
		);

	-- Conexiones correspondientes a la escritura en el banco de registros
	SP_REGISTER_WRITE_SELECTOR <= BP_OP_DESTINATION_REG_ID after TIME_DELAY_PROPAGATION;
	SP_REGISTER_WRITE_DATA     <= I_REGISTER_WRITE_DATA after TIME_DELAY_PROPAGATION;
	SP_REGISTER_WRITE_ENABLE   <= BP_OP_IS_ALU_RR or BP_OP_IS_ALU_RI or BP_OP_IS_LOAD after TIME_DELAY_PROPAGATION;

end architecture A_STAGE_WRITEBACK;
