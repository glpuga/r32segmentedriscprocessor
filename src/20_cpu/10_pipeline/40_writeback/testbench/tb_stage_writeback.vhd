--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_STAGE_WRITEBACK is
-- body
end entity TB_E_STAGE_WRITEBACK;

-- --------------------------------

architecture TB_A_STAGE_WRITEBACK of TB_E_STAGE_WRITEBACK is
	for all : C_STAGE_WRITEBACK use entity E_STAGE_WRITEBACK;

	signal SP_RAM_READ_ADDR_BUS       : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 2); -- Bus de direcciones LOADs desde RAM.
	signal SP_RAM_READ_DATA_BUS       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para LOADs desde RAM.
	signal SP_RAM_WRITE_ADDR_BUS      : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 2); -- Bus de direcciones para STOREs a la RAM.
	signal SP_RAM_WRITE_DATA_BUS      : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para STOREs a la RAM. 
	signal SP_RAM_WRITE_ENABLE        : STD_LOGIC; -- Se�al de habilitaci�n de escritura para STOREs a la RAM.
	signal SP_REGISTER_WRITE_SELECTOR : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Selector del registro para escrituras al banco de registros. 
	signal SP_REGISTER_WRITE_DATA     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus da datos para escrituras al banco de registros.
	signal SP_REGISTER_WRITE_ENABLE   : STD_LOGIC; -- Habilitaci�n de escritura al banco de registros.

	signal BP_CLOCK_BEATS            : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci�n en esta etapa.
	signal BP_PC_VALUE               : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci�n en esta etapa.
	signal BP_INSTR                  : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucci�n a la instrucci�n en esta etapa.
	signal BP_OP_IS_ALU_RR           : STD_LOGIC; -- La clase de instrucci�n es ALU registro-registro
	signal BP_OP_IS_ALU_RI           : STD_LOGIC; -- La clase de instrucci�n es ALU registro-inmediato
	signal BP_OP_IS_LOAD_STORE       : STD_LOGIC; -- La clase de instrucci�n es LOAD o es STORE
	signal BP_OP_IS_LOAD             : STD_LOGIC; -- La clase de instrucci�n es LOAD.
	signal BP_OP_IS_STORE            : STD_LOGIC; -- La clase de instrucci�n es STORE.
	signal BP_OP_IS_CONDITIONAL_JUMP : STD_LOGIC; -- La clase de instrucci�n es JUMP.
	signal BP_OP_DESTINATION_REG_ID  : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal BP_OP_OPERAND_1_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal BP_OP_OPERAND_2_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
	signal BP_OP_OPERAND_1_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
	signal BP_ALU_RESULT             : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Resultado de la operaci�n realizada en la ALU.
	signal BP_BRANCH_TAKEN           : STD_LOGIC; -- Resultado de evaluar la condici�n de salto (si vale 1, se debe tomar la bifurcaci�n).

	signal FP_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
	signal FP_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal FP_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	signal BP_CONTROL_SIGNALS : STD_LOGIC_VECTOR(5 downto 0);

begin

	-- Utilizo este vector de se�ales para manejar las se�ales de control de forma m�s pr�ctica
	BP_OP_IS_ALU_RR           <= BP_CONTROL_SIGNALS(5);
	BP_OP_IS_ALU_RI           <= BP_CONTROL_SIGNALS(4);
	BP_OP_IS_LOAD_STORE       <= BP_CONTROL_SIGNALS(3);
	BP_OP_IS_LOAD             <= BP_CONTROL_SIGNALS(2);
	BP_OP_IS_STORE            <= BP_CONTROL_SIGNALS(1);
	BP_OP_IS_CONDITIONAL_JUMP <= BP_CONTROL_SIGNALS(0);

	unit_under_test : C_STAGE_WRITEBACK port map(
			SP_RAM_READ_ADDR_BUS       => SP_RAM_READ_ADDR_BUS,
			SP_RAM_READ_DATA_BUS       => SP_RAM_READ_DATA_BUS,
			SP_RAM_WRITE_ADDR_BUS      => SP_RAM_WRITE_ADDR_BUS,
			SP_RAM_WRITE_DATA_BUS      => SP_RAM_WRITE_DATA_BUS,
			SP_RAM_WRITE_ENABLE        => SP_RAM_WRITE_ENABLE,
			SP_REGISTER_WRITE_SELECTOR => SP_REGISTER_WRITE_SELECTOR,
			SP_REGISTER_WRITE_DATA     => SP_REGISTER_WRITE_DATA,
			SP_REGISTER_WRITE_ENABLE   => SP_REGISTER_WRITE_ENABLE,
			BP_CLOCK_BEATS             => BP_CLOCK_BEATS,
			BP_PC_VALUE                => BP_PC_VALUE,
			BP_INSTR                   => BP_INSTR,
			BP_OP_IS_ALU_RR            => BP_OP_IS_ALU_RR,
			BP_OP_IS_ALU_RI            => BP_OP_IS_ALU_RI,
			BP_OP_IS_LOAD_STORE        => BP_OP_IS_LOAD_STORE,
			BP_OP_IS_LOAD              => BP_OP_IS_LOAD,
			BP_OP_IS_STORE             => BP_OP_IS_STORE,
			BP_OP_IS_CONDITIONAL_JUMP  => BP_OP_IS_CONDITIONAL_JUMP,
			BP_OP_DESTINATION_REG_ID   => BP_OP_DESTINATION_REG_ID,
			BP_OP_OPERAND_1_REG_ID     => BP_OP_OPERAND_1_REG_ID,
			BP_OP_OPERAND_2_REG_ID     => BP_OP_OPERAND_2_REG_ID,
			BP_OP_OPERAND_1_VALUE      => BP_OP_OPERAND_1_VALUE,
			BP_ALU_RESULT              => BP_ALU_RESULT,
			BP_BRANCH_TAKEN            => BP_BRANCH_TAKEN,
			FP_CLOCK_BEATS             => FP_CLOCK_BEATS,
			FP_PC_VALUE                => FP_PC_VALUE,
			FP_INSTR                   => FP_INSTR
		);

	test_process : process
	begin
		report "Comienzo en ensayo de la etapa WRITEBACK" severity note;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Verifico las se�ales que tienen que atravesar la etapa
		BP_CLOCK_BEATS <= std_logic_vector(to_unsigned(0, BEATS_COUNTER_SIZE));
		BP_PC_VALUE    <= std_logic_vector(to_unsigned(0, ADDR_BUS_SIZE));
		BP_INSTR       <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE));

		wait for 1 us;
		assert (BP_CLOCK_BEATS = FP_CLOCK_BEATS) and (BP_PC_VALUE = FP_PC_VALUE) and (BP_INSTR = FP_INSTR)
			report "Error (21)"
			severity error;
		wait for 1 us;

		BP_CLOCK_BEATS <= std_logic_vector(to_unsigned(1, BEATS_COUNTER_SIZE));
		BP_PC_VALUE    <= std_logic_vector(to_unsigned(3, ADDR_BUS_SIZE));
		BP_INSTR       <= std_logic_vector(to_unsigned(5, DATA_BUS_SIZE));

		wait for 1 us;
		assert (BP_CLOCK_BEATS = FP_CLOCK_BEATS) and (BP_PC_VALUE = FP_PC_VALUE) and (BP_INSTR = FP_INSTR)
			report "Error (22)"
			severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		BP_OP_DESTINATION_REG_ID <= std_logic_vector(to_unsigned(1, REG_SEL_SIZE));
		BP_OP_OPERAND_1_REG_ID   <= std_logic_vector(to_unsigned(2, REG_SEL_SIZE));
		BP_OP_OPERAND_2_REG_ID   <= std_logic_vector(to_unsigned(4, REG_SEL_SIZE));
		BP_OP_OPERAND_1_VALUE    <= std_logic_vector(to_unsigned(8, DATA_BUS_SIZE));
		BP_ALU_RESULT            <= std_logic_vector(to_unsigned(16, DATA_BUS_SIZE));
		SP_RAM_READ_DATA_BUS     <= std_logic_vector(to_unsigned(32, DATA_BUS_SIZE));

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n RR
		-- En este caso el resultado de la ALU tiene que enviarse al banco de registros
		BP_CONTROL_SIGNALS <= "100000";
		BP_BRANCH_TAKEN    <= '0';
		wait for 1 us;
		assert SP_RAM_WRITE_ENABLE = '0'
			report "Error (11)" severity error;
		assert SP_REGISTER_WRITE_SELECTOR = BP_OP_DESTINATION_REG_ID
			report "Error (12)" severity error;
		assert SP_REGISTER_WRITE_DATA = BP_ALU_RESULT
			report "Error (13)" severity error;
		assert SP_REGISTER_WRITE_ENABLE = '1'
			report "Error (14)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n RI
		-- En este caso el resultado de la ALU tiene que enviarse al banco de registros
		BP_CONTROL_SIGNALS <= "010000";
		BP_BRANCH_TAKEN    <= '0';
		wait for 1 us;
		assert SP_RAM_WRITE_ENABLE = '0'
			report "Error (21)" severity error;
		assert SP_REGISTER_WRITE_SELECTOR = BP_OP_DESTINATION_REG_ID
			report "Error (22)" severity error;
		assert SP_REGISTER_WRITE_DATA = BP_ALU_RESULT
			report "Error (23)" severity error;
		assert SP_REGISTER_WRITE_ENABLE = '1'
			report "Error (24)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n LOAD
		-- En este caso se debe realizar una lectura desde la RAM. La direcci�n de dicha lectura
		-- es la direcci�n calculada en la ALU. Dicha escritura tiene que ser enviada 
		-- al banco de registros al registro apuntado por Rd.
		BP_CONTROL_SIGNALS <= "001100";
		BP_BRANCH_TAKEN    <= '0';
		wait for 1 us;
		assert SP_RAM_READ_ADDR_BUS = BP_ALU_RESULT((ADDR_BUS_SIZE - 1) downto 2)
			report "Error (31)" severity error;
		assert SP_RAM_WRITE_ENABLE = '0'
			report "Error (32)" severity error;
		assert SP_REGISTER_WRITE_SELECTOR = BP_OP_DESTINATION_REG_ID
			report "Error (33)" severity error;
		assert SP_REGISTER_WRITE_DATA = SP_RAM_READ_DATA_BUS
			report "Error (34)" severity error;
		assert SP_REGISTER_WRITE_ENABLE = '1'
			report "Error (35)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n STORE
		-- En este caso se debe realizar una escritura a la RAM. La direcci�n de dicha escritura
		-- es la direcci�n calculada en la ALU. El valor a escribir el es contenido del registro Rs
		-- No se opera con el banco de registros en este caso.  
		BP_CONTROL_SIGNALS <= "001010";
		BP_BRANCH_TAKEN    <= '0';
		wait for 1 us;
		assert SP_RAM_WRITE_ADDR_BUS = BP_ALU_RESULT((ADDR_BUS_SIZE - 1) downto 2)
			report "Error (41)" severity error;
		assert SP_RAM_WRITE_DATA_BUS = BP_OP_OPERAND_1_VALUE
			report "Error (42)" severity error;
		assert SP_RAM_WRITE_ENABLE = '1'
			report "Error (43)" severity error;
		assert SP_REGISTER_WRITE_ENABLE = '0'
			report "Error (44)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		-- Ensayo una operaci�n JUMP
		-- En este caso esta etapa no debe hacer nada. 
		BP_CONTROL_SIGNALS <= "000001";
		BP_BRANCH_TAKEN    <= '0';
		wait for 1 us;
		assert SP_RAM_WRITE_ENABLE = '0'
			report "Error (51)" severity error;
		assert SP_REGISTER_WRITE_ENABLE = '0'
			report "Error (52)" severity error;
		wait for 1 us;

		-- -----------------------------------------------
		-- -----------------------------------------------
		-- -----------------------------------------------

		report "Fin en ensayo de la etapa WRITEBACK" severity note;
		wait;

	end process;

end architecture TB_A_STAGE_WRITEBACK;


