--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_REGISTER_FEDE is
	port(
		-- Puertos laterales (side ports)
		SP_SYNC_STALL         : in  STD_LOGIC; -- Inhibici�n de avance.
		SP_SYNC_INSERT_BUBBLE : in  STD_LOGIC; -- Clear sincr�nico.
		SP_ASYNC_RESET        : in  STD_LOGIC; -- Reset asincr�nico.
		SP_CLOCK              : in  STD_LOGIC; -- Se�al de reloj, activo en flanco ascendente.

		-- Puertos hacia la etapa de FETCH
		BP_CLOCK_BEATS        : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado a la etapa siguiente, corresponde a BP_CLOCK_BEATS
		BP_PC_VALUE           : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado a la etapa siguiente, corresponde a BP_PC_VALUE
		BP_INSTR              : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Instrucci�n a ejecutar.

		-- Puertos hacia la etapa de DECODE
		FP_CLOCK_BEATS        : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		FP_PC_VALUE           : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		FP_INSTR              : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
	);
end entity E_REGISTER_FEDE;

architecture A_REGISTER_FEDE of E_REGISTER_FEDE is
	for all : C_INTERSTAGE_REGISTER use entity E_INTERSTAGE_REGISTER;

	signal I_SYNC_STALL         : STD_LOGIC;
	signal I_SYNC_INSERT_BUBBLE : STD_LOGIC;
	signal I_ASYNC_RESET        : STD_LOGIC;
	signal I_CLOCK              : STD_LOGIC;

begin
	I_SYNC_STALL         <= SP_SYNC_STALL;
	I_SYNC_INSERT_BUBBLE <= SP_SYNC_INSERT_BUBBLE;
	I_ASYNC_RESET        <= SP_ASYNC_RESET;
	I_CLOCK              <= SP_CLOCK;

	rBeatsCounter : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => BEATS_COUNTER_SIZE)
		port map(
			DATAIN       => BP_CLOCK_BEATS,
			DATAOUT      => FP_CLOCK_BEATS,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rPCValue : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => ADDR_BUS_SIZE)
		port map(
			DATAIN       => BP_PC_VALUE,
			DATAOUT      => FP_PC_VALUE,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rInstr : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => DATA_BUS_SIZE)
		port map(
			DATAIN       => BP_INSTR,
			DATAOUT      => FP_INSTR,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

end architecture A_REGISTER_FEDE;
