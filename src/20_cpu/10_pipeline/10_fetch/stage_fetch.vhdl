--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

entity E_STAGE_FETCH is
	port(
		-- Puertos laterales (side ports)
		SP_PROG_MEM_ADDR  : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones de acceso a la RAM
		SP_PROG_MEM_INSTR : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos de acceso a la RAM, para leer la instrucci�n siguiente. 

		-- Puertos anteriores (backward facing ports)
		BP_CLOCK_BEATS    : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci�n en esta etapa.
		BP_PC_VALUE       : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci�n en esta etapa. 

		-- Puertos frontales  (forward facing ports)
		FP_CLOCK_BEATS    : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado a la etapa siguiente, corresponde a BP_CLOCK_BEATS
		FP_PC_VALUE       : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado a la etapa siguiente, corresponde a BP_PC_VALUE

		FP_INSTR          : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0) -- Instrucci�n a ejecutar. 
	);
-- body
end entity E_STAGE_FETCH;

-- --------------------------------

architecture A_STAGE_FETCH of E_STAGE_FETCH is
begin

	-- Conexi�n de los valores que deben ser acarreados hasta la etapa siguiente. 
	FP_CLOCK_BEATS <= BP_CLOCK_BEATS after TIME_DELAY_PROPAGATION;
	FP_PC_VALUE    <= BP_PC_VALUE after TIME_DELAY_PROPAGATION;

	-- Conexiones a y desde la memoria de instrucciones.
	SP_PROG_MEM_ADDR <= BP_PC_VALUE((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT) after TIME_DELAY_PROPAGATION;
	FP_INSTR         <= SP_PROG_MEM_INSTR after TIME_DELAY_PROPAGATION;

end architecture A_STAGE_FETCH;
