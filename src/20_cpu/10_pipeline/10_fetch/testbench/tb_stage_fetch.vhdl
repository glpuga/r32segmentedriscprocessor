--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_STAGE_FETCH is
-- body
end entity TB_E_STAGE_FETCH;

-- --------------------------------

architecture TB_A_STAGE_FETCH of TB_E_STAGE_FETCH is
	for all : C_STAGE_FETCH use entity E_STAGE_FETCH;

	-- --
	--
	-- Se�ales locales
	-- 
	-- --

	-- Puertos laterales (side ports)
	signal SP_PROG_MEM_INSTR : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal SP_PROG_MEM_ADDR  : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 2);

	-- Puertos anteriores (backward facing ports)
	signal BP_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
	signal BP_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

	-- Puertos frontales 
	signal FP_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
	signal FP_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal FP_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

begin
	stage_under_test : C_STAGE_FETCH
		port map(
			SP_PROG_MEM_ADDR  => SP_PROG_MEM_ADDR,
			SP_PROG_MEM_INSTR => SP_PROG_MEM_INSTR,
			BP_CLOCK_BEATS    => BP_CLOCK_BEATS,
			BP_PC_VALUE       => BP_PC_VALUE,
			FP_CLOCK_BEATS    => FP_CLOCK_BEATS,
			FP_PC_VALUE       => FP_PC_VALUE,
			FP_INSTR          => FP_INSTR
		);

	test_process : process
	begin
		report "Comienzo en ensayo de multiplexor" severity note;
		wait for 1 us;

		-- Coloco valores en las entradas de la etapa 
		BP_PC_VALUE       <= x"0ffff";
		BP_CLOCK_BEATS    <= x"00000001";
		SP_PROG_MEM_INSTR <= x"abcdabcd";
		wait for 1 us;

		-- Verifico las salidas
		assert FP_PC_VALUE = BP_PC_VALUE report "Error (1)" severity error;
		assert FP_CLOCK_BEATS = BP_CLOCK_BEATS report "Error (2)" severity error;
		assert FP_INSTR = SP_PROG_MEM_INSTR report "Error (3)" severity error;
		assert SP_PROG_MEM_ADDR = BP_PC_VALUE((ADDR_BUS_SIZE - 1) downto 2) report "Error (4)" severity error;
		wait for 1 us;

		-- Cambio los valores de las entradas. 
		BP_PC_VALUE       <= x"ffff0";
		BP_CLOCK_BEATS    <= x"11341234";
		SP_PROG_MEM_INSTR <= x"31234123";
		wait for 1 us;

		-- Verifico las salidas
		assert FP_PC_VALUE = BP_PC_VALUE report "Error (5)" severity error;
		assert FP_CLOCK_BEATS = BP_CLOCK_BEATS report "Error (6)" severity error;
		assert FP_INSTR = SP_PROG_MEM_INSTR report "Error (7)" severity error;
		assert SP_PROG_MEM_ADDR = BP_PC_VALUE((ADDR_BUS_SIZE - 1) downto 2) report "Error (8)" severity error;
		wait for 1 us;

		report "Fin en ensayo de etapa FETCH" severity note;
		wait;

	end process;

end architecture TB_A_STAGE_FETCH;
