--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_STAGE_DECODE is
	port(
		-- Puertos laterales (side ports)
		SP_REGISTER_RS_SELECTOR   : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
		SP_REGISTER_RT_SELECTOR   : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
		SP_REGISTER_RS_VALUE      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor le�do del registro RS
		SP_REGISTER_RT_VALUE      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor le�do del registro RT

		-- Puertos anteriores (backward facing ports)
		BP_CLOCK_BEATS            : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci�n en esta etapa.
		BP_PC_VALUE               : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci�n en esta etapa.
		BP_INSTR                  : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucci�n a la instrucci�n en esta etapa.

		-- Puertos frontales  (forward facing ports)
		FP_CLOCK_BEATS            : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
		FP_PC_VALUE               : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.  
		FP_INSTR                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

		FP_OP_IS_ALU_RR           : out STD_LOGIC; -- La clase de instrucci�n es ALU registro-registro
		FP_OP_IS_ALU_RI           : out STD_LOGIC; -- La clase de instrucci�n es ALU registro-inmediato
		FP_OP_IS_LOAD_STORE       : out STD_LOGIC; -- La clase de instrucci�n es LOAD o es STORE
		FP_OP_IS_LOAD             : out STD_LOGIC; -- La clase de instrucci�n es LOAD.
		FP_OP_IS_STORE            : out STD_LOGIC; -- La clase de instrucci�n es STORE.
		FP_OP_IS_CONDITIONAL_JUMP : out STD_LOGIC; -- La clase de instrucci�n es JUMP.

		FP_ALU_OPERATION_CODE     : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c�digo de operaci�n ALU (si corresponde).
		FP_JUMP_CONDITION_CODE    : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c�digo de condici�n de salto condicional (si corresponde).

		FP_OP_DESTINATION_REG_ID  : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
		FP_OP_OPERAND_1_REG_ID    : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
		FP_OP_OPERAND_2_REG_ID    : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

		FP_OP_OPERAND_1_VALUE     : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
		FP_OP_OPERAND_2_VALUE     : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros. 
		FP_OP_IMM_VALUE           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0) -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucci�n). 
	);
end entity E_STAGE_DECODE;

-- --------------------------------

architecture A_STAGE_DECODE of E_STAGE_DECODE is
	for all : C_WORDEXTENDER use entity E_WORDEXTENDER;
	for all : C_CONTROLUNIT use entity E_CONTROLUNIT;
	for all : C_MULTIPLEXER2 use entity E_MULTIPLEXER2;

	-- ------------------------------------------

	-- --
	--
	-- Declaraci�n de las se�ales internas
	signal I_ICLASS : STD_LOGIC_VECTOR((CLASS_FIELD_SIZE - 1) downto 0); -- Campo clase de instrucci�n, extraido de la instrucci�n.
	signal I_ICODE  : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Campo c�digo de operaci�n, extraido de la instrucci�n.
	signal I_IMM16  : STD_LOGIC_VECTOR(15 downto 0); -- Campo operando inmediato, extraido de la instrucci�n.
	signal I_ADDR21 : STD_LOGIC_VECTOR(20 downto 0); -- Campo direcci�n de salto condicional, extraido de la instrucci�n.

	signal I_FIELD_RD : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Campo identificador del registro RD, extra�do de la instrucci�n.
	signal I_FIELD_RS : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Campo identificador del registro RS, extra�do de la instrucci�n.
	signal I_FIELD_RT : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Campo identificador del registro RT, extra�do de la instrucci�n.

	signal I_OPERAND_1_REG_ID : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Campo identificador del registro RS, extra�do de la instrucci�n.
	signal I_OPERAND_2_REG_ID : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Campo identificador del registro RT, extra�do de la instrucci�n.

	signal I_ADDR32 : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Direcci�n del salto condicional, extendida SIN SIGNO al ancho de la ALU.
	signal I_IMM32  : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato, extendido CON SIGNO al ancho de palabra de la ALU.

	signal I_OP_IS_ALU_RR           : STD_LOGIC; -- La clase de instrucci�n es ALU registro-registro
	signal I_OP_IS_ALU_RI           : STD_LOGIC; -- La clase de instrucci�n es ALU registro-inmediato
	signal I_OP_IS_LOAD_STORE       : STD_LOGIC; -- La clase de instrucci�n es LOAD o es STORE
	signal I_OP_IS_LOAD             : STD_LOGIC; -- La clase de instrucci�n es LOAD.
	signal I_OP_IS_STORE            : STD_LOGIC; -- La clase de instrucci�n es STORE.
	signal I_OP_IS_CONDITIONAL_JUMP : STD_LOGIC; -- La clase de instrucci�n es JUMP.

	signal I_ALU_OPERATION_CODE  : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c�digo de operaci�n ALU (si corresponde).
	signal I_JUMP_CONDITION_CODE : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c�digo de condici�n del salto condicional (si corresponde).

	signal I_OPERAND_1_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
	signal I_OPERAND_2_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros.
	signal I_VALUE_IMM       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucci�n).

begin

	-- -- 
	-- 
	-- Extracci�n de los campos de la palabra de instrucci�n
	--
	-- Los formatos posibles para la palabra de instrucci�n son:
	--  * Formato R : Utilizado para instrucciones de clase ALU registro-registro.
	--  * Formato I : Utilizado para instrucciones de clase ALU registro-inmediato, y para LOAD/STORE. 
	--  * Formato J : Utilizado para las instrucciones de salto condicional.
	-- 
	I_ICLASS   <= BP_INSTR(31 downto 30); -- Campo de clase de instrucci�n (Val�do para formatos R, I y J).
	I_ICODE    <= BP_INSTR(29 downto 26); -- C�digo de operaci�n (Val�do para formatos R, I y J, pero cambia su interpretaci�n seg�n la clase). 
	I_FIELD_RS <= BP_INSTR(25 downto 21); -- Registro fuente (Val�do para formatos R, I y J).
	I_FIELD_RD <= BP_INSTR(20 downto 16); -- Registro destino (Val�do para formatos R e I).
	I_FIELD_RT <= BP_INSTR(4 downto 0); -- Registro auxiliar (Val�do para formatos R).
	I_IMM16    <= BP_INSTR(15 downto 0); -- Operando inmediato (Val�do para formatos I).
	I_ADDR21   <= BP_INSTR(20 downto 0); -- Direcci�n de destino del salto (Val�do para formatos J).

	-- El operando 1 de origen 1 siempre es el registro RS
	I_OPERAND_1_REG_ID <= I_FIELD_RS;

	-- El operando 2 es RT para las instrucciones en formato ALU RR, para las load/store es RD y para las dem�s no importa. 
	operand_2_mux : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => REG_SEL_SIZE
		) port map(
			-- Entradas
			IPORT00 => I_FIELD_RD,
			IPORT01 => I_FIELD_RT,
			SEL => I_OP_IS_ALU_RR,
			-- Salidas
			OPORT => I_OPERAND_2_REG_ID
		);

	-- --
	--
	-- Extensi�n en signo en signo del operando inmediato para formar una palabra de 32 bits 
	--
	-- Este operando se extiende con signo, para poder operar con n�meros positivos y negativos. 
	-- Para instrucciones RR y RI esto significa que todas las operaciones son operaciones con signo. 
	-- Para instrucciones de LOAD/STORE esto significa que el direccionamiento relativo puede sumar o restar
	-- al valor del registro RS. 
	imm_word_extender : C_WORDEXTENDER generic map(
			INPUT_WIDTH  => 16,
			OUTPUT_WIDTH => DATA_BUS_SIZE
		)
		port map(
			-- Entradas
			DATAIN     => I_IMM16,
			-- Salidas
			DATAOUT    => I_IMM32,
			ISUNSIGNED => '0'
		);

	-- --
	--
	-- Extensi�n de la direcci�n del salto condicional para formar una palabra de 32 bits. Este
	-- operando se extiende sin signo porque es una direcci�n absoluta de memoria. 
	--
	addr_word_extender : C_WORDEXTENDER generic map(
			INPUT_WIDTH  => 21,
			OUTPUT_WIDTH => DATA_BUS_SIZE
		)
		port map(
			-- Entradas
			DATAIN     => I_ADDR21,
			-- Salidas
			DATAOUT    => I_ADDR32,
			ISUNSIGNED => '1'
		);

	-- --
	-- 
	-- Conexi�n de la unidad de control que realiza la decodificaci�n del c�digo de la instrucci�n 
	-- (formado por la clase y el c�digo de la instrucci�n) y genera la se�alizaci�n necesaria para 
	-- activar las unidades funcionales posteriores.
	--
	control_unit : C_CONTROLUNIT
		port map(
			-- Entradas
			ICLASS              => I_ICLASS,
			ICODE               => I_ICODE,
			-- Salidas
			OP_IS_ALU_RR        => I_OP_IS_ALU_RR,
			OP_IS_ALU_RI        => I_OP_IS_ALU_RI,
			OP_IS_LOAD_STORE    => I_OP_IS_LOAD_STORE,
			OP_IS_LOAD          => I_OP_IS_LOAD,
			OP_IS_STORE         => I_OP_IS_STORE,
			OP_IS_C_JUMP        => I_OP_IS_CONDITIONAL_JUMP,
			ALU_OPERATION_CODE  => I_ALU_OPERATION_CODE,
			JUMP_CONDITION_CODE => I_JUMP_CONDITION_CODE
		);

	-- --
	--
	-- Para no tener dos puertos con valores inmediatos a la salida que luego deberan multiplexados 
	-- en la etapa de ejecuci�n, multiplexo en esta misma etapa y utilizo un �nico puerto 
	-- de salida para el operando inmediato de las instrucciones RI, LOAD y STORE, y la direcci�n
	-- de salto de las instrucciones JUMP.
	--
	imm_addr_multiplexer : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => DATA_BUS_SIZE
		) port map(
			-- Entradas
			IPORT00 => I_IMM32,
			IPORT01 => I_ADDR32,
			SEL => I_OP_IS_CONDITIONAL_JUMP,
			-- Salidas
			OPORT => I_VALUE_IMM
		);

	-- --
	--
	-- Conexi�n de las se�ales que van al banco de registro y vuelven con los valores d
	-- de los registros RS y RT
	SP_REGISTER_RS_SELECTOR <= I_OPERAND_1_REG_ID after TIME_DELAY_PROPAGATION;

	SP_REGISTER_RT_SELECTOR <= I_OPERAND_2_REG_ID after TIME_DELAY_PROPAGATION;

	-- -- 
	--
	-- Anulo el valor del registro cuando el registro accedido es R0
	I_OPERAND_1_VALUE <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE)) when I_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(0, REG_SEL_SIZE)) else SP_REGISTER_RS_VALUE;
	I_OPERAND_2_VALUE <= std_logic_vector(to_unsigned(0, DATA_BUS_SIZE)) when I_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(0, REG_SEL_SIZE)) else SP_REGISTER_RT_VALUE;

	-- --
	-- 
	-- Conexi�n de las se�ales que deben ser acarreadas a las etapas posteriores.  
	--
	-- Se�ales acarredas desde las etapas anteriores.
	FP_CLOCK_BEATS <= BP_CLOCK_BEATS after TIME_DELAY_PROPAGATION;
	FP_PC_VALUE    <= BP_PC_VALUE after TIME_DELAY_PROPAGATION;
	FP_INSTR       <= BP_INSTR after TIME_DELAY_PROPAGATION;

	-- Se�ales agregadas por esta etapa.
	FP_OP_IS_ALU_RR           <= I_OP_IS_ALU_RR after TIME_DELAY_PROPAGATION;
	FP_OP_IS_ALU_RI           <= I_OP_IS_ALU_RI after TIME_DELAY_PROPAGATION;
	FP_OP_IS_LOAD_STORE       <= I_OP_IS_LOAD_STORE after TIME_DELAY_PROPAGATION;
	FP_OP_IS_LOAD             <= I_OP_IS_LOAD after TIME_DELAY_PROPAGATION;
	FP_OP_IS_STORE            <= I_OP_IS_STORE after TIME_DELAY_PROPAGATION;
	FP_OP_IS_CONDITIONAL_JUMP <= I_OP_IS_CONDITIONAL_JUMP after TIME_DELAY_PROPAGATION;
	FP_ALU_OPERATION_CODE     <= I_ALU_OPERATION_CODE after TIME_DELAY_PROPAGATION;
	FP_JUMP_CONDITION_CODE    <= I_JUMP_CONDITION_CODE after TIME_DELAY_PROPAGATION;
	FP_OP_DESTINATION_REG_ID  <= I_FIELD_RD after TIME_DELAY_PROPAGATION;
	FP_OP_OPERAND_1_REG_ID    <= I_OPERAND_1_REG_ID after TIME_DELAY_PROPAGATION;
	FP_OP_OPERAND_2_REG_ID    <= I_OPERAND_2_REG_ID after TIME_DELAY_PROPAGATION;
	FP_OP_OPERAND_1_VALUE     <= I_OPERAND_1_VALUE after TIME_DELAY_PROPAGATION;
	FP_OP_OPERAND_2_VALUE     <= I_OPERAND_2_VALUE after TIME_DELAY_PROPAGATION;
	FP_OP_IMM_VALUE           <= I_VALUE_IMM after TIME_DELAY_PROPAGATION;

end architecture A_STAGE_DECODE;
