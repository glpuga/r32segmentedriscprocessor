--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_CONTROLUNIT is
-- body
end entity TB_E_CONTROLUNIT;

-- --------------------------------

architecture TB_A_CONTROLUNIT of TB_E_CONTROLUNIT is
	for all : C_CONTROLUNIT use entity E_CONTROLUNIT;

	-- Se�ales locales
	signal ICLASS : STD_LOGIC_VECTOR(1 downto 0);
	signal ICODE  : STD_LOGIC_VECTOR(3 downto 0);

	signal INPUT : STD_LOGIC_VECTOR(5 downto 0);

	signal OP_IS_ALU_RR        : STD_LOGIC;
	signal OP_IS_ALU_RI        : STD_LOGIC;
	signal OP_IS_LOAD_STORE    : STD_LOGIC;
	signal OP_IS_LOAD          : STD_LOGIC;
	signal OP_IS_STORE         : STD_LOGIC;
	signal OP_IS_C_JUMP        : STD_LOGIC;
	signal ALU_OPERATION_CODE  : STD_LOGIC_VECTOR(3 downto 0);
	signal JUMP_CONDITION_CODE : STD_LOGIC_VECTOR(3 downto 0);

begin
	ICLASS <= INPUT(5 downto 4);
	ICODE  <= INPUT(3 downto 0);

	control_unit : C_CONTROLUNIT port map(
			ICLASS              => ICLASS,
			ICODE               => ICODE,
			OP_IS_ALU_RR        => OP_IS_ALU_RR,
			OP_IS_ALU_RI        => OP_IS_ALU_RI,
			OP_IS_LOAD_STORE    => OP_IS_LOAD_STORE,
			OP_IS_LOAD          => OP_IS_LOAD,
			OP_IS_STORE         => OP_IS_STORE,
			OP_IS_C_JUMP        => OP_IS_C_JUMP,
			ALU_OPERATION_CODE  => ALU_OPERATION_CODE,
			JUMP_CONDITION_CODE => JUMP_CONDITION_CODE
		);

	test_process : process
	begin
		report "Comienzo en ensayo de la unidad de control" severity note;
		wait for 1 us;

		-- RR,    ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "000000";
		wait for 1 us;

		assert OP_IS_ALU_RR = '1' report "Error(1)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error(1)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error(1)" severity error;
		assert OP_IS_LOAD = '0' report "Error(1)" severity error;
		assert OP_IS_STORE = '0' report "Error(1)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error(1)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error(1)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error(1)" severity error;
		wait for 1 us;

		-- RR,    ALU_CODE = "0001", JUMP_CODE = "0000"
		INPUT <= "000001";
		wait for 1 us;

		assert OP_IS_ALU_RR = '1' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0001" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- RR,    ALU_CODE = "0011", JUMP_CODE = "0000"
		INPUT <= "000011";
		wait for 1 us;

		assert OP_IS_ALU_RR = '1' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0011" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- RR,    ALU_CODE = "0111", JUMP_CODE = "0000"
		INPUT <= "000111";
		wait for 1 us;

		assert OP_IS_ALU_RR = '1' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0111" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- RR,    ALU_CODE = "1111", JUMP_CODE = "0000"
		INPUT <= "001111";
		wait for 1 us;

		assert OP_IS_ALU_RR = '1' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "1111" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- RI,    ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "010000";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- RI,    ALU_CODE = "0001", JUMP_CODE = "0000"
		INPUT <= "010001";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0001" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- RI,    ALU_CODE = "0011", JUMP_CODE = "0000"
		INPUT <= "010011";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0011" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- RI,    ALU_CODE = "0111", JUMP_CODE = "0000"
		INPUT <= "010111";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0111" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- RI,    ALU_CODE = "1111", JUMP_CODE = "0000"
		INPUT <= "011111";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "1111" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- LOAD,  ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "100000";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD = '1' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- LOAD,  ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "100010";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD = '1' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- LOAD,  ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "100110";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD = '1' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- LOAD,  ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "101110";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD = '1' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- STORE, ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "100001";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- STORE, ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "100011";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- STORE, ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "100111";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- STORE, ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "101111";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '1' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '0' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- JUMP,  ALU_CODE = "0000", JUMP_CODE = "0000"
		INPUT <= "110000";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '1' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0000" report "Error (2)" severity error;
		wait for 1 us;

		-- JUMP,  ALU_CODE = "0000", JUMP_CODE = "0001"
		INPUT <= "110001";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '1' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0001" report "Error (2)" severity error;
		wait for 1 us;

		-- JUMP,  ALU_CODE = "0000", JUMP_CODE = "0011"
		INPUT <= "110011";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '1' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0011" report "Error (2)" severity error;
		wait for 1 us;

		-- JUMP,  ALU_CODE = "0000", JUMP_CODE = "0111"
		INPUT <= "110111";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '1' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "0111" report "Error (2)" severity error;
		wait for 1 us;

		-- JUMP,  ALU_CODE = "0000", JUMP_CODE = "1111"
		INPUT <= "111111";
		wait for 1 us;

		assert OP_IS_ALU_RR = '0' report "Error (2)" severity error;
		assert OP_IS_ALU_RI = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_LOAD = '0' report "Error (2)" severity error;
		assert OP_IS_STORE = '0' report "Error (2)" severity error;
		assert OP_IS_C_JUMP = '1' report "Error (2)" severity error;
		assert ALU_OPERATION_CODE = "0000" report "Error (2)" severity error;
		assert JUMP_CONDITION_CODE = "1111" report "Error (2)" severity error;
		wait for 1 us;

		report "Fin en ensayo de la unidad de control" severity note;
		wait;

	end process;

end architecture TB_A_CONTROLUNIT;
