--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingeniería, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * Año 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_STAGE_DECODE is
-- body
end entity TB_E_STAGE_DECODE;

-- --------------------------------
-- Definición de la Arquitectura --
-- --------------------------------

architecture TB_A_STAGE_DECODE of TB_E_STAGE_DECODE is
	for all : C_STAGE_DECODE use entity E_STAGE_DECODE;

	signal BP_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
	signal BP_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal BP_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	signal SP_REGISTER_RS_SELECTOR : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal SP_REGISTER_RT_SELECTOR : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

	signal SP_REGISTER_RS_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal SP_REGISTER_RT_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	signal FP_OP_IS_ALU_RR           : STD_LOGIC;
	signal FP_OP_IS_ALU_RI           : STD_LOGIC;
	signal FP_OP_IS_LOAD_STORE       : STD_LOGIC;
	signal FP_OP_IS_LOAD             : STD_LOGIC;
	signal FP_OP_IS_STORE            : STD_LOGIC;
	signal FP_OP_IS_CONDITIONAL_JUMP : STD_LOGIC;
	signal FP_ALU_OPERATION_CODE     : STD_LOGIC_VECTOR(3 downto 0);
	signal FP_JUMP_CONDITION_CODE    : STD_LOGIC_VECTOR(3 downto 0);

	signal FP_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
	signal FP_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.  
	signal FP_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

	signal FP_OP_DESTINATION_REG_ID : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal FP_OP_OPERAND_1_REG_ID   : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal FP_OP_OPERAND_2_REG_ID   : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

	signal FP_OP_OPERAND_1_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
	signal FP_OP_OPERAND_2_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros. 
	signal FP_OP_IMM_VALUE       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucción). 


begin
	test_process : process
	begin
		report "Comienzo en ensayo de la etapa de DECODE" severity note;
		wait for 1 us;

		-- Verifico las entradas que deben pasar directo		
		BP_INSTR       <= x"abcdefab";
		BP_PC_VALUE    <= std_logic_vector(to_unsigned(24, ADDR_BUS_SIZE));
		BP_CLOCK_BEATS <= std_logic_vector(to_unsigned(36, BEATS_COUNTER_SIZE));
		wait for 1 us;

		assert FP_PC_VALUE = BP_PC_VALUE
			report "Error (0)" severity error;
		assert FP_CLOCK_BEATS = BP_CLOCK_BEATS
			report "Error (0)" severity error;
		assert FP_INSTR = BP_INSTR
			report "Error (0)" severity error;

		-- Verifico las salidas propias de esta etapa
		SP_REGISTER_RS_VALUE <= x"aaaaaaaa";
		SP_REGISTER_RT_VALUE <= x"bbbbbbbb";

		-- Instrucción RR, Rs 0, Rt 0, Rd 0. [NOP]
		BP_INSTR <= "00" & "0000" & "00000" & "00000" & "00000000000" & "00000";
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (11)" severity error;
		assert FP_OP_IS_ALU_RR = '1' and FP_OP_IS_ALU_RI = '0' and FP_OP_IS_LOAD_STORE = '0' and FP_OP_IS_LOAD = '0' and FP_OP_IS_STORE = '0' and FP_OP_IS_CONDITIONAL_JUMP = '0'
			report "Error (12)" severity error;
		assert FP_ALU_OPERATION_CODE = "0000"
			report "Error (13)" severity error;
		--assert FP_JUMP_CONDITION_CODE = "0000"
		--	report "Error (14)" severity error;
		assert FP_OP_DESTINATION_REG_ID = "00000"
			report "Error (15)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = "00000"
			report "Error (16)" severity error;
		assert FP_OP_OPERAND_2_REG_ID = "00000"
			report "Error (17)" severity error;
		assert FP_OP_OPERAND_1_VALUE = x"00000000"
			report "Error (18)" severity error;
		assert FP_OP_OPERAND_2_VALUE = x"00000000"
			report "Error (19)" severity error;
		--assert FP_OP_IMM_VALUE = x"00000000"
		--	report "Error (1a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "00000"
			report "Error (1A)" severity error;
		assert SP_REGISTER_RT_SELECTOR = "00000"
			report "Error (1B)" severity error;
		wait for 1 us;

		-- Instrucción RR, Code 10, Rs 17, Rt 19, Rd 18.
		BP_INSTR <= "00" & "1010" & "10001" & "10010" & "00000000000" & "10011";
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (21)" severity error;
		assert FP_OP_IS_ALU_RR = '1' and FP_OP_IS_ALU_RI = '0' and FP_OP_IS_LOAD_STORE = '0' and FP_OP_IS_LOAD = '0' and FP_OP_IS_STORE = '0' and FP_OP_IS_CONDITIONAL_JUMP = '0'
			report "Error (22)" severity error;
		assert FP_ALU_OPERATION_CODE = "1010"
			report "Error (23)" severity error;
		--assert FP_JUMP_CONDITION_CODE = "0000"
		--	report "Error (24)" severity error;
		assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(18, 5))
			report "Error (25)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(17, 5))
			report "Error (26)" severity error;
		assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
			report "Error (27)" severity error;
		assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
			report "Error (28)" severity error;
		assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
			report "Error (29)" severity error;
		--assert FP_OP_IMM_VALUE = x"00000000"
		--	report "Error (2a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "10001"
			report "Error (2A)" severity error;
		assert SP_REGISTER_RT_SELECTOR = "10011"
			report "Error (2B)" severity error;
		wait for 1 us;

		-- Instrucción RI, Code 5, Rs 17, Rd 18, dato Inmediato 3.
		BP_INSTR <= "01" & "0101" & "10001" & "10010" & "0000000000000011";
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (31)" severity error;
		assert FP_OP_IS_ALU_RR = '0' and FP_OP_IS_ALU_RI = '1' and FP_OP_IS_LOAD_STORE = '0' and FP_OP_IS_LOAD = '0' and FP_OP_IS_STORE = '0' and FP_OP_IS_CONDITIONAL_JUMP = '0'
			report "Error (32)" severity error;
		assert FP_ALU_OPERATION_CODE = "0101"
			report "Error (33)" severity error;
		--assert FP_JUMP_CONDITION_CODE = "0000"
		--	report "Error (34)" severity error;
		assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(18, 5))
			report "Error (35)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(17, 5))
			report "Error (36)" severity error;
		--assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
		--	report "Error (37)" severity error;
		assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
			report "Error (38)" severity error;
		assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
			report "Error (39)" severity error;
		assert FP_OP_IMM_VALUE = x"00000003"
			report "Error (3a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "10001"
			report "Error (3A)" severity error;
		assert SP_REGISTER_RT_SELECTOR = "10010"
			report "Error (3B)" severity error;

		wait for 1 us;

		-- Instrucción RI, Code 10, Rs 17, Rd 18, dato Inmediato -1 (en complento-2).
		BP_INSTR <= "01" & "1010" & "10001" & "10010" & "1111111111111111";
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (41)" severity error;
		assert FP_OP_IS_ALU_RR = '0' and FP_OP_IS_ALU_RI = '1' and FP_OP_IS_LOAD_STORE = '0' and FP_OP_IS_LOAD = '0' and FP_OP_IS_STORE = '0' and FP_OP_IS_CONDITIONAL_JUMP = '0'
			report "Error (42)" severity error;
		assert FP_ALU_OPERATION_CODE = "1010"
			report "Error (43)" severity error;
		--assert FP_JUMP_CONDITION_CODE = "0000"
		--	report "Error (44)" severity error;
		assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(18, 5))
			report "Error (45)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(17, 5))
			report "Error (46)" severity error;
		--assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
		--	report "Error (47)" severity error;
		assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
			report "Error (48)" severity error;
		assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
			report "Error (49)" severity error;
		assert FP_OP_IMM_VALUE = x"ffffffff"
			report "Error (4a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "10001"
			report "Error (4A)" severity error;
		assert SP_REGISTER_RT_SELECTOR = "10010"
			report "Error (4B)" severity error;

		wait for 1 us;

		-- Instrucción LOAD, Rd 24, RS 31, dato Inmediato 3.
		BP_INSTR <= "10" & "0000" & "11111" & "11000" & "0000000000000011";
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (51)" severity error;
		assert FP_OP_IS_ALU_RR = '0' and FP_OP_IS_ALU_RI = '0' and FP_OP_IS_LOAD_STORE = '1' and FP_OP_IS_LOAD = '1' and FP_OP_IS_STORE = '0' and FP_OP_IS_CONDITIONAL_JUMP = '0'
			report "Error (52)" severity error;
		--assert FP_ALU_OPERATION_CODE = "1010"
		--	report "Error (53)" severity error;
		--assert FP_JUMP_CONDITION_CODE = "0000"
		--	report "Error (54)" severity error;
		assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(24, 5))
			report "Error (55)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(31, 5))
			report "Error (56)" severity error;
		--assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
		--	report "Error (57)" severity error;
		assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
			report "Error (58)" severity error;
		--assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
		--	report "Error (59)" severity error;
		assert FP_OP_IMM_VALUE = x"00000003"
			report "Error (5a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "11111"
			report "Error (5A)" severity error;
		assert SP_REGISTER_RT_SELECTOR = "11000"
			report "Error (5B)" severity error;
		wait for 1 us;

		-- Instrucción LOAD, Rd 17, Rs 1, dato Inmediato -2.
		BP_INSTR <= "10" & "0000" & "00001" & "10001" & "1111111111111110";
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (61)" severity error;
		assert FP_OP_IS_ALU_RR = '0' and FP_OP_IS_ALU_RI = '0' and FP_OP_IS_LOAD_STORE = '1' and FP_OP_IS_LOAD = '1' and FP_OP_IS_STORE = '0' and FP_OP_IS_CONDITIONAL_JUMP = '0'
			report "Error (62)" severity error;
		--assert FP_ALU_OPERATION_CODE = "1010"
		--	report "Error (63)" severity error;
		--assert FP_JUMP_CONDITION_CODE = "0000"
		--	report "Error (64)" severity error;
		assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(17, 5))
			report "Error (65)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(1, 5))
			report "Error (66)" severity error;
		--assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
		--	report "Error (67)" severity error;
		assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
			report "Error (68)" severity error;
		--assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
		--	report "Error (69)" severity error;
		assert FP_OP_IMM_VALUE = x"fffffffe"
			report "Error (6a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "00001"
			report "Error (6A)" severity error;
		assert SP_REGISTER_RT_SELECTOR = "10001"
			report "Error (6B)" severity error;

		wait for 1 us;

		-- Instrucción STORE Rd 31, Rs 24 dato Inmediato 3.
		BP_INSTR <= "10" & "0001" & "11000" & "11111" & "0000000000000011";
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (71)" severity error;
		assert FP_OP_IS_ALU_RR = '0' and FP_OP_IS_ALU_RI = '0' and FP_OP_IS_LOAD_STORE = '1' and FP_OP_IS_LOAD = '0' and FP_OP_IS_STORE = '1' and FP_OP_IS_CONDITIONAL_JUMP = '0'
			report "Error (72)" severity error;
		--assert FP_ALU_OPERATION_CODE = "1010"
		--	report "Error (73)" severity error;
		--assert FP_JUMP_CONDITION_CODE = "0000"
		--	report "Error (74)" severity error;
		assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(31, 5))
			report "Error (75)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(24, 5))
			report "Error (76)" severity error;
		--assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
		--	report "Error (77)" severity error;
		--assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
		--	report "Error (78)" severity error;
		assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
			report "Error (79)" severity error;
		assert FP_OP_IMM_VALUE = x"00000003"
			report "Error (7a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "11000"
			report "Error (7A)" severity error;
		assert SP_REGISTER_RT_SELECTOR = "11111"
			report "Error (7B)" severity error;

		wait for 1 us;

		BP_INSTR <= "10" & "0001" & "10001" & "11111" & "1111111111111110"; -- Instrucción STORE, Rd 17, dato Inmediato -2.
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (81)" severity error;
		assert FP_OP_IS_ALU_RR = '0' and FP_OP_IS_ALU_RI = '0' and FP_OP_IS_LOAD_STORE = '1' and FP_OP_IS_LOAD = '0' and FP_OP_IS_STORE = '1' and FP_OP_IS_CONDITIONAL_JUMP = '0'
			report "Error (82)" severity error;
		--assert FP_ALU_OPERATION_CODE = "1010"
		--	report "Error (83)" severity error;
		--assert FP_JUMP_CONDITION_CODE = "0000"
		--	report "Error (84)" severity error;
		assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(31, 5))
			report "Error (85)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(17, 5))
			report "Error (86)" severity error;
		--assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
		--	report "Error (87)" severity error;
		--assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
		--	report "Error (88)" severity error;
		assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
			report "Error (89)" severity error;
		assert FP_OP_IMM_VALUE = x"fffffffe"
			report "Error (8a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "10001"
			report "Error (8A)" severity error;
		assert SP_REGISTER_RT_SELECTOR = "11111"
			report "Error (8B)" severity error;

		wait for 1 us;

		BP_INSTR <= "11" & "0001" & "10001" & "000000000000000000001"; -- Instrucción JUMP, Code 1, Rs 17, dato Inmediato 1.
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (91)" severity error;
		assert FP_OP_IS_ALU_RR = '0' and FP_OP_IS_ALU_RI = '0' and FP_OP_IS_LOAD_STORE = '0' and FP_OP_IS_LOAD = '0' and FP_OP_IS_STORE = '0' and FP_OP_IS_CONDITIONAL_JUMP = '1'
			report "Error (92)" severity error;
		--assert FP_ALU_OPERATION_CODE = "1010"
		--	report "Error (93)" severity error;
		assert FP_JUMP_CONDITION_CODE = "0001"
			report "Error (94)" severity error;
		--assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(17, 5))
		--	report "Error (95)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(17, 5))
			report "Error (96)" severity error;
		--assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
		--	report "Error (97)" severity error;
		assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
			report "Error (98)" severity error;
		--assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
		--	report "Error (99)" severity error;
		assert FP_OP_IMM_VALUE = x"00000001"
			report "Error (9a)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "10001"
			report "Error (9A)" severity error;

		wait for 1 us;

		-- Instrucción JUMP, Code 13, Rs 18, dato Inmediato 2097151.
		BP_INSTR <= "11" & "1101" & "10010" & "111111111111111111111";
		wait for 1 us;

		assert FP_INSTR = BP_INSTR
			report "Error (a1)" severity error;
		assert FP_OP_IS_ALU_RR = '0' and FP_OP_IS_ALU_RI = '0' and FP_OP_IS_LOAD_STORE = '0' and FP_OP_IS_LOAD = '0' and FP_OP_IS_STORE = '0' and FP_OP_IS_CONDITIONAL_JUMP = '1'
			report "Error (a2)" severity error;
		--assert FP_ALU_OPERATION_CODE = "1010"
		--	report "Error (a3)" severity error;
		assert FP_JUMP_CONDITION_CODE = "1101"
			report "Error (a4)" severity error;
		--assert FP_OP_DESTINATION_REG_ID = std_logic_vector(to_unsigned(17, 5))
		--	report "Error (a5)" severity error;
		assert FP_OP_OPERAND_1_REG_ID = std_logic_vector(to_unsigned(18, 5))
			report "Error (a6)" severity error;
		--assert FP_OP_OPERAND_2_REG_ID = std_logic_vector(to_unsigned(19, 5))
		--	report "Error (a7)" severity error;
		assert FP_OP_OPERAND_1_VALUE = x"aaaaaaaa"
			report "Error (a8)" severity error;
		--assert FP_OP_OPERAND_2_VALUE = x"bbbbbbbb"
		--	report "Error (a9)" severity error;
		assert FP_OP_IMM_VALUE = x"001fffff"
			report "Error (aa)" severity error;
		assert SP_REGISTER_RS_SELECTOR = "10010"
			report "Error (aa)" severity error;
		wait for 1 us;

		report "Fin en ensayo de la etapa de DECODE" severity note;
		wait;

	end process;

	stage_under_test : C_STAGE_DECODE port map(
			SP_REGISTER_RS_SELECTOR   => SP_REGISTER_RS_SELECTOR,
			SP_REGISTER_RT_SELECTOR   => SP_REGISTER_RT_SELECTOR,
			SP_REGISTER_RS_VALUE      => SP_REGISTER_RS_VALUE,
			SP_REGISTER_RT_VALUE      => SP_REGISTER_RT_VALUE,
			BP_CLOCK_BEATS            => BP_CLOCK_BEATS,
			BP_PC_VALUE               => BP_PC_VALUE,
			BP_INSTR                  => BP_INSTR,
			FP_CLOCK_BEATS            => FP_CLOCK_BEATS,
			FP_PC_VALUE               => FP_PC_VALUE,
			FP_INSTR                  => FP_INSTR,
			FP_OP_IS_ALU_RR           => FP_OP_IS_ALU_RR,
			FP_OP_IS_ALU_RI           => FP_OP_IS_ALU_RI,
			FP_OP_IS_LOAD_STORE       => FP_OP_IS_LOAD_STORE,
			FP_OP_IS_LOAD             => FP_OP_IS_LOAD,
			FP_OP_IS_STORE            => FP_OP_IS_STORE,
			FP_OP_IS_CONDITIONAL_JUMP => FP_OP_IS_CONDITIONAL_JUMP,
			FP_ALU_OPERATION_CODE     => FP_ALU_OPERATION_CODE,
			FP_JUMP_CONDITION_CODE    => FP_JUMP_CONDITION_CODE,
			FP_OP_DESTINATION_REG_ID  => FP_OP_DESTINATION_REG_ID,
			FP_OP_OPERAND_1_REG_ID    => FP_OP_OPERAND_1_REG_ID,
			FP_OP_OPERAND_2_REG_ID    => FP_OP_OPERAND_2_REG_ID,
			FP_OP_OPERAND_1_VALUE     => FP_OP_OPERAND_1_VALUE,
			FP_OP_OPERAND_2_VALUE     => FP_OP_OPERAND_2_VALUE,
			FP_OP_IMM_VALUE           => FP_OP_IMM_VALUE
		);

end architecture TB_A_STAGE_DECODE;
