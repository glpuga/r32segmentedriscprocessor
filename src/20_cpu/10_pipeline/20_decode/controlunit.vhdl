--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_CONTROLUNIT is
	generic(
		DELAY : time := TIME_DELAY_CONTROlUNIT
	);
	port(
		ICLASS              : in  STD_LOGIC_VECTOR((CLASS_FIELD_SIZE - 1) downto 0);
		ICODE               : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);

		OP_IS_ALU_RR        : out STD_LOGIC;
		OP_IS_ALU_RI        : out STD_LOGIC;
		OP_IS_LOAD_STORE    : out STD_LOGIC;
		OP_IS_LOAD          : out STD_LOGIC;
		OP_IS_STORE         : out STD_LOGIC;
		OP_IS_C_JUMP        : out STD_LOGIC;

		ALU_OPERATION_CODE  : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
		JUMP_CONDITION_CODE : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0)
	);

end entity E_CONTROLUNIT;

-- -------------------------------- 

architecture A_CONTROLUNIT of E_CONTROLUNIT is
	signal FIELD_LOAD_STORE_SELECTOR : STD_LOGIC;
	signal FIELD_ALU_OPERATION_CODE  : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
	signal FIELD_JUMP_CONDITION_CODE : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);

	signal I_OP_IS_ALU_RR     : STD_LOGIC;
	signal I_OP_IS_ALU_RI     : STD_LOGIC;
	signal I_OP_IS_LOAD_STORE : STD_LOGIC;
	signal I_OP_IS_LOAD       : STD_LOGIC;
	signal I_OP_IS_STORE      : STD_LOGIC;
	signal I_OP_IS_C_JUMP     : STD_LOGIC;

	signal I_ALU_OPERATION_CODE  : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
	signal I_JUMP_CONDITION_CODE : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);

begin
	FIELD_LOAD_STORE_SELECTOR <= ICODE(0);
	FIELD_ALU_OPERATION_CODE  <= ICODE((CODE_FIELD_SIZE - 1) downto 0);
	FIELD_JUMP_CONDITION_CODE <= ICODE((CODE_FIELD_SIZE - 1) downto 0);

	I_OP_IS_ALU_RR     <= not ICLASS(1) and not ICLASS(0); -- Cuando ICLASS = "00"
	I_OP_IS_ALU_RI     <= not ICLASS(1) and ICLASS(0); -- Cuando ICLASS = "01"
	I_OP_IS_LOAD_STORE <= ICLASS(1) and not ICLASS(0); -- Cuando ICLASS = "10"
	I_OP_IS_C_JUMP     <= ICLASS(1) and ICLASS(0); -- Cuando ICLASS = "11"

	-- Las instrucciones LOAD y STORE salen diferenciadas de la unidad de control
	I_OP_IS_LOAD  <= I_OP_IS_LOAD_STORE and not FIELD_LOAD_STORE_SELECTOR;
	I_OP_IS_STORE <= I_OP_IS_LOAD_STORE and FIELD_LOAD_STORE_SELECTOR;

	-- Slo habilito las salidas cuando su contenido tiene sentido
	I_ALU_OPERATION_CODE <= FIELD_ALU_OPERATION_CODE when (I_OP_IS_ALU_RR = '1' or I_OP_IS_ALU_RI = '1')
		else "0000" when (I_OP_IS_ALU_RR = '0' and I_OP_IS_ALU_RI = '0');

	I_JUMP_CONDITION_CODE <= FIELD_JUMP_CONDITION_CODE when (I_OP_IS_C_JUMP = '1')
		else "0000" when (I_OP_IS_C_JUMP = '0');

	-- Conecto las se�ales internas a las de los puertos de salida.									  
	OP_IS_ALU_RR        <= I_OP_IS_ALU_RR after DELAY;
	OP_IS_ALU_RI        <= I_OP_IS_ALU_RI after DELAY;
	OP_IS_LOAD_STORE    <= I_OP_IS_LOAD_STORE after DELAY;
	OP_IS_LOAD          <= I_OP_IS_LOAD after DELAY;
	OP_IS_STORE         <= I_OP_IS_STORE after DELAY;
	OP_IS_C_JUMP        <= I_OP_IS_C_JUMP after DELAY;
	ALU_OPERATION_CODE  <= I_ALU_OPERATION_CODE after DELAY;
	JUMP_CONDITION_CODE <= I_JUMP_CONDITION_CODE after DELAY;

end architecture A_CONTROLUNIT;
