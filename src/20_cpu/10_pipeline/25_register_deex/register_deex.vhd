--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_REGISTER_DEEX is
	port(
		-- Puertos laterales (side ports)
		SP_PREVIOUS_STAGE_OP_IS_ALU_RR           : out STD_LOGIC;
		SP_PREVIOUS_STAGE_OP_IS_ALU_RI           : out STD_LOGIC;
		SP_PREVIOUS_STAGE_OP_IS_LOAD_STORE       : out STD_LOGIC;
		SP_PREVIOUS_STAGE_OP_IS_LOAD             : out STD_LOGIC;
		SP_PREVIOUS_STAGE_OP_IS_STORE            : out STD_LOGIC;
		SP_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP : out STD_LOGIC;

		SP_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		SP_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID    : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		SP_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID    : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);

		SP_FORWARD_OPERAND_1                     : in  STD_LOGIC; -- Adelantar RS 
		SP_FORWARD_OPERAND_2                     : in  STD_LOGIC; -- Adelantar RT

		SP_FORWARDED_VALUE                       : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

		SP_SYNC_STALL                            : in  STD_LOGIC; -- Inhibici�n de avance.
		SP_SYNC_INSERT_BUBBLE                    : in  STD_LOGIC; -- Clear sincr�nico.

		SP_ASYNC_RESET                           : in  STD_LOGIC; -- Reset asincr�nico.
		SP_CLOCK                                 : in  STD_LOGIC; -- Se�al de reloj, activo en flanco ascendente.

		-- Puertos hacia la etapa de DECODE
		BP_CLOCK_BEATS                           : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		BP_PC_VALUE                              : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		BP_INSTR                                 : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		BP_OP_IS_ALU_RR                          : in  STD_LOGIC;
		BP_OP_IS_ALU_RI                          : in  STD_LOGIC;
		BP_OP_IS_LOAD_STORE                      : in  STD_LOGIC;
		BP_OP_IS_LOAD                            : in  STD_LOGIC;
		BP_OP_IS_STORE                           : in  STD_LOGIC;
		BP_OP_IS_CONDITIONAL_JUMP                : in  STD_LOGIC;
		BP_ALU_OPERATION_CODE                    : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
		BP_JUMP_CONDITION_CODE                   : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
		BP_OP_DESTINATION_REG_ID                 : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		BP_OP_OPERAND_1_REG_ID                   : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		BP_OP_OPERAND_2_REG_ID                   : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		BP_OP_OPERAND_1_VALUE                    : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		BP_OP_OPERAND_2_VALUE                    : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		BP_OP_IMM_VALUE                          : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

		-- Puertos hacia la etapa de EXECUTE
		FP_CLOCK_BEATS                           : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		FP_PC_VALUE                              : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		FP_INSTR                                 : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		FP_OP_IS_ALU_RR                          : out STD_LOGIC;
		FP_OP_IS_ALU_RI                          : out STD_LOGIC;
		FP_OP_IS_LOAD_STORE                      : out STD_LOGIC;
		FP_OP_IS_LOAD                            : out STD_LOGIC;
		FP_OP_IS_STORE                           : out STD_LOGIC;
		FP_OP_IS_CONDITIONAL_JUMP                : out STD_LOGIC;
		FP_ALU_OPERATION_CODE                    : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
		FP_JUMP_CONDITION_CODE                   : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
		FP_OP_DESTINATION_REG_ID                 : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		FP_OP_OPERAND_1_REG_ID                   : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		FP_OP_OPERAND_2_REG_ID                   : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		FP_OP_OPERAND_1_VALUE                    : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		FP_OP_OPERAND_2_VALUE                    : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		FP_OP_IMM_VALUE                          : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
	);
end entity E_REGISTER_DEEX;

architecture A_REGISTER_DEEX of E_REGISTER_DEEX is
	for all : C_INTERSTAGE_REGISTER use entity E_INTERSTAGE_REGISTER;
	for all : C_MULTIPLEXER2 use entity E_MULTIPLEXER2;

	signal I_SYNC_STALL         : STD_LOGIC;
	signal I_SYNC_INSERT_BUBBLE : STD_LOGIC;
	signal I_ASYNC_RESET        : STD_LOGIC;
	signal I_CLOCK              : STD_LOGIC;

	signal I_INPUT_LONERS  : STD_LOGIC_VECTOR(5 downto 0);
	signal I_OUTPUT_LONERS : STD_LOGIC_VECTOR(5 downto 0);

	signal I_OP_OPERAND_1_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal I_OP_OPERAND_2_VALUE : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

begin
	SP_PREVIOUS_STAGE_OP_IS_ALU_RR           <= BP_OP_IS_ALU_RR;
	SP_PREVIOUS_STAGE_OP_IS_ALU_RI           <= BP_OP_IS_ALU_RI;
	SP_PREVIOUS_STAGE_OP_IS_LOAD_STORE       <= BP_OP_IS_LOAD_STORE;
	SP_PREVIOUS_STAGE_OP_IS_LOAD             <= BP_OP_IS_LOAD;
	SP_PREVIOUS_STAGE_OP_IS_STORE            <= BP_OP_IS_STORE;
	SP_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP <= BP_OP_IS_CONDITIONAL_JUMP;

	SP_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID <= BP_OP_OPERAND_1_REG_ID;
	SP_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID <= BP_OP_OPERAND_2_REG_ID;

	SP_PREVIOUS_STAGE_OP_DESTINATION_REG_ID <= BP_OP_DESTINATION_REG_ID;

	I_SYNC_STALL         <= SP_SYNC_STALL;
	I_SYNC_INSERT_BUBBLE <= SP_SYNC_INSERT_BUBBLE;
	I_ASYNC_RESET        <= SP_ASYNC_RESET;
	I_CLOCK              <= SP_CLOCK;

	I_INPUT_LONERS <= BP_OP_IS_CONDITIONAL_JUMP & BP_OP_IS_STORE & BP_OP_IS_LOAD & BP_OP_IS_LOAD_STORE & BP_OP_IS_ALU_RI & BP_OP_IS_ALU_RR;

	FP_OP_IS_ALU_RR           <= I_OUTPUT_LONERS(0);
	FP_OP_IS_ALU_RI           <= I_OUTPUT_LONERS(1);
	FP_OP_IS_LOAD_STORE       <= I_OUTPUT_LONERS(2);
	FP_OP_IS_LOAD             <= I_OUTPUT_LONERS(3);
	FP_OP_IS_STORE            <= I_OUTPUT_LONERS(4);
	FP_OP_IS_CONDITIONAL_JUMP <= I_OUTPUT_LONERS(5);

	rLonerSignals : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => 6)
		port map(
			DATAIN       => I_INPUT_LONERS,
			DATAOUT      => I_OUTPUT_LONERS,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rBeatsCounterSize : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => BEATS_COUNTER_SIZE)
		port map(
			DATAIN       => BP_CLOCK_BEATS,
			DATAOUT      => FP_CLOCK_BEATS,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rPCValue : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => ADDR_BUS_SIZE)
		port map(
			DATAIN       => BP_PC_VALUE,
			DATAOUT      => FP_PC_VALUE,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rInstr : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => DATA_BUS_SIZE)
		port map(
			DATAIN       => BP_INSTR,
			DATAOUT      => FP_INSTR,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rAluOperationCode : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => 4)
		port map(
			DATAIN       => BP_ALU_OPERATION_CODE,
			DATAOUT      => FP_ALU_OPERATION_CODE,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rJumpConditionCode : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => 4)
		port map(
			DATAIN       => BP_JUMP_CONDITION_CODE,
			DATAOUT      => FP_JUMP_CONDITION_CODE,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rFieldRd : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => REG_SEL_SIZE)
		port map(
			DATAIN       => BP_OP_DESTINATION_REG_ID,
			DATAOUT      => FP_OP_DESTINATION_REG_ID,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rFieldRs : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => REG_SEL_SIZE)
		port map(
			DATAIN       => BP_OP_OPERAND_1_REG_ID,
			DATAOUT      => FP_OP_OPERAND_1_REG_ID,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rFieldRt : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => REG_SEL_SIZE)
		port map(
			DATAIN       => BP_OP_OPERAND_2_REG_ID,
			DATAOUT      => FP_OP_OPERAND_2_REG_ID,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	rImm : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => DATA_BUS_SIZE)
		port map(
			DATAIN       => BP_OP_IMM_VALUE,
			DATAOUT      => FP_OP_IMM_VALUE,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	-- ---------------------------------
	-- Resoluci�n de forwarding

	op1ForwardingMux : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => DATA_BUS_SIZE
		) port map(
			IPORT00 => BP_OP_OPERAND_1_VALUE,
			IPORT01 => SP_FORWARDED_VALUE,
			OPORT => I_OP_OPERAND_1_VALUE,
			SEL => SP_FORWARD_OPERAND_1
		);

	rOp1Value : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => DATA_BUS_SIZE)
		port map(
			DATAIN       => I_OP_OPERAND_1_VALUE,
			DATAOUT      => FP_OP_OPERAND_1_VALUE,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

	op2ForwardingMux : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => DATA_BUS_SIZE
		) port map(
			IPORT00 => BP_OP_OPERAND_2_VALUE,
			IPORT01 => SP_FORWARDED_VALUE,
			OPORT => I_OP_OPERAND_2_VALUE,
			SEL => SP_FORWARD_OPERAND_2
		);

	rOp2Value : C_INTERSTAGE_REGISTER generic map(
			INTERSTAGE_REGISTER_WIDTH => DATA_BUS_SIZE)
		port map(
			DATAIN       => I_OP_OPERAND_2_VALUE,
			DATAOUT      => FP_OP_OPERAND_2_VALUE,
			SYNC_INHIBIT => I_SYNC_STALL,
			SYNC_CLEAR   => I_SYNC_INSERT_BUBBLE,
			ASYNC_RESET  => I_ASYNC_RESET,
			CLOCK        => I_CLOCK);

end architecture A_REGISTER_DEEX;
