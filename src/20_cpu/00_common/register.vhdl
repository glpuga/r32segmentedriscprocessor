--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

entity E_REGISTER is
	generic(
		REGISTER_WIDTH : integer := 32;
		DELAY          : time    := TIME_DELAY_REGISTER
	);
	port(
		-- Input ports 
		DATAIN  : in  STD_LOGIC_VECTOR((REGISTER_WIDTH - 1) downto 0);
		CLOCK   : in  STD_LOGIC;
		RESET   : in  STD_LOGIC;
		-- OUtput Ports
		DATAOUT : out STD_LOGIC_VECTOR((REGISTER_WIDTH - 1) downto 0)
	);
end E_REGISTER;

architecture A_REGISTER of E_REGISTER is
begin
	process(CLOCK, RESET)
		variable register_value : STD_LOGIC_VECTOR((REGISTER_WIDTH - 1) downto 0);

	begin
		if (RESET = '0') then
			-- Trato la condici�n de reset. Esta se�al es asincr�nica.
			register_value := (others => '0');

		elsif (CLOCK'event and CLOCK = '1') then
			-- Trato la condici�n de toma de datos. El registro es activo en flanco de subida.
			register_value := DATAIN;
		end if;

		DATAOUT <= register_value after DELAY;
	end process;

end A_REGISTER;
			