--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_WORDEXTENDER is
	generic(
		DELAY        : time := TIME_DELAY_WORDEXTENDER;
		INPUT_WIDTH  : integer;
		OUTPUT_WIDTH : integer
	);
	port(
		DATAIN     : in  STD_LOGIC_VECTOR((INPUT_WIDTH - 1) downto 0);
		ISUNSIGNED : IN  STD_LOGIC;
		DATAOUT    : out STD_LOGIC_VECTOR((OUTPUT_WIDTH - 1) downto 0)
	);
end entity E_WORDEXTENDER;

-- --------------------------------

architecture A_WORDEXTENDER of E_WORDEXTENDER is
	signal PADBIT  : STD_LOGIC;
	signal PADDING : STD_LOGIC_VECTOR((OUTPUT_WIDTH - INPUT_WIDTH - 1) downto 0);

begin

	-- Los bits de relleno son iguales al signo de la palabra entrante 
	-- solamente si �sta debe interpretarse como con signo

	PADBIT  <= DATAIN(INPUT_WIDTH - 1) and not ISUNSIGNED;
	PADDING <= (others => PADBIT);

	DATAOUT <= PADDING & DATAIN after DELAY;

end architecture A_WORDEXTENDER;
