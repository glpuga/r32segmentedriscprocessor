--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingeniería, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * Año 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

entity E_INTERSTAGE_REGISTER is
	generic(
		INTERSTAGE_REGISTER_WIDTH : integer := 32;
		DELAY                     : time    := TIME_DELAY_REGISTER
	);
	port(
		-- Puertos de datos sincrónicos 
		DATAIN       : in  STD_LOGIC_VECTOR((INTERSTAGE_REGISTER_WIDTH - 1) downto 0);
		DATAOUT      : out STD_LOGIC_VECTOR((INTERSTAGE_REGISTER_WIDTH - 1) downto 0);
		-- Puertos de datos sincrónicos
		SYNC_INHIBIT : in  STD_LOGIC;   -- activo en alto
		SYNC_CLEAR   : in  STD_LOGIC;   -- activo en alto		
		-- Puertos asincrónicos
		ASYNC_RESET  : in  STD_LOGIC;   -- activo en alto
		-- Reloj
		CLOCK        : in  STD_LOGIC    -- activo en flanco ascendente
	);
end E_INTERSTAGE_REGISTER;

architecture A_INTERSTAGE_REGISTER of E_INTERSTAGE_REGISTER is
begin
	process(CLOCK, ASYNC_RESET)
		variable registerValue : STD_LOGIC_VECTOR((INTERSTAGE_REGISTER_WIDTH - 1) downto 0);
		variable selector      : STD_LOGIC_VECTOR(1 downto 0);

	begin
		-- Trato primero la condición de reset asincrónico
		if (ASYNC_RESET = '1') then
			-- Trato la condición de reset asincrónico
			registerValue := (others => '0');

		elsif (CLOCK'event and CLOCK = '1') then

			-- Si no hay reset pero hay un flanco ascendente, entonces verifico las lineas de control
			-- sincrónicas, SYNC_INHIBIT y SYNC_CLEAR.

			selector := SYNC_CLEAR & SYNC_INHIBIT;
			case selector is
				when "00"   => registerValue := DATAIN; -- Si no hay acciones de control, registra la entrada.
				when "01"   => null;    -- Si INHIBIT, entonces no cambia de valor.
				when others => registerValue := (others => '0'); -- Si CLEAR, entonces pasa a cero.
			end case;

		end if;

		DATAOUT <= registerValue after DELAY;
	end process;

end A_INTERSTAGE_REGISTER;
			