--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_COUNTER is
	generic(
		COUNTER_WIDTH : integer := BEATS_COUNTER_SIZE;
		DELAY         : time    := TIME_DELAY_PROGRAM_COUNTER
	);
	port(
		-- Puertos sincr�nicos
		VALUE       : out STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0);
		-- Puertos asincr�nicos
		ASYNC_RESET : in  STD_LOGIC;    -- Activo en alto
		-- Reloj
		CLOCK       : in  STD_LOGIC
	);
end entity E_COUNTER;

-- --------------------------------

architecture A_COUNTER of E_COUNTER is
begin
	process(CLOCK, ASYNC_RESET)
		variable internalCounter : STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0) := (others => '0');

	begin
		if (ASYNC_RESET = '1') then
			internalCounter := (others => '0');

		elsif (CLOCK'event and CLOCK = '1') then
			internalCounter := std_logic_vector(unsigned(internalCounter) + to_unsigned(1, COUNTER_WIDTH));

		end if;

		VALUE <= internalCounter after DELAY;
	end process;

end architecture A_COUNTER;
