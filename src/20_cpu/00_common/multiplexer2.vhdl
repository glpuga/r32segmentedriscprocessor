--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

entity E_MULTIPLEXER2 is
	generic(
		PORT_WIDTH : integer := 32;
		DELAY      : time    := TIME_DELAY_MULTIPLEXERS
	);
	port(
		IPORT00 : in  STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0); -- Entrada 0
		IPORT01 : in  STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0); -- Entrada 1
		SEL     : in  STD_LOGIC;        -- L�nea de selecci�n. 
		OPORT   : out STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0) -- Salida.
	);
end entity E_MULTIPLEXER2;

architecture A_MULTIPLEXER of E_MULTIPLEXER2 is
begin
	process(IPORT00, IPORT01, SEL)
	begin
		if (SEL = '0') then
			OPORT <= IPORT00 after DELAY;
		else
			OPORT <= IPORT01 after DELAY;
		end if;

	end process;

end architecture A_MULTIPLEXER;

