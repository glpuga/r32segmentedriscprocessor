--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

entity TB_E_INTERSTAGE_REGISTER is
-- body
end entity TB_E_INTERSTAGE_REGISTER;

architecture TB_A_INTERSTAGE_REGISTER of TB_E_INTERSTAGE_REGISTER is
	for all : C_INTERSTAGE_REGISTER use entity E_INTERSTAGE_REGISTER;

	constant DATA_WIDTH : integer := 4;

	signal DATAIN            : STD_LOGIC_VECTOR((DATA_WIDTH - 1) downto 0);
	signal DATAOUT           : STD_LOGIC_VECTOR((DATA_WIDTH - 1) downto 0);
	signal SYNC_INHIBIT        : STD_LOGIC; -- activo en alto
	signal SYNC_CLEAR : STD_LOGIC; -- activo en alto		
	signal ASYNC_RESET       : STD_LOGIC; -- activo en alto
	signal CLOCK             : STD_LOGIC; -- activo en flanco ascendente

begin
	unit_under_test : C_INTERSTAGE_REGISTER
		generic map(INTERSTAGE_REGISTER_WIDTH => 4)
		port map(DATAIN            => DATAIN,
			     DATAOUT           => DATAOUT,
			     SYNC_INHIBIT        => SYNC_INHIBIT,
			     SYNC_CLEAR => SYNC_CLEAR,
			     ASYNC_RESET       => ASYNC_RESET,
			     CLOCK             => CLOCK);

	test_process : process
	begin
		report "Comienzo en ensayo de registro" severity note;
		wait for 1 us;

		DATAIN            <= "1010";
		SYNC_INHIBIT        <= '0';
		SYNC_CLEAR <= '0';
		ASYNC_RESET       <= '0';
		CLOCK             <= '0';

		-- ------------------------------------------
		-- ------------------------------------------
		-- ------------------------------------------

		-- Verifico el funcionamiento del reset inicial
		-- La salida tiene que tomar valor cero.
		DATAIN            <= "1010";
		SYNC_INHIBIT        <= '0';
		SYNC_CLEAR <= '0';
		ASYNC_RESET       <= '0';
		CLOCK             <= '0';
		wait for 1 us;
		ASYNC_RESET <= '1';
		wait for 1 us;
		ASYNC_RESET <= '0';
		wait for 1 us;

		assert DATAOUT = "0000" report "Error (10)" severity error;
		wait for 1 us;

		-- ------------------------------------------
		-- ------------------------------------------
		-- ------------------------------------------

		-- Aplico un flanco de reloj ascendente
		-- La salida tiene que tomar valor "1010".
		DATAIN            <= "1010";
		SYNC_INHIBIT        <= '0';
		SYNC_CLEAR <= '0';
		ASYNC_RESET       <= '0';
		CLOCK             <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		DATAIN <= "1111";               -- Quito el dato de la entrada para asegurarme de que tiene memoria.
		wait for 1 us;

		assert DATAOUT = "1010" report "Error (20)" severity error;
		wait for 1 us;

		-- ------------------------------------------
		-- ------------------------------------------
		-- ------------------------------------------

		-- Verifico el INSERT_ZEROS sincr�nico
		-- La salida tiene que tomar valor "0000".
		DATAIN            <= "1010";
		SYNC_INHIBIT        <= '0';
		SYNC_CLEAR <= '1';
		ASYNC_RESET       <= '0';
		CLOCK             <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		DATAIN <= "1111";               -- Quito el dato de la entrada para asegurarme de que tiene memoria.
		wait for 1 us;

		assert DATAOUT = "0000" report "Error (30)" severity error;
		wait for 1 us;

		-- ------------------------------------------
		-- ------------------------------------------
		-- ------------------------------------------

		-- Cargo un nuevo n�mero diferente de cero
		-- La salida tiene que tomar valor "1110".
		DATAIN            <= "1110";
		SYNC_INHIBIT        <= '0';
		SYNC_CLEAR <= '0';
		ASYNC_RESET       <= '0';
		CLOCK             <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		DATAIN <= "1111";               -- Quito el dato de la entrada para asegurarme de que tiene memoria.
		wait for 1 us;

		assert DATAOUT = "1110" report "Error (40)" severity error;
		wait for 1 us;

		-- ------------------------------------------
		-- ------------------------------------------
		-- ------------------------------------------

		-- Verifico el inhibidor sincr�nico
		-- La salida tiene que permanecer en "1110".
		DATAIN            <= "0101";
		SYNC_INHIBIT        <= '1';
		SYNC_CLEAR <= '0';
		ASYNC_RESET       <= '0';
		CLOCK             <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		DATAIN <= "1111";               -- Quito el dato de la entrada para asegurarme de que tiene memoria.
		wait for 1 us;

		assert DATAOUT = "1110" report "Error (50)" severity error;
		wait for 1 us;

		-- ------------------------------------------
		-- ------------------------------------------
		-- ------------------------------------------

		-- Verifico el clear sincr�nico tiene prioridad por sobre el inhibidor
		-- La salida tiene que permanecer en "1110".
		DATAIN            <= "0101";
		SYNC_INHIBIT        <= '1';
		SYNC_CLEAR <= '1';
		ASYNC_RESET       <= '0';
		CLOCK             <= '0';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		DATAIN <= "1111";               -- Quito el dato de la entrada para asegurarme de que tiene memoria.
		wait for 1 us;

		assert DATAOUT = "0000" report "Error (60)" severity error;
		wait for 1 us;

		report "Fin en ensayo de registro" severity note;
		wait;

	end process;

end architecture TB_A_INTERSTAGE_REGISTER;
