--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_WORDEXTENDER is
-- body
end entity TB_E_WORDEXTENDER;

-- --------------------------------

architecture TB_A_WORDEXTENDER of TB_E_WORDEXTENDER is
	for all : C_WORDEXTENDER use entity E_WORDEXTENDER;

	-- Se�ales locales
	signal DATAIN     : STD_LOGIC_VECTOR(3 downto 0);
	signal DATAOUT    : STD_LOGIC_VECTOR(7 downto 0);
	signal ISUNSIGNED : STD_LOGIC;

begin
	sign_extender : C_WORDEXTENDER generic map(
			INPUT_WIDTH  => 4,
			OUTPUT_WIDTH => 8
		)
		port map(
			DATAIN     => DATAIN,
			DATAOUT    => DATAOUT,
			ISUNSIGNED => ISUNSIGNED
		);

	test_process : process
	begin
		report "Comienzo en ensayo de extensor de signo" severity note;
		wait for 1 us;

		-- Pruebo cuando el extensor es CON SIGNO
		ISUNSIGNED <= '0';

		DATAIN <= "0000";
		wait for 1 us;
		assert DATAOUT = "00000000" report "Error (1)" severity error;
		wait for 1 us;
		DATAIN <= "0001";
		wait for 1 us;
		assert DATAOUT = "00000001" report "Error (2)" severity error;
		wait for 1 us;
		DATAIN <= "0010";
		wait for 1 us;
		assert DATAOUT = "00000010" report "Error (3)" severity error;
		wait for 1 us;
		DATAIN <= "0100";
		wait for 1 us;
		assert DATAOUT = "00000100" report "Error (4)" severity error;
		wait for 1 us;
		DATAIN <= "1000";
		wait for 1 us;
		assert DATAOUT = "11111000" report "Error (5)" severity error;
		wait for 1 us;

		-- Pruebo cuando el extensor es SIN SIGNO
		ISUNSIGNED <= '1';

		DATAIN <= "0000";
		wait for 1 us;
		assert DATAOUT = "00000000" report "Error (1)" severity error;
		wait for 1 us;
		DATAIN <= "0001";
		wait for 1 us;
		assert DATAOUT = "00000001" report "Error (2)" severity error;
		wait for 1 us;
		DATAIN <= "0010";
		wait for 1 us;
		assert DATAOUT = "00000010" report "Error (3)" severity error;
		wait for 1 us;
		DATAIN <= "0100";
		wait for 1 us;
		assert DATAOUT = "00000100" report "Error (4)" severity error;
		wait for 1 us;
		DATAIN <= "1000";
		wait for 1 us;
		assert DATAOUT = "00001000" report "Error (5)" severity error;
		wait for 1 us;

		report "Fin en ensayo de extensor de signo" severity note;
		wait;

	end process;

end architecture TB_A_WORDEXTENDER;
