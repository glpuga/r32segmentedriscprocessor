--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------
-- Definici�n de la Entidad      --
-- --------------------------------

entity TB_E_REGISTER is
-- body
end entity TB_E_REGISTER;

-- --------------------------------
-- Definici�n de la Arquitectura --
-- --------------------------------

architecture TB_A_REGISTER of TB_E_REGISTER is
	for all : C_REGISTER use entity E_REGISTER;

	constant DATA_WIDTH : integer := 4;

	-- Se�ales locales
	signal DATAIN  : STD_LOGIC_VECTOR((DATA_WIDTH - 1) downto 0);
	signal DATAOUT : STD_LOGIC_VECTOR((DATA_WIDTH - 1) downto 0);
	signal CLOCK   : STD_LOGIC;
	signal RESET   : STD_LOGIC;

begin
	m1 : C_REGISTER
		generic map(
			REGISTER_WIDTH => DATA_WIDTH)
		port map(
			DATAIN  => DATAIN,
			DATAOUT => DATAOUT,
			CLOCK   => CLOCK,
			RESET   => RESET);

	test_process : process
	begin
		report "Comienzo en ensayo de registro" severity note;
		wait for 1 us;

		-- aplico un reset
		CLOCK  <= '0';
		DATAIN <= "1111";
		RESET  <= '1';
		wait for 1 us;
		RESET <= '0';
		wait for 1 us;
		RESET <= '1';
		wait for 1 us;

		-- Valido el valor del registro
		assert DATAOUT = "0000" report "Error (1)" severity error;
		wait for 1 us;

		-- Aplico un flanco de reloj ascendente
		CLOCK  <= '0';
		DATAIN <= "1111";
		RESET  <= '1';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		-- Saco el dato de la entrada para asegurarme de que tiene memoria.
		DATAIN <= "0000";
		wait for 1 us;

		-- Valido el valor del registro
		assert DATAOUT = "1111" report "Error (2)" severity error;
		wait for 1 us;

		-- Aplico un flanco de reloj descendente
		CLOCK <= '0';
		wait for 1 us;

		-- Valido el valor del registro, tiene que seguir recordando el valor
		assert DATAOUT = "1111" report "Error (3)" severity error;
		wait for 1 us;

		-- Aplico un flanco de reloj ascendente
		CLOCK  <= '0';
		DATAIN <= "1010";
		RESET  <= '1';
		wait for 1 us;
		CLOCK <= '1';
		wait for 1 us;
		-- Saco el dato de la entrada para asegurarme de que tiene memoria.
		DATAIN <= "1111";
		wait for 1 us;

		-- Valido el valor del registro
		assert DATAOUT = "1010" report "Error (4)" severity error;
		wait for 1 us;

		-- aplico un reset
		CLOCK  <= '1';
		DATAIN <= "1111";
		RESET  <= '1';
		wait for 1 us;
		RESET <= '0';
		wait for 1 us;
		RESET <= '1';
		wait for 1 us;

		-- Valido el valor del registro
		assert DATAOUT = "0000" report "Error (5)" severity error;
		wait for 1 us;

		report "Fin en ensayo de registro" severity note;
		wait;

	end process;

end architecture TB_A_REGISTER;
