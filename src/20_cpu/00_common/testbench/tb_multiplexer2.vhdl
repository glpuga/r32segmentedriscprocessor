--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------
-- Definici�n de la Entidad      --
-- --------------------------------

entity TB_E_MULTIPLEXER2 is
end entity TB_E_MULTIPLEXER2;

-- --------------------------------
-- Definici�n de la Arquitectura --
-- --------------------------------

architecture TB_A_MULTIPLEXER2 of TB_E_MULTIPLEXER2 is
	for all : C_MULTIPLEXER2 use entity E_MULTIPLEXER2;

	constant PORT_WIDTH : integer := 4;

	signal LIPORT00 : STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0);
	signal LIPORT01 : STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0);
	signal LOPORT   : STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0);
	signal LSEL     : STD_LOGIC;

begin
	m1 : C_MULTIPLEXER2
		generic map(
			PORT_WIDTH => PORT_WIDTH
		) port map(
			IPORT00 => LIPORT00,
			IPORT01 => LIPORT01,
			OPORT => LOPORT,
			SEL => LSEL
		);

	test : process
	begin

		-- Establezco los valores de las entradas del multiplexor
		LIPORT00 <= "0001";
		LIPORT01 <= "1111";
		-- Selecciono el valor 0001
		LSEL     <= '0';

		report "Comienzo en ensayo de multiplexor" severity note;

		wait for 1 us;

		-- Verifico el valor de la salida
		assert (LOPORT = "0001") report "Error (1)" severity error;

		wait for 1 us;

		-- Selecciono el valor 1111
		LSEL <= '1';

		wait for 1 us;

		-- Verifico el valor de la salida
		assert (LOPORT = "1111") report "Error (2)" severity error;

		wait for 1 us;

		-- Establezco los valores de las entradas del multiplexor
		LIPORT00 <= "1111";
		LIPORT01 <= "0001";
		-- Selecciono el valor 0001
		LSEL     <= '0';

		wait for 1 us;

		-- Verifico el valor de la salida
		assert (LOPORT = "1111") report "Error (3)" severity error;

		wait for 1 us;

		-- Selecciono el valor 1111
		LSEL <= '1';

		wait for 1 us;

		-- Verifico el valor de la salida
		assert (LOPORT = "0001") report "Error (4)" severity error;

		report "Fin en ensayo de multiplexor" severity note;
		wait;

	end process;

end architecture TB_A_MULTIPLEXER2;
