--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_PIPELINE_CONTROL is
	port(

		-- -----------------------------------
		-- Puertos hacia el PC COUNTER
		PCNT_STALL                                 : out STD_LOGIC; -- Inhibo el avance de la etapa.
		PCNT_SET_VALUE                             : out STD_LOGIC; -- Inserto un a burbuja en esta etapa.
		PCNT_NEW_VALUE                             : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

		-- -----------------------------------
		-- Puertos hacia el registro FEDE
		FEDE_STALL                                 : out STD_LOGIC; -- Inhibo el avance de la etapa.
		FEDE_INSERT_BUBBLE                         : out STD_LOGIC; -- Inserto un a burbuja en esta etapa.

		-- -----------------------------------
		-- Puertos hacia el registro DEEX
		DEEX_PREVIOUS_STAGE_OP_IS_ALU_RR           : in  STD_LOGIC;
		DEEX_PREVIOUS_STAGE_OP_IS_ALU_RI           : in  STD_LOGIC;
		DEEX_PREVIOUS_STAGE_OP_IS_LOAD_STORE       : in  STD_LOGIC;
		DEEX_PREVIOUS_STAGE_OP_IS_LOAD             : in  STD_LOGIC;
		DEEX_PREVIOUS_STAGE_OP_IS_STORE            : in  STD_LOGIC;
		DEEX_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP : in  STD_LOGIC;

		DEEX_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID    : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID    : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);

		DEEX_FORWARD_OPERAND_1                     : out STD_LOGIC; -- Adelantar RS 
		DEEX_FORWARD_OPERAND_2                     : out STD_LOGIC; -- Adelantar RT

		DEEX_FORWARDED_VALUE                       : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

		DEEX_STALL                                 : out STD_LOGIC; -- Inhibo el avance de la etapa.
		DEEX_INSERT_BUBBLE                         : out STD_LOGIC; -- Inserto un a burbuja en esta etapa.

		-- -----------------------------------
		-- Puertos hacia el registro EXWR
		EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR           : in  STD_LOGIC;
		EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI           : in  STD_LOGIC;
		EXWR_PREVIOUS_STAGE_OP_IS_LOAD_STORE       : in  STD_LOGIC;
		EXWR_PREVIOUS_STAGE_OP_IS_LOAD             : in  STD_LOGIC;
		EXWR_PREVIOUS_STAGE_OP_IS_STORE            : in  STD_LOGIC;
		EXWR_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP : in  STD_LOGIC;

		EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
		EXWR_PREVIOUS_STAGE_OP_BRANCH_TAKEN        : in  STD_LOGIC;

		EXWR_PREVIOUS_STAGE_OP_ALU_RESULT          : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

		EXWR_BRANCH_DESTINATION_ADDR               : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

		EXWR_STALL                                 : out STD_LOGIC; -- Inhibo el avance de la etapa.
		EXWR_INSERT_BUBBLE                         : out STD_LOGIC -- Inserto un a burbuja en esta etapa.

	);
end entity E_PIPELINE_CONTROL;

architecture A_PIPELINE_CONTROL of E_PIPELINE_CONTROL is
begin

	-- Valor adelantado desde la etapa de ejecuci�n a la de decodificaci�n.
	-- Notar que esto no significa que la etapa de decodificaci�n utilice este
	-- valor. Para eso tiene que recibir una o las dos se�ales DEEX_FORWARD_RS
	-- o DEEX_FORWARD_RT
	DEEX_FORWARDED_VALUE <= EXWR_PREVIOUS_STAGE_OP_ALU_RESULT;

	-- En el caso de las instrucciones de salto condicional, el operando inmediato
	-- es la direcci�n destino del salto. Al igual que con la asignaci�n de se�al anterior
	-- esta asignaci�n no implica que se modifique el contador. La decisi�n de modificar
	-- el contador se realiza en el proceso de abajo en funci�n de que la instrucci�n asociada 
	-- efectivamente sea un salto, y que la condici�n haya evaluado a verdader
	PCNT_NEW_VALUE <= EXWR_BRANCH_DESTINATION_ADDR;

	pPipelineControl : process(         --
		DEEX_PREVIOUS_STAGE_OP_IS_ALU_RR, --
		DEEX_PREVIOUS_STAGE_OP_IS_ALU_RI, --
		DEEX_PREVIOUS_STAGE_OP_IS_LOAD_STORE, --
		DEEX_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP, --
		DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID, --
		DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID, --
		EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR, --
		EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI, --
		EXWR_PREVIOUS_STAGE_OP_IS_LOAD, --
		EXWR_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP, --
		EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID, --
		EXWR_PREVIOUS_STAGE_OP_BRANCH_TAKEN --
)
	begin

		-- --
		-- El c�digo siguiente es el encargado de realizar el control de riesgos del pipeline.
		-- El control tiene las siguientes caracter�sticas:
		-- * Resoluci�n de dependencias verdaderas ALU-ALU y ALU-JUMP mediante adelantamiento.
		-- * Resoluci�n de dependencias verdaderas LD-ALU mediante detenciones.
		-- * Anulaci�n de instrucciones de control posteriores a un salto si el salto se produce (no hay delay-slot).   

		-- Si la instrucci�n que est� atravesando EX (entrando al registro EXWR) es un salto condicional.  
		-- entonces verifico si la condici�n de salto es verdadera. Este caso no requiere otros controles, ya que
		-- un salto nunca es fuente de adelantamiento a las instrucciones siguientes (no causa tiene dependencia de datos).
		if (EXWR_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP = '1') then
			--
			-- La instrucci�n en EX es un salto condicional.
			--	
			-- Este es un caso donde nunca es necesario un adelantamiento porque los saltos condicionales no son 
			-- productores de datos. Adicionalmente, este es un excelente lugar para controlar el resultado de los 
			-- saltos condicionales. 

			-- No son necesarias detenciones para tratar este caso.
			PCNT_STALL <= '0';
			FEDE_STALL <= '0';
			DEEX_STALL <= '0';
			EXWR_STALL <= '0';

			-- Verifico si el salto fue tomado
			if (EXWR_PREVIOUS_STAGE_OP_BRANCH_TAKEN = '1') then

				--
				-- LA BIFURCACI�N FUE TOMADA
				--

				-- Es necesario purgar las dos instrucciones que entraron al pipeline
				-- desde que comenz� la ejecuci�n del salto condicional hasta que se evalu� la condic�n, y 
				-- cambiar el valor del contador de programa por la direcci�n de destino del salto.

				-- Esta se�al fuerza a que cuando llegue el flanco de reloj, el contador de programa en lugar
				-- de incrementarse tome el valor de la se�al PCNT_NEW_VALUE que fue conectada a la direcci�n
				-- de destino del salto m�s arriba. 
				PCNT_SET_VALUE <= '1';

				-- Elimino las dos instrucciones detr�s del salto. Las se�ales INSERT_BUBBLE hacen que cuando llegue 
				-- el flanco de reloj, los registros FEDE y DEEX conviertan la instrucci�n que est� en la etapa anterior
				-- (FETCH y DECODE respectivamente) en instrucciones nulas. 
				FEDE_INSERT_BUBBLE <= '1'; -- FEDE va a eliminar la instrucci�n que est� en FETCH
				DEEX_INSERT_BUBBLE <= '1'; -- DEEX va a eliminar la instrucci�n que est� en DECODE
				EXWR_INSERT_BUBBLE <= '0'; -- EXWR funciona normalmente. 

			else

				--
				-- LA BIFURCACI�N NO FUE TOMADA
				--

				-- La bifurcaci�n no fue tomada, por lo que no es necesario purgar las instrucciones que 
				-- entraron al pipeline desde el comienzo de la ejecuci�n del salto.

				PCNT_SET_VALUE <= '0';  -- El contador de programa se incrementar� normalmente

				FEDE_INSERT_BUBBLE <= '0'; -- FEDE propagar� a DECODE la instrucci�n que est� ahora en FECTH.
				DEEX_INSERT_BUBBLE <= '0'; -- DEEX propagar� a EXECUTE la instrucci�n que est� ahora en DECODE.
				EXWR_INSERT_BUBBLE <= '0'; -- EXWR propagar� a WRITEBACK la instrucci�n que est� ahora en EXECUTE.

			end if;
		--
		elsif ((DEEX_PREVIOUS_STAGE_OP_IS_ALU_RR = '1') or (DEEX_PREVIOUS_STAGE_OP_IS_LOAD_STORE = '1')) then

			--
			-- VERIFICO LA NECESIDAD DE REALIZAR ADELANTAMIENTOS EN INSTRUCCIONES CON DOS OPERANDOS
			--
			-- Las instrucciones LOAD/STORE y ALU RR requieren dos operandos (RS y RT/RD) as� que si la 
			-- instrucci�n en EXECUTE modifica cualquiera de esos dos registros se debe realizar un adelantamiento
			-- del valor correspondiente. 
			--

			-- Si la instrucci�n que est� en EXECUTE es una instrucci�n que modifica el valor de su registro destino
			-- entonces verifico adelantamiento si es necesario hacer adelantamiento.
			if ((EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR = '1') or (EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI = '1')) then

				--
				-- LA INSTRUCCI�N EN EXECUTE MODIFICA EL REGISTRO DESTINO
				--

				-- Resuelvo el adelantamiento del operando 1 (Rs)
				if (DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID = EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID) then
					DEEX_FORWARD_OPERAND_1 <= '1'; -- Adelantar operando 1
				else
					DEEX_FORWARD_OPERAND_1 <= '0'; -- No adelantar operando 1
				end if;

				-- Resuelvo el adelantamiento del operando 2 (Rt o Rd)
				if (DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID = EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID) then
					DEEX_FORWARD_OPERAND_2 <= '1'; -- Adelantar operando 2
				else
					DEEX_FORWARD_OPERAND_2 <= '0'; -- No adelantar operando 2
				end if;
			else
				--
				-- LA INSTRUCCI�N EN EXECUTE NO MODIFICA EL REGISTRO DESTINO
				--
				DEEX_FORWARD_OPERAND_1 <= '0'; -- No hacer adelantamiento del operando 1
				DEEX_FORWARD_OPERAND_2 <= '0'; -- No hacer adelantamiento del operando 2
			end if;

			--
			-- TRATAMIENDO DEL CASO LD-ALU
			--
			-- Si se requiere un adelantamiento, y la instrucci�n en EXECUTE es un LOAD es 
			-- el �nico caso donde es necesario meter un stall por RAW. 
			if (((DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID = EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID) or (DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID = EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID)) and (EXWR_PREVIOUS_STAGE_OP_IS_LOAD = '1')) then

				--
				-- INSERTAR DETENCION
				--

				-- No modificar el valor del contador de prorgrama (ver que igual m�s adelante lo inhibimos para que no incremete su valor)
				PCNT_SET_VALUE <= '0';

				-- Inserto una burbuja en el pipeline porque el adelantamiento de WR a EX no puede resolver este problema.

				FEDE_INSERT_BUBBLE <= '0'; -- FEDE no anula la instrucci�n que tiene en puertas.
				DEEX_INSERT_BUBBLE <= '1'; -- DEEX reemplaza la instrucci�n que tiene en puertas por un NOP.
				EXWR_INSERT_BUBBLE <= '0'; -- EXWR no anula la instrucci�n que tiene en puertas.

				PCNT_STALL <= '1';      -- El contador de programa no se incrementa, por lo que la instrucci�n en FETCH se queda ah�.
				FEDE_STALL <= '1';      -- El registro FEDE no se actualiza, por lo que la instrucci�n en DECODE se queda ah� 
				DEEX_STALL <= '0';      -- El registro DEEX funciona normalmente (pero recorder que arriba lo obligamos a que reemplace la instrucci�n por un NOP). 
				EXWR_STALL <= '0';      -- El registro EXWR funciona normalmente. 

			else

				--
				-- NO ES NECESARIO INSERTAR UNA DETENCION
				--

				-- El contador de programa se incrementa normalmente.
				PCNT_SET_VALUE <= '0';

				-- Se deja avanzar el pipeline normalmente. 
				FEDE_INSERT_BUBBLE <= '0';
				DEEX_INSERT_BUBBLE <= '0';
				EXWR_INSERT_BUBBLE <= '0';

				PCNT_STALL <= '0';
				FEDE_STALL <= '0';      -- FEDE propaga a DECODE la instrucci�n que ahora est� en FETCH
				DEEX_STALL <= '0';      -- DEEX propaga a EXECUTE la instrucci�n que ahora est� en DECODE
				EXWR_STALL <= '0';      -- EXWR propaga a WRITEBACK la instrucci�n que ahora est� en EXECUTE

			end if;

		elsif ((DEEX_PREVIOUS_STAGE_OP_IS_ALU_RI = '1') or (DEEX_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP = '1')) then

			--
			-- VERIFICO LA NECESIDAD DE REALIZAR ADELANTAMIENTOS EN INSTRUCCIONES CON UN �NICO OPERANDO
			--
			-- Las instrucciones ALU RI y JUMP requieren un �nico operando (RS) as� que si la 
			-- instrucci�n en EXECUTE modifica ese registro se debe realizar un adelantamiento
			-- del valor correspondiente. 
			--

			-- Si la instrucci�n que est� en EXECUTE es una instrucci�n que modifica el valor de su registro destino
			-- entonces verifico adelantamiento si es necesario hacer adelantamiento.
			if ((EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR = '1') or (EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI = '1')) then

				-- Resuelvo el adelantamiento del operando 1
				if (DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID = EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID) then
					DEEX_FORWARD_OPERAND_1 <= '1'; -- Adelantar operando 1
				else
					DEEX_FORWARD_OPERAND_1 <= '0'; -- No adelantar operando 1
				end if;
			else
				-- No es neceario ning�n adelantamiento del operando 1.
				DEEX_FORWARD_OPERAND_1 <= '0';
			end if;

			-- El operando 2 no se adelanta
			DEEX_FORWARD_OPERAND_2 <= '0';

			--
			-- TRATAMIENDO DEL CASO LD-ALU
			--
			-- Si se requiere un adelantamiento, y la instrucci�n en EXECUTE es un LOAD es 
			-- el �nico caso donde es necesario meter un stall por RAW. 
			if ((DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID = EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID) and (EXWR_PREVIOUS_STAGE_OP_IS_LOAD = '1')) then

				--
				-- INSERTAR DETENCION
				--

				-- No modificar el valor del contador de prorgrama (ver que igual m�s adelante lo inhibimos para que no incremete su valor)
				PCNT_SET_VALUE <= '0';

				-- Inserto una burbuja en el pipeline porque el adelantamiento de WR a EX no puede resolver
				-- este problema.
				FEDE_INSERT_BUBBLE <= '0'; -- FEDE no anula la instrucci�n que tiene en puertas.
				DEEX_INSERT_BUBBLE <= '1'; -- DEEX reemplaza la instrucci�n que tiene en puertas por un NOP.
				EXWR_INSERT_BUBBLE <= '0'; -- EXWR no anula la instrucci�n que tiene en puertas.

				PCNT_STALL <= '1';      -- El contador de programa no se incrementa, por lo que la instrucci�n en FETCH se queda ah�.
				FEDE_STALL <= '1';      -- El registro FEDE no se actualiza, por lo que la instrucci�n en DECODE se queda ah�
				DEEX_STALL <= '0';      -- El registro DEEX funciona normalmente (pero recorder que arriba lo obligamos a que reemplace la instrucci�n por un NOP).
				EXWR_STALL <= '0';      -- El registro EXWR funciona normalmente.

			else

				--
				-- NO ES NECESARIO INSERTAR UNA DETENCION
				--

				-- El contador de programa se incrementa normalmente.
				PCNT_SET_VALUE <= '0';

				-- Se deja avanzar el pipeline normalmente. 
				FEDE_INSERT_BUBBLE <= '0';
				DEEX_INSERT_BUBBLE <= '0';
				EXWR_INSERT_BUBBLE <= '0';

				PCNT_STALL <= '0';
				FEDE_STALL <= '0';      -- FEDE propaga a DECODE la instrucci�n que ahora est� en FETCH
				DEEX_STALL <= '0';      -- DEEX propaga a EXECUTE la instrucci�n que ahora est� en DECODE
				EXWR_STALL <= '0';      -- EXWR propaga a WRITEBACK la instrucci�n que ahora est� en EXECUTE

			end if;

		else

			--
			-- NO HAY UNA INSTRUCCI�N PELIGROSA EN EL PIPELINE. 
			--
			
			-- Este caso puede ejecutarse si hay una burbuja en DECODE.			
			-- Dejo fluir el pipeline.
			
			PCNT_SET_VALUE     <= '0';
			FEDE_INSERT_BUBBLE <= '0';
			DEEX_INSERT_BUBBLE <= '0';
			EXWR_INSERT_BUBBLE <= '0';

			PCNT_STALL <= '0';
			FEDE_STALL <= '0';
			DEEX_STALL <= '0';
			EXWR_STALL <= '0';
		end if;

	end process;

end architecture A_PIPELINE_CONTROL;
