--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingeniería, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * Año 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_CPU_SEGMENTED is
	port(
		-- Señales de control
		CLOCK                          : in  STD_LOGIC; -- Activo en flanco ascendente.
		ASYNC_RESET                    : in  STD_LOGIC; -- Activo en nivel alto.

		-- Acceso a la memoria de programa
		PROG_MEM_ADDR_BUS              : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones de acceso a la RAM
		PROG_MEM_INSTR_BUS             : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos de acceso a la RAM, para leer la instrucción siguiente. 

		-- Acceso de lectura a la memoria de datos
		DATA_MEM_READ_ADDR_BUS         : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones LOADs desde RAM.
		DATA_MEM_READ_DATA_BUS         : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para LOADs desde RAM.

		-- Acceso de escritura a la memoria de datos
		DATA_MEM_WRITE_ADDR_BUS        : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones para STOREs a la RAM.
		DATA_MEM_WRITE_DATA_BUS        : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para STOREs a la RAM. 
		DATA_MEM_WRITE_ENABLE          : out STD_LOGIC; -- Señal de habilitación de escritura para STOREs a la RAM.

		-- Señales de depuración
		MONITOR_STFE_CLOCK_BEATS       : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		MONITOR_STFE_PC_VALUE          : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		MONITOR_STFE_INSTR             : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_STFE_STALLED           : out STD_LOGIC;

		MONITOR_STDE_CLOCK_BEATS       : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		MONITOR_STDE_PC_VALUE          : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		MONITOR_STDE_INSTR             : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_STDE_STALLED           : out STD_LOGIC;
		MONITOR_STDE_FORWARD_OPERAND_1 : out STD_LOGIC;
		MONITOR_STDE_FORWARD_OPERAND_2 : out STD_LOGIC;

		MONITOR_STEX_CLOCK_BEATS       : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		MONITOR_STEX_PC_VALUE          : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		MONITOR_STEX_INSTR             : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_STEX_STALLED           : out STD_LOGIC;
		MONITOR_STEX_BRANCH_TAKEN      : out STD_LOGIC;

		MONITOR_STWR_CLOCK_BEATS       : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
		MONITOR_STWR_PC_VALUE          : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		MONITOR_STWR_INSTR             : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_STWR_STALLED           : out STD_LOGIC;

		-- Puertos de monitoreo de estado de los primeros 8 registros
		MONITOR_RB_R0                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R1                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R2                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R3                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R4                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R5                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R6                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R7                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
	);
end entity E_CPU_SEGMENTED;

-- --------------------------------
-- Definición de la Arquitectura --
-- --------------------------------

architecture A_CPU_SEGMENTED of E_CPU_SEGMENTED is
	for all : C_PROGRAM_COUNTER use entity E_PROGRAM_COUNTER;
	for all : C_COUNTER use entity E_COUNTER;

	for all : C_STAGE_FETCH use entity E_STAGE_FETCH;
	for all : C_REGISTER_FEDE use entity E_REGISTER_FEDE;
	for all : C_STAGE_DECODE use entity E_STAGE_DECODE;
	for all : C_REGISTER_DEEX use entity E_REGISTER_DEEX;
	for all : C_STAGE_EXECUTE use entity E_STAGE_EXECUTE;
	for all : C_REGISTER_EXWR use entity E_REGISTER_EXWR;
	for all : C_STAGE_WRITEBACK use entity E_STAGE_WRITEBACK;

	for all : C_PIPELINE_CONTROL use entity E_PIPELINE_CONTROL;

	for all : C_REGISTER_BANK_2R_1W use entity E_REGISTER_BANK_2R_1W;

	signal REGBANK_PORT_RA_SELECTOR    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal REGBANK_PORT_RA_DATA        : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal REGBANK_PORT_RB_SELECTOR    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal REGBANK_PORT_RB_DATA        : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal REGBANK_PORT_WA_WRITEENABLE : STD_LOGIC;
	signal REGBANK_PORT_WA_SELECTOR    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal REGBANK_PORT_WA_DATA        : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	--------------------

	signal PC_CLOCK_BEATS    : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
	signal PC_PC_VALUE       : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal PC_SYNC_STALL     : STD_LOGIC;
	signal PC_SYNC_SET_VALUE : STD_LOGIC;
	signal PC_NEW_VALUE      : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

	--------------------

	signal STFE_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
	signal STFE_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal STFE_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	--------------------

	signal FEDE_SYNC_STALL         : STD_LOGIC; -- Inhibición de avance.
	signal FEDE_SYNC_INSERT_BUBBLE : STD_LOGIC; -- Clear sincrónico.

	signal FEDE_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal FEDE_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal FEDE_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.

	--------------------

	signal STDE_CLOCK_BEATS            : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal STDE_PC_VALUE               : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal STDE_INSTR                  : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.
	signal STDE_OP_IS_ALU_RR           : STD_LOGIC; -- La clase de instrucción es ALU registro-registro
	signal STDE_OP_IS_ALU_RI           : STD_LOGIC; -- La clase de instrucción es ALU registro-inmediato
	signal STDE_OP_IS_LOAD_STORE       : STD_LOGIC; -- La clase de instrucción es LOAD o es STORE
	signal STDE_OP_IS_LOAD             : STD_LOGIC; -- La clase de instrucción es LOAD.
	signal STDE_OP_IS_STORE            : STD_LOGIC; -- La clase de instrucción es STORE.
	signal STDE_OP_IS_CONDITIONAL_JUMP : STD_LOGIC; -- La clase de instrucción es JUMP.
	signal STDE_ALU_OPERATION_CODE     : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del código de operación ALU (si corresponde).
	signal STDE_JUMP_CONDITION_CODE    : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del código de condición de salto condicional (si corresponde).
	signal STDE_OP_DESTINATION_REG_ID  : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal STDE_OP_OPERAND_1_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal STDE_OP_OPERAND_2_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
	signal STDE_OP_OPERAND_1_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
	signal STDE_OP_OPERAND_2_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros.
	signal STDE_OP_IMM_VALUE           : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucción).

	-----------------------

	signal DEEX_PREVIOUS_STAGE_OP_IS_ALU_RR           : STD_LOGIC;
	signal DEEX_PREVIOUS_STAGE_OP_IS_ALU_RI           : STD_LOGIC;
	signal DEEX_PREVIOUS_STAGE_OP_IS_LOAD_STORE       : STD_LOGIC;
	signal DEEX_PREVIOUS_STAGE_OP_IS_LOAD             : STD_LOGIC;
	signal DEEX_PREVIOUS_STAGE_OP_IS_STORE            : STD_LOGIC;
	signal DEEX_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP : STD_LOGIC;
	signal DEEX_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal DEEX_FORWARD_OPERAND_1                     : STD_LOGIC; -- Adelantar RS 
	signal DEEX_FORWARD_OPERAND_2                     : STD_LOGIC; -- Adelantar RT
	signal DEEX_FORWARDED_VALUE                       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal DEEX_SYNC_STALL                            : STD_LOGIC; -- Inhibición de avance.
	signal DEEX_SYNC_INSERT_BUBBLE                    : STD_LOGIC; -- Clear sincrónico.

	signal DEEX_CLOCK_BEATS            : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal DEEX_PC_VALUE               : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal DEEX_INSTR                  : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.
	signal DEEX_OP_IS_ALU_RR           : STD_LOGIC; -- La clase de instrucción es ALU registro-registro
	signal DEEX_OP_IS_ALU_RI           : STD_LOGIC; -- La clase de instrucción es ALU registro-inmediato
	signal DEEX_OP_IS_LOAD_STORE       : STD_LOGIC; -- La clase de instrucción es LOAD o es STORE
	signal DEEX_OP_IS_LOAD             : STD_LOGIC; -- La clase de instrucción es LOAD.
	signal DEEX_OP_IS_STORE            : STD_LOGIC; -- La clase de instrucción es STORE.
	signal DEEX_OP_IS_CONDITIONAL_JUMP : STD_LOGIC; -- La clase de instrucción es JUMP.
	signal DEEX_ALU_OPERATION_CODE     : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del código de operación ALU (si corresponde).
	signal DEEX_JUMP_CONDITION_CODE    : STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del código de condición de salto condicional (si corresponde).
	signal DEEX_OP_DESTINATION_REG_ID  : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal DEEX_OP_OPERAND_1_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal DEEX_OP_OPERAND_2_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
	signal DEEX_OP_OPERAND_1_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
	signal DEEX_OP_OPERAND_2_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros.
	signal DEEX_OP_IMM_VALUE           : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucción).

	--------------------

	signal STEX_CLOCK_BEATS             : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal STEX_PC_VALUE                : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal STEX_INSTR                   : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.
	signal STEX_OP_IS_ALU_RR            : STD_LOGIC; -- La clase de instrucción es ALU registro-registro
	signal STEX_OP_IS_ALU_RI            : STD_LOGIC; -- La clase de instrucción es ALU registro-inmediato
	signal STEX_OP_IS_LOAD_STORE        : STD_LOGIC; -- La clase de instrucción es LOAD o es STORE
	signal STEX_OP_IS_LOAD              : STD_LOGIC; -- La clase de instrucción es LOAD.
	signal STEX_OP_IS_STORE             : STD_LOGIC; -- La clase de instrucción es STORE.
	signal STEX_OP_IS_CONDITIONAL_JUMP  : STD_LOGIC; -- La clase de instrucción es JUMP.
	signal STEX_OP_DESTINATION_REG_ID   : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal STEX_OP_OPERAND_1_REG_ID     : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal STEX_OP_OPERAND_2_REG_ID     : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
	signal STEX_ALU_RESULT              : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal STEX_BRANCH_TAKEN            : STD_LOGIC;
	signal STEX_BRANCH_DESTINATION_ADDR : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal STEX_OP_OPERAND_1_VALUE      : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.

	--------------------

	signal EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR               : STD_LOGIC;
	signal EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI               : STD_LOGIC;
	signal EXWR_PREVIOUS_STAGE_OP_IS_LOAD_STORE           : STD_LOGIC;
	signal EXWR_PREVIOUS_STAGE_OP_IS_LOAD                 : STD_LOGIC;
	signal EXWR_PREVIOUS_STAGE_OP_IS_STORE                : STD_LOGIC;
	signal EXWR_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP     : STD_LOGIC;
	signal EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID      : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
	signal EXWR_PREVIOUS_STAGE_OP_BRANCH_TAKEN            : STD_LOGIC;
	signal EXWR_PREVIOUS_STAGE_OP_BRANCH_DESTINATION_ADDR : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
	signal EXWR_PREVIOUS_STAGE_OP_ALU_RESULT              : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal EXWR_SYNC_STALL                                : STD_LOGIC; -- Inhibición de avance.
	signal EXWR_SYNC_INSERT_BUBBLE                        : STD_LOGIC; -- Clear sincrónico.

	signal EXWR_CLOCK_BEATS            : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal EXWR_PC_VALUE               : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal EXWR_INSTR                  : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.
	signal EXWR_OP_IS_ALU_RR           : STD_LOGIC; -- La clase de instrucción es ALU registro-registro
	signal EXWR_OP_IS_ALU_RI           : STD_LOGIC; -- La clase de instrucción es ALU registro-inmediato
	signal EXWR_OP_IS_LOAD_STORE       : STD_LOGIC; -- La clase de instrucción es LOAD o es STORE
	signal EXWR_OP_IS_LOAD             : STD_LOGIC; -- La clase de instrucción es LOAD.
	signal EXWR_OP_IS_STORE            : STD_LOGIC; -- La clase de instrucción es STORE.
	signal EXWR_OP_IS_CONDITIONAL_JUMP : STD_LOGIC; -- La clase de instrucción es JUMP.
	signal EXWR_OP_DESTINATION_REG_ID  : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
	signal EXWR_OP_OPERAND_1_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
	signal EXWR_OP_OPERAND_2_REG_ID    : STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
	signal EXWR_ALU_RESULT             : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal EXWR_BRANCH_TAKEN           : STD_LOGIC;
	signal EXWR_OP_OPERAND_1_VALUE     : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.

	--------------------
	signal STWR_CLOCK_BEATS : STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucción en esta etapa.
	signal STWR_PC_VALUE    : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucción en esta etapa.
	signal STWR_INSTR       : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucción a la instrucción en esta etapa.


begin
	rRegisterBank : C_REGISTER_BANK_2R_1W
		generic map(ASYNC_VALUE_UPDATE => true)
		port map(CLOCK               => CLOCK,
			     RESET               => ASYNC_RESET,
			     PORT_RA_SELECTOR    => REGBANK_PORT_RA_SELECTOR,
			     PORT_RA_DATA        => REGBANK_PORT_RA_DATA,
			     PORT_RB_SELECTOR    => REGBANK_PORT_RB_SELECTOR,
			     PORT_RB_DATA        => REGBANK_PORT_RB_DATA,
			     PORT_WA_WRITEENABLE => REGBANK_PORT_WA_WRITEENABLE,
			     PORT_WA_SELECTOR    => REGBANK_PORT_WA_SELECTOR,
			     PORT_WA_DATA        => REGBANK_PORT_WA_DATA,
			     -- Puertos de monitoreo de estado de los primeros 8 registros.
			     -- Estos se acarrean a la interfaz del CPU para que sean vistos desde afuera. 
			     MONITOR_RB_R0       => MONITOR_RB_R0,
			     MONITOR_RB_R1       => MONITOR_RB_R1,
			     MONITOR_RB_R2       => MONITOR_RB_R2,
			     MONITOR_RB_R3       => MONITOR_RB_R3,
			     MONITOR_RB_R4       => MONITOR_RB_R4,
			     MONITOR_RB_R5       => MONITOR_RB_R5,
			     MONITOR_RB_R6       => MONITOR_RB_R6,
			     MONITOR_RB_R7       => MONITOR_RB_R7
		);

	rProgramCounter : C_PROGRAM_COUNTER port map(
			PC_VALUE       => PC_PC_VALUE,
			NEW_VALUE      => PC_NEW_VALUE,
			SYNC_SET_VALUE => PC_SYNC_SET_VALUE,
			SYNC_INHIBIT   => PC_SYNC_STALL,
			ASYNC_RESET    => ASYNC_RESET,
			CLOCK          => CLOCK
		);

	rBeatsCounter : C_COUNTER port map(
			VALUE       => PC_CLOCK_BEATS,
			ASYNC_RESET => ASYNC_RESET,
			CLOCK       => CLOCK
		);

	rStageFetch : C_STAGE_FETCH port map(
			SP_PROG_MEM_ADDR  => PROG_MEM_ADDR_BUS,
			SP_PROG_MEM_INSTR => PROG_MEM_INSTR_BUS,
			--
			BP_CLOCK_BEATS    => PC_CLOCK_BEATS,
			BP_PC_VALUE       => PC_PC_VALUE,
			--
			FP_CLOCK_BEATS    => STFE_CLOCK_BEATS,
			FP_PC_VALUE       => STFE_PC_VALUE,
			FP_INSTR          => STFE_INSTR
		);

	rRegisterFEDE : c_register_fede port map(
			SP_SYNC_STALL         => FEDE_SYNC_STALL,
			SP_SYNC_INSERT_BUBBLE => FEDE_SYNC_INSERT_BUBBLE,
			SP_ASYNC_RESET        => ASYNC_RESET,
			SP_CLOCK              => CLOCK,
			--
			BP_CLOCK_BEATS        => STFE_CLOCK_BEATS,
			BP_PC_VALUE           => STFE_PC_VALUE,
			BP_INSTR              => STFE_INSTR,
			--
			FP_CLOCK_BEATS        => FEDE_CLOCK_BEATS,
			FP_PC_VALUE           => FEDE_PC_VALUE,
			FP_INSTR              => FEDE_INSTR
		);

	rStageDecode : C_STAGE_DECODE port map(
			SP_REGISTER_RS_SELECTOR   => REGBANK_PORT_RA_SELECTOR,
			SP_REGISTER_RT_SELECTOR   => REGBANK_PORT_RB_SELECTOR,
			SP_REGISTER_RS_VALUE      => REGBANK_PORT_RA_DATA,
			SP_REGISTER_RT_VALUE      => REGBANK_PORT_RB_DATA,
			--
			BP_CLOCK_BEATS            => FEDE_CLOCK_BEATS,
			BP_PC_VALUE               => FEDE_PC_VALUE,
			BP_INSTR                  => FEDE_INSTR,
			--
			FP_CLOCK_BEATS            => STDE_CLOCK_BEATS,
			FP_PC_VALUE               => STDE_PC_VALUE,
			FP_INSTR                  => STDE_INSTR,
			FP_OP_IS_ALU_RR           => STDE_OP_IS_ALU_RR,
			FP_OP_IS_ALU_RI           => STDE_OP_IS_ALU_RI,
			FP_OP_IS_LOAD_STORE       => STDE_OP_IS_LOAD_STORE,
			FP_OP_IS_LOAD             => STDE_OP_IS_LOAD,
			FP_OP_IS_STORE            => STDE_OP_IS_STORE,
			FP_OP_IS_CONDITIONAL_JUMP => STDE_OP_IS_CONDITIONAL_JUMP,
			FP_ALU_OPERATION_CODE     => STDE_ALU_OPERATION_CODE,
			FP_JUMP_CONDITION_CODE    => STDE_JUMP_CONDITION_CODE,
			FP_OP_DESTINATION_REG_ID  => STDE_OP_DESTINATION_REG_ID,
			FP_OP_OPERAND_1_REG_ID    => STDE_OP_OPERAND_1_REG_ID,
			FP_OP_OPERAND_2_REG_ID    => STDE_OP_OPERAND_2_REG_ID,
			FP_OP_OPERAND_1_VALUE     => STDE_OP_OPERAND_1_VALUE,
			FP_OP_OPERAND_2_VALUE     => STDE_OP_OPERAND_2_VALUE,
			FP_OP_IMM_VALUE           => STDE_OP_IMM_VALUE
		);

	rRegisterDEEX : C_REGISTER_DEEX port map(
			SP_PREVIOUS_STAGE_OP_IS_ALU_RR           => DEEX_PREVIOUS_STAGE_OP_IS_ALU_RR,
			SP_PREVIOUS_STAGE_OP_IS_ALU_RI           => DEEX_PREVIOUS_STAGE_OP_IS_ALU_RI,
			SP_PREVIOUS_STAGE_OP_IS_LOAD_STORE       => DEEX_PREVIOUS_STAGE_OP_IS_LOAD_STORE,
			SP_PREVIOUS_STAGE_OP_IS_LOAD             => DEEX_PREVIOUS_STAGE_OP_IS_LOAD,
			SP_PREVIOUS_STAGE_OP_IS_STORE            => DEEX_PREVIOUS_STAGE_OP_IS_STORE,
			SP_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP => DEEX_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP,
			SP_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  => DEEX_PREVIOUS_STAGE_OP_DESTINATION_REG_ID,
			SP_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID    => DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID,
			SP_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID    => DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID,
			SP_FORWARD_OPERAND_1                     => DEEX_FORWARD_OPERAND_1,
			SP_FORWARD_OPERAND_2                     => DEEX_FORWARD_OPERAND_2,
			SP_FORWARDED_VALUE                       => DEEX_FORWARDED_VALUE,
			SP_SYNC_STALL                            => DEEX_SYNC_STALL,
			SP_SYNC_INSERT_BUBBLE                    => DEEX_SYNC_INSERT_BUBBLE,
			SP_ASYNC_RESET                           => ASYNC_RESET,
			SP_CLOCK                                 => CLOCK,
			--
			BP_CLOCK_BEATS                           => STDE_CLOCK_BEATS,
			BP_PC_VALUE                              => STDE_PC_VALUE,
			BP_INSTR                                 => STDE_INSTR,
			BP_OP_IS_ALU_RR                          => STDE_OP_IS_ALU_RR,
			BP_OP_IS_ALU_RI                          => STDE_OP_IS_ALU_RI,
			BP_OP_IS_LOAD_STORE                      => STDE_OP_IS_LOAD_STORE,
			BP_OP_IS_LOAD                            => STDE_OP_IS_LOAD,
			BP_OP_IS_STORE                           => STDE_OP_IS_STORE,
			BP_OP_IS_CONDITIONAL_JUMP                => STDE_OP_IS_CONDITIONAL_JUMP,
			BP_ALU_OPERATION_CODE                    => STDE_ALU_OPERATION_CODE,
			BP_JUMP_CONDITION_CODE                   => STDE_JUMP_CONDITION_CODE,
			BP_OP_DESTINATION_REG_ID                 => STDE_OP_DESTINATION_REG_ID,
			BP_OP_OPERAND_1_REG_ID                   => STDE_OP_OPERAND_1_REG_ID,
			BP_OP_OPERAND_2_REG_ID                   => STDE_OP_OPERAND_2_REG_ID,
			BP_OP_OPERAND_1_VALUE                    => STDE_OP_OPERAND_1_VALUE,
			BP_OP_OPERAND_2_VALUE                    => STDE_OP_OPERAND_2_VALUE,
			BP_OP_IMM_VALUE                          => STDE_OP_IMM_VALUE,
			--
			FP_CLOCK_BEATS                           => DEEX_CLOCK_BEATS,
			FP_PC_VALUE                              => DEEX_PC_VALUE,
			FP_INSTR                                 => DEEX_INSTR,
			FP_OP_IS_ALU_RR                          => DEEX_OP_IS_ALU_RR,
			FP_OP_IS_ALU_RI                          => DEEX_OP_IS_ALU_RI,
			FP_OP_IS_LOAD_STORE                      => DEEX_OP_IS_LOAD_STORE,
			FP_OP_IS_LOAD                            => DEEX_OP_IS_LOAD,
			FP_OP_IS_STORE                           => DEEX_OP_IS_STORE,
			FP_OP_IS_CONDITIONAL_JUMP                => DEEX_OP_IS_CONDITIONAL_JUMP,
			FP_ALU_OPERATION_CODE                    => DEEX_ALU_OPERATION_CODE,
			FP_JUMP_CONDITION_CODE                   => DEEX_JUMP_CONDITION_CODE,
			FP_OP_DESTINATION_REG_ID                 => DEEX_OP_DESTINATION_REG_ID,
			FP_OP_OPERAND_1_REG_ID                   => DEEX_OP_OPERAND_1_REG_ID,
			FP_OP_OPERAND_2_REG_ID                   => DEEX_OP_OPERAND_2_REG_ID,
			FP_OP_OPERAND_1_VALUE                    => DEEX_OP_OPERAND_1_VALUE,
			FP_OP_OPERAND_2_VALUE                    => DEEX_OP_OPERAND_2_VALUE,
			FP_OP_IMM_VALUE                          => DEEX_OP_IMM_VALUE
		);

	rStageExecute : C_STAGE_EXECUTE port map(
			BP_CLOCK_BEATS                => DEEX_CLOCK_BEATS,
			BP_PC_VALUE                   => DEEX_PC_VALUE,
			BP_INSTR                      => DEEX_INSTR,
			BP_OP_IS_ALU_RR               => DEEX_OP_IS_ALU_RR,
			BP_OP_IS_ALU_RI               => DEEX_OP_IS_ALU_RI,
			BP_OP_IS_LOAD_STORE           => DEEX_OP_IS_LOAD_STORE,
			BP_OP_IS_LOAD                 => DEEX_OP_IS_LOAD,
			BP_OP_IS_STORE                => DEEX_OP_IS_STORE,
			BP_OP_IS_CONDITIONAL_JUMP     => DEEX_OP_IS_CONDITIONAL_JUMP,
			BP_ALU_OPERATION_CODE         => DEEX_ALU_OPERATION_CODE,
			BP_JUMP_CONDITION_CODE        => DEEX_JUMP_CONDITION_CODE,
			BP_OP_DESTINATION_REG_ID      => DEEX_OP_DESTINATION_REG_ID,
			BP_OP_OPERAND_1_REG_ID        => DEEX_OP_OPERAND_1_REG_ID,
			BP_OP_OPERAND_2_REG_ID        => DEEX_OP_OPERAND_2_REG_ID,
			BP_OP_OPERAND_1_VALUE         => DEEX_OP_OPERAND_1_VALUE,
			BP_OP_OPERAND_2_VALUE         => DEEX_OP_OPERAND_2_VALUE,
			BP_OP_IMM_VALUE               => DEEX_OP_IMM_VALUE,
			--
			FP_CLOCK_BEATS                => STEX_CLOCK_BEATS,
			FP_PC_VALUE                   => STEX_PC_VALUE,
			FP_INSTR                      => STEX_INSTR,
			FP_OP_IS_ALU_RR               => STEX_OP_IS_ALU_RR,
			FP_OP_IS_ALU_RI               => STEX_OP_IS_ALU_RI,
			FP_OP_IS_LOAD_STORE           => STEX_OP_IS_LOAD_STORE,
			FP_OP_IS_LOAD                 => STEX_OP_IS_LOAD,
			FP_OP_IS_STORE                => STEX_OP_IS_STORE,
			FP_OP_IS_CONDITIONAL_JUMP     => STEX_OP_IS_CONDITIONAL_JUMP,
			FP_OP_DESTINATION_REG_ID      => STEX_OP_DESTINATION_REG_ID,
			FP_OP_OPERAND_1_REG_ID        => STEX_OP_OPERAND_1_REG_ID,
			FP_OP_OPERAND_2_REG_ID        => STEX_OP_OPERAND_2_REG_ID,
			FP_OP_OPERAND_1_VALUE         => STEX_OP_OPERAND_1_VALUE,
			FP_ALU_RESULT                 => STEX_ALU_RESULT,
			FP_BRANCH_TAKEN               => STEX_BRANCH_TAKEN,
			FP_OP_BRANCH_DESTINATION_ADDR => STEX_BRANCH_DESTINATION_ADDR
		);

	rRegisterEXWR : C_REGISTER_EXWR port map(
			SP_PREVIOUS_STAGE_OP_IS_ALU_RR               => EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR,
			SP_PREVIOUS_STAGE_OP_IS_ALU_RI               => EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI,
			SP_PREVIOUS_STAGE_OP_IS_LOAD_STORE           => EXWR_PREVIOUS_STAGE_OP_IS_LOAD_STORE,
			SP_PREVIOUS_STAGE_OP_IS_LOAD                 => EXWR_PREVIOUS_STAGE_OP_IS_LOAD,
			SP_PREVIOUS_STAGE_OP_IS_STORE                => EXWR_PREVIOUS_STAGE_OP_IS_STORE,
			SP_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP     => EXWR_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP,
			SP_PREVIOUS_STAGE_OP_DESTINATION_REG_ID      => EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID,
			SP_PREVIOUS_STAGE_OP_BRANCH_TAKEN            => EXWR_PREVIOUS_STAGE_OP_BRANCH_TAKEN,
			SP_PREVIOUS_STAGE_OP_BRANCH_DESTINATION_ADDR => EXWR_PREVIOUS_STAGE_OP_BRANCH_DESTINATION_ADDR,
			SP_PREVIOUS_STAGE_OP_ALU_RESULT              => EXWR_PREVIOUS_STAGE_OP_ALU_RESULT,
			SP_SYNC_STALL                                => EXWR_SYNC_STALL,
			SP_SYNC_INSERT_BUBBLE                        => EXWR_SYNC_INSERT_BUBBLE,
			SP_ASYNC_RESET                               => ASYNC_RESET,
			SP_CLOCK                                     => CLOCK,
			--
			BP_CLOCK_BEATS                               => STEX_CLOCK_BEATS,
			BP_PC_VALUE                                  => STEX_PC_VALUE,
			BP_INSTR                                     => STEX_INSTR,
			BP_OP_IS_ALU_RR                              => STEX_OP_IS_ALU_RR,
			BP_OP_IS_ALU_RI                              => STEX_OP_IS_ALU_RI,
			BP_OP_IS_LOAD_STORE                          => STEX_OP_IS_LOAD_STORE,
			BP_OP_IS_LOAD                                => STEX_OP_IS_LOAD,
			BP_OP_IS_STORE                               => STEX_OP_IS_STORE,
			BP_OP_IS_CONDITIONAL_JUMP                    => STEX_OP_IS_CONDITIONAL_JUMP,
			BP_OP_DESTINATION_REG_ID                     => STEX_OP_DESTINATION_REG_ID,
			BP_OP_OPERAND_1_REG_ID                       => STEX_OP_OPERAND_1_REG_ID,
			BP_OP_OPERAND_2_REG_ID                       => STEX_OP_OPERAND_2_REG_ID,
			BP_OP_OPERAND_1_VALUE                        => STEX_OP_OPERAND_1_VALUE,
			BP_ALU_RESULT                                => STEX_ALU_RESULT,
			BP_OP_BRANCH_TAKEN                           => STEX_BRANCH_TAKEN,
			BP_OP_BRANCH_DESTINATION_ADDR                => STEX_BRANCH_DESTINATION_ADDR,
			--
			FP_CLOCK_BEATS                               => EXWR_CLOCK_BEATS,
			FP_PC_VALUE                                  => EXWR_PC_VALUE,
			FP_INSTR                                     => EXWR_INSTR,
			FP_OP_IS_ALU_RR                              => EXWR_OP_IS_ALU_RR,
			FP_OP_IS_ALU_RI                              => EXWR_OP_IS_ALU_RI,
			FP_OP_IS_LOAD_STORE                          => EXWR_OP_IS_LOAD_STORE,
			FP_OP_IS_LOAD                                => EXWR_OP_IS_LOAD,
			FP_OP_IS_STORE                               => EXWR_OP_IS_STORE,
			FP_OP_IS_CONDITIONAL_JUMP                    => EXWR_OP_IS_CONDITIONAL_JUMP,
			FP_OP_DESTINATION_REG_ID                     => EXWR_OP_DESTINATION_REG_ID,
			FP_OP_OPERAND_1_REG_ID                       => EXWR_OP_OPERAND_1_REG_ID,
			FP_OP_OPERAND_2_REG_ID                       => EXWR_OP_OPERAND_2_REG_ID,
			FP_OP_OPERAND_1_VALUE                        => EXWR_OP_OPERAND_1_VALUE,
			FP_ALU_RESULT                                => EXWR_ALU_RESULT,
			FP_JUMP_EVALUATION                           => EXWR_BRANCH_TAKEN
		);

	rStageWriteback : C_STAGE_WRITEBACK port map(
			SP_RAM_READ_ADDR_BUS       => DATA_MEM_READ_ADDR_BUS,
			SP_RAM_READ_DATA_BUS       => DATA_MEM_READ_DATA_BUS,
			SP_RAM_WRITE_ADDR_BUS      => DATA_MEM_WRITE_ADDR_BUS,
			SP_RAM_WRITE_DATA_BUS      => DATA_MEM_WRITE_DATA_BUS,
			SP_RAM_WRITE_ENABLE        => DATA_MEM_WRITE_ENABLE,
			SP_REGISTER_WRITE_SELECTOR => REGBANK_PORT_WA_SELECTOR,
			SP_REGISTER_WRITE_DATA     => REGBANK_PORT_WA_DATA,
			SP_REGISTER_WRITE_ENABLE   => REGBANK_PORT_WA_WRITEENABLE,
			--
			BP_CLOCK_BEATS             => EXWR_CLOCK_BEATS,
			BP_PC_VALUE                => EXWR_PC_VALUE,
			BP_INSTR                   => EXWR_INSTR,
			BP_OP_IS_ALU_RR            => EXWR_OP_IS_ALU_RR,
			BP_OP_IS_ALU_RI            => EXWR_OP_IS_ALU_RI,
			BP_OP_IS_LOAD_STORE        => EXWR_OP_IS_LOAD_STORE,
			BP_OP_IS_LOAD              => EXWR_OP_IS_LOAD,
			BP_OP_IS_STORE             => EXWR_OP_IS_STORE,
			BP_OP_IS_CONDITIONAL_JUMP  => EXWR_OP_IS_CONDITIONAL_JUMP,
			BP_OP_DESTINATION_REG_ID   => EXWR_OP_DESTINATION_REG_ID,
			BP_OP_OPERAND_1_REG_ID     => EXWR_OP_OPERAND_1_REG_ID,
			BP_OP_OPERAND_2_REG_ID     => EXWR_OP_OPERAND_2_REG_ID,
			BP_OP_OPERAND_1_VALUE      => EXWR_OP_OPERAND_1_VALUE,
			BP_ALU_RESULT              => EXWR_ALU_RESULT,
			BP_BRANCH_TAKEN            => EXWR_BRANCH_TAKEN,
			--
			FP_CLOCK_BEATS             => STWR_CLOCK_BEATS,
			FP_PC_VALUE                => STWR_PC_VALUE,
			FP_INSTR                   => STWR_INSTR
		);

	----------------------

	rPipelineControl : C_PIPELINE_CONTROL
		port map(PCNT_STALL                                 => PC_SYNC_STALL,
			     PCNT_SET_VALUE                             => PC_SYNC_SET_VALUE,
			     PCNT_NEW_VALUE                             => PC_NEW_VALUE,
			     ---
			     FEDE_STALL                                 => FEDE_SYNC_STALL,
			     FEDE_INSERT_BUBBLE                         => FEDE_SYNC_INSERT_BUBBLE,
			     ---
			     DEEX_PREVIOUS_STAGE_OP_IS_ALU_RR           => DEEX_PREVIOUS_STAGE_OP_IS_ALU_RR,
			     DEEX_PREVIOUS_STAGE_OP_IS_ALU_RI           => DEEX_PREVIOUS_STAGE_OP_IS_ALU_RI,
			     DEEX_PREVIOUS_STAGE_OP_IS_LOAD_STORE       => DEEX_PREVIOUS_STAGE_OP_IS_LOAD_STORE,
			     DEEX_PREVIOUS_STAGE_OP_IS_LOAD             => DEEX_PREVIOUS_STAGE_OP_IS_LOAD,
			     DEEX_PREVIOUS_STAGE_OP_IS_STORE            => DEEX_PREVIOUS_STAGE_OP_IS_STORE,
			     DEEX_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP => DEEX_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP,
			     DEEX_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  => DEEX_PREVIOUS_STAGE_OP_DESTINATION_REG_ID,
			     DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID    => DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID,
			     DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID    => DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID,
			     DEEX_FORWARD_OPERAND_1                     => DEEX_FORWARD_OPERAND_1,
			     DEEX_FORWARD_OPERAND_2                     => DEEX_FORWARD_OPERAND_2,
			     DEEX_FORWARDED_VALUE                       => DEEX_FORWARDED_VALUE,
			     DEEX_STALL                                 => DEEX_SYNC_STALL,
			     DEEX_INSERT_BUBBLE                         => DEEX_SYNC_INSERT_BUBBLE,
			     ---
			     EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR           => EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR,
			     EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI           => EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI,
			     EXWR_PREVIOUS_STAGE_OP_IS_LOAD_STORE       => EXWR_PREVIOUS_STAGE_OP_IS_LOAD_STORE,
			     EXWR_PREVIOUS_STAGE_OP_IS_LOAD             => EXWR_PREVIOUS_STAGE_OP_IS_LOAD,
			     EXWR_PREVIOUS_STAGE_OP_IS_STORE            => EXWR_PREVIOUS_STAGE_OP_IS_STORE,
			     EXWR_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP => EXWR_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP,
			     EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  => EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID,
			     EXWR_PREVIOUS_STAGE_OP_BRANCH_TAKEN        => EXWR_PREVIOUS_STAGE_OP_BRANCH_TAKEN,
			     EXWR_PREVIOUS_STAGE_OP_ALU_RESULT          => EXWR_PREVIOUS_STAGE_OP_ALU_RESULT,
			     EXWR_BRANCH_DESTINATION_ADDR               => EXWR_PREVIOUS_STAGE_OP_BRANCH_DESTINATION_ADDR,
			     EXWR_STALL                                 => EXWR_SYNC_STALL,
			     EXWR_INSERT_BUBBLE                         => EXWR_SYNC_INSERT_BUBBLE);

	-- Conecto las señales de monitoreo externo
	MONITOR_STFE_CLOCK_BEATS <= STFE_CLOCK_BEATS;
	MONITOR_STFE_PC_VALUE    <= STFE_PC_VALUE;
	MONITOR_STFE_INSTR       <= STFE_INSTR;
	MONITOR_STFE_STALLED     <= PC_SYNC_STALL;

	MONITOR_STDE_CLOCK_BEATS       <= STDE_CLOCK_BEATS;
	MONITOR_STDE_PC_VALUE          <= STDE_PC_VALUE;
	MONITOR_STDE_INSTR             <= STDE_INSTR;
	MONITOR_STDE_STALLED           <= FEDE_SYNC_STALL;
	MONITOR_STDE_FORWARD_OPERAND_1 <= DEEX_FORWARD_OPERAND_1;
	MONITOR_STDE_FORWARD_OPERAND_2 <= DEEX_FORWARD_OPERAND_2;

	MONITOR_STEX_CLOCK_BEATS  <= STEX_CLOCK_BEATS;
	MONITOR_STEX_PC_VALUE     <= STEX_PC_VALUE;
	MONITOR_STEX_INSTR        <= STEX_INSTR;
	MONITOR_STEX_STALLED      <= DEEX_SYNC_STALL;
	MONITOR_STEX_BRANCH_TAKEN <= STEX_BRANCH_TAKEN;

	MONITOR_STWR_CLOCK_BEATS <= STWR_CLOCK_BEATS;
	MONITOR_STWR_PC_VALUE    <= STWR_PC_VALUE;
	MONITOR_STWR_INSTR       <= STWR_INSTR;
	MONITOR_STWR_STALLED     <= EXWR_SYNC_STALL;

end architecture A_CPU_SEGMENTED;
