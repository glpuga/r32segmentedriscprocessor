--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package NANO_PET_CONST_PKG is

	-- --
	--
	-- Contantes que dimensionan el sistema
	constant DATA_BUS_SIZE    : integer := 32; -- Tama�o de una palabra (instrucci�n o datos)
	constant CLASS_FIELD_SIZE : integer := 2; -- Ancho del campo Clase en la palabra de instrucci�n.
	constant CODE_FIELD_SIZE  : integer := 4; -- Ancho del campo C�digo (de ALU y de condici�n de salto) en la palabra de instrucci�n.
	constant REG_SEL_SIZE     : integer := 5; -- Ancho del bus de direcciones de los registros
	constant ADDR_BUS_SIZE    : integer := 20; -- Ancho del bus de direcciones.

	-- Cantidad Bits de la parte de abajo de la direcci�n que debido al alineamiento de palabra siempre deben estar en cero.
	constant LOWEST_ADDR_BIT : integer := 2;

	-- --
	--
	-- Constantes calculadas autom�ticamente
	constant PC_INCREMENT       : integer := DATA_BUS_SIZE / 8; -- Incremento qu�ntico del contador de programa
	constant REG_COUNT          : integer := 2 ** REG_SEL_SIZE; -- Cantidad de registros en el banco de registros
	constant BEATS_COUNTER_SIZE : integer := 32; -- Ancho del contador de ciclos que cuenta pulsos del reloj.


	-- --
	--
	-- Constantes de temporizaci�n. Por ahora esto no est� en uso. 
	--

	-- Unidad base de los tiempos de propagaci�n (depender�a de la tecnolog�a). Corresponder�a
	-- aproximadamente a un FO4 (Fan-Out of Four).
	constant TIME_DELAY_UNIT : time := 0 ns;

	-- Tiempos de propagaci�n de los diversos subsistemas, funci�n de la complejidad.
	constant TIME_DELAY_PROPAGATION     : time := TIME_DELAY_UNIT;
	constant TIME_DELAY_WORDEXTENDER    : time := TIME_DELAY_UNIT * 3;
	constant TIME_DELAY_REGISTER        : time := TIME_DELAY_UNIT * 3;
	constant TIME_DELAY_PROGRAM_COUNTER : time := TIME_DELAY_UNIT * 3;
	constant TIME_DELAY_MULTIPLEXERS    : time := TIME_DELAY_UNIT * 5;
	constant TIME_DELAY_BARRELSHIFTER   : time := TIME_DELAY_UNIT * 5;
	constant TIME_DELAY_CONTROLUNIT     : time := TIME_DELAY_UNIT * 7;
	constant TIME_DELAY_JEU             : time := TIME_DELAY_UNIT * 8;
	constant TIME_DELAY_ALU             : time := TIME_DELAY_UNIT * 10;
	constant TIME_DELAY_MEMORY          : time := TIME_DELAY_UNIT * 15;

end package NANO_PET_CONST_PKG;