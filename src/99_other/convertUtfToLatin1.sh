#!/bin/bash

for x in `find . -name '*.i' -or -name '*.vhdl' -or -name '*.vhd'`

do

	echo "Converting $x"
	iconv --verbose -f UTF-8 -t ISO-8859-1 "$x" > "$x.latin1"
	rm $x
	mv "$x.latin1" $x

done
