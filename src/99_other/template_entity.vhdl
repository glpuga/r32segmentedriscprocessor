--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_TEMPLATE is
--	generic(
--      REGISTER_WIDTH : integer := 4;
--		DELAY          : time    := 0 ns
--	);
end entity E_TEMPLATE;

-- --------------------------------
-- Definici�n de la Arquitectura --
-- --------------------------------

architecture A_TEMPLATE of E_TEMPLATE is

--	signal TEMPLATE_SIGNAL : std_logic_vector((REGISTER_WIDTH - 1) downto 0);

begin

--	TEMPLATE_SIGNAL <= "0000" after DELAY;

end architecture A_TEMPLATE;
