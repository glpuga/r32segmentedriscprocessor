--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity TB_E_TEMPLATE is
-- body
end entity TB_E_TEMPLATE;

-- --------------------------------

architecture TB_A_TEMPLATE of TB_E_TEMPLATE is

-- for all: C_TEMPLATE use entity E_TEMPLATE;

begin
	test_process : process
	begin
		report "Comienzo en ensayo de multiplexor" severity note;
		wait for 1 us;
		report "Fin en ensayo de multiplexor" severity note;
		wait;

	end process;

end architecture TB_A_TEMPLATE;
