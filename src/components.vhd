--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier铆a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A駉 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.all;

package NANO_PET_COMP_PKG is
	component C_CLKGEN is
		generic(
			PERIOD : time := 1 us       -- Per铆odo de la se馻l de reloj a generar. 
		);
		port(
			CLKOUT : out STD_LOGIC      -- Se帽al de reloj generada.
		);
	end component C_CLKGEN;

	component C_MULTIPLEXER2 is
		generic(
			PORT_WIDTH : integer := 32;
			DELAY      : time    := TIME_DELAY_MULTIPLEXERS
		);
		port(
			IPORT00 : in  STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0); -- Entrada 0
			IPORT01 : in  STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0); -- Entrada 1
			SEL     : in  STD_LOGIC;    -- L铆nea de selecci贸n. 
			OPORT   : out STD_LOGIC_VECTOR((PORT_WIDTH - 1) downto 0) -- Salida.
		);
	end component C_MULTIPLEXER2;

	component C_REGISTER is
		generic(
			REGISTER_WIDTH : integer := 32;
			DELAY          : time    := TIME_DELAY_REGISTER
		);
		port(
			-- Input ports 
			DATAIN  : in  STD_LOGIC_VECTOR((REGISTER_WIDTH - 1) downto 0);
			CLOCK   : in  STD_LOGIC;
			RESET   : in  STD_LOGIC;
			-- OUtput Ports
			DATAOUT : out STD_LOGIC_VECTOR((REGISTER_WIDTH - 1) downto 0)
		);
	end component C_REGISTER;

	component C_INTERSTAGE_REGISTER is
		generic(
			INTERSTAGE_REGISTER_WIDTH : integer := 32;
			DELAY                     : time    := TIME_DELAY_REGISTER
		);
		port(
			-- Puertos de datos sincr贸nicos 
			DATAIN       : in  STD_LOGIC_VECTOR((INTERSTAGE_REGISTER_WIDTH - 1) downto 0);
			DATAOUT      : out STD_LOGIC_VECTOR((INTERSTAGE_REGISTER_WIDTH - 1) downto 0);
			-- Puertos de datos sincr贸nicos
			SYNC_INHIBIT : in  STD_LOGIC; -- activo en alto
			SYNC_CLEAR   : in  STD_LOGIC; -- activo en alto		
			-- Puertos asincr贸nicos
			ASYNC_RESET  : in  STD_LOGIC; -- activo en alto
			-- Reloj
			CLOCK        : in  STD_LOGIC -- activo en flanco ascendente
		);
	end component C_INTERSTAGE_REGISTER;

	component C_REGISTER_BANK_2R_1W is
		generic(
			ASYNC_VALUE_UPDATE : boolean; -- Debe ser true para el procesador segmentado, y falso para el no segmentado.
			WORD_WIDTH         : integer := 32;
			SELECTOR_WIDTH     : integer := 5;
			DELAY              : time    := TIME_DELAY_MEMORY
		);
		port(
			-- Entradas de control
			CLOCK               : in  STD_LOGIC;
			RESET               : in  STD_LOGIC;
			-- Puerto de A de lectura
			PORT_RA_SELECTOR    : in  STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
			PORT_RA_DATA        : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			-- Puerto B de lectura
			PORT_RB_SELECTOR    : in  STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
			PORT_RB_DATA        : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			-- Puerto de escritura
			PORT_WA_WRITEENABLE : in  STD_LOGIC;
			PORT_WA_SELECTOR    : in  STD_LOGIC_VECTOR((SELECTOR_WIDTH - 1) downto 0);
			PORT_WA_DATA        : in  STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);

			-- Puertos de monitoreo de estado de los primeros 8 registros
			MONITOR_RB_R0       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_RB_R1       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_RB_R2       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_RB_R3       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_RB_R4       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_RB_R5       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_RB_R6       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_RB_R7       : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0)
		);
	end component C_REGISTER_BANK_2R_1W;

	component C_WORDEXTENDER is
		generic(
			DELAY        : time := TIME_DELAY_WORDEXTENDER;
			INPUT_WIDTH  : integer;
			OUTPUT_WIDTH : integer
		);
		port(
			DATAIN     : in  STD_LOGIC_VECTOR((INPUT_WIDTH - 1) downto 0);
			ISUNSIGNED : in  STD_LOGIC;
			DATAOUT    : out STD_LOGIC_VECTOR((OUTPUT_WIDTH - 1) downto 0)
		);
	end component C_WORDEXTENDER;

	component C_PROGRAM_COUNTER is
		generic(
			COUNTER_WIDTH     : integer := ADDR_BUS_SIZE;
			COUNTER_INCREMENT : integer := PC_INCREMENT;
			DELAY             : time    := TIME_DELAY_PROGRAM_COUNTER
		);
		port(
			-- Puertos sincr贸nicos
			PC_VALUE       : out STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0);
			NEW_VALUE      : in  STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0);
			SYNC_SET_VALUE : in  STD_LOGIC; -- Establece el valor del contador en el valor de VALUE.
			SYNC_INHIBIT   : in  STD_LOGIC; -- Inhibidor de cuenta, activo en alto.
			-- Puertos asincr贸nicos
			ASYNC_RESET    : in  STD_LOGIC; -- Activo en alto
			-- Reloj
			CLOCK          : in  STD_LOGIC);
	end component C_PROGRAM_COUNTER;

	component C_COUNTER is
		generic(
			COUNTER_WIDTH : integer := BEATS_COUNTER_SIZE;
			DELAY         : time    := TIME_DELAY_PROGRAM_COUNTER
		);
		port(
			-- Puertos sincr贸nicos
			VALUE       : out STD_LOGIC_VECTOR((COUNTER_WIDTH - 1) downto 0);
			-- Puertos asincr贸nicos
			ASYNC_RESET : in  STD_LOGIC; -- Activo en alto
			-- Reloj
			CLOCK       : in  STD_LOGIC
		);
	end component C_COUNTER;

	component C_CONTROLUNIT is
		generic(
			DELAY : time := TIME_DELAY_CONTROlUNIT
		);
		port(
			ICLASS              : in  STD_LOGIC_VECTOR((CLASS_FIELD_SIZE - 1) downto 0);
			ICODE               : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);

			OP_IS_ALU_RR        : out STD_LOGIC;
			OP_IS_ALU_RI        : out STD_LOGIC;
			OP_IS_LOAD_STORE    : out STD_LOGIC;
			OP_IS_LOAD          : out STD_LOGIC;
			OP_IS_STORE         : out STD_LOGIC;
			OP_IS_C_JUMP        : out STD_LOGIC;

			ALU_OPERATION_CODE  : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
			JUMP_CONDITION_CODE : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0)
		);

	end component C_CONTROLUNIT;

	component C_ALU is
		generic(
			DELAY     : time    := TIME_DELAY_ALU;
			ALU_WIDTH : integer := DATA_BUS_SIZE
		);
		port(
			OP_CODE   : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
			OPERAND_A : in  STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);
			OPERAND_B : in  STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0);

			RESULT    : out STD_LOGIC_VECTOR((ALU_WIDTH - 1) downto 0)
		);
	end component C_ALU;

	component C_JEU is                  -- In case you are wondering, "Jump Evaluation Unit"
		generic(
			DELAY     : time    := TIME_DELAY_JEU;
			JEU_WIDTH : integer := DATA_BUS_SIZE
		);
		port(
			TEST_CONDITION : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
			TEST_VALUE     : in  STD_LOGIC_VECTOR((JEU_WIDTH - 1) downto 0);
			TEST_RESULT    : out STD_LOGIC
		);

	end component C_JEU;

	component C_SHIFT_RIGHT_32 is
		port(
			INPUT    : in  STD_LOGIC_VECTOR(31 downto 0);
			BITCOUNT : in  STD_LOGIC_VECTOR(4 downto 0);

			UROTATE  : out STD_LOGIC_VECTOR(31 downto 0);
			USHIFT   : out STD_LOGIC_VECTOR(31 downto 0);
			SSHIFT   : out STD_LOGIC_VECTOR(31 downto 0)
		);
	end component C_SHIFT_RIGHT_32;

	component C_SHIFT_LEFT_32 is
		port(
			INPUT    : in  STD_LOGIC_VECTOR(31 downto 0);
			BITCOUNT : in  STD_LOGIC_VECTOR(4 downto 0);

			UROTATE  : out STD_LOGIC_VECTOR(31 downto 0);
			USHIFT   : out STD_LOGIC_VECTOR(31 downto 0);
			SSHIFT   : out STD_LOGIC_VECTOR(31 downto 0)
		);
	end component C_SHIFT_LEFT_32;

	component C_STAGE_FETCH is
		port(
			-- Puertos laterales (side ports)
			SP_PROG_MEM_ADDR  : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones de acceso a la RAM
			SP_PROG_MEM_INSTR : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos de acceso a la RAM, para leer la instrucci贸n siguiente. 

			-- Puertos anteriores (backward facing ports)
			BP_CLOCK_BEATS    : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci贸n en esta etapa.
			BP_PC_VALUE       : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci贸n en esta etapa. 

			-- Puertos frontales  (forward facing ports)
			FP_CLOCK_BEATS    : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado a la etapa siguiente, corresponde a BP_CLOCK_BEATS
			FP_PC_VALUE       : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado a la etapa siguiente, corresponde a BP_PC_VALUE

			FP_INSTR          : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0) -- Instrucci贸n a ejecutar. 
		);
	-- body
	end component C_STAGE_FETCH;

	component C_STAGE_DECODE is
		port(
			-- Puertos laterales (side ports)
			SP_REGISTER_RS_SELECTOR   : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
			SP_REGISTER_RT_SELECTOR   : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT
			SP_REGISTER_RS_VALUE      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor le铆do del registro RS
			SP_REGISTER_RT_VALUE      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor le铆do del registro RT

			-- Puertos anteriores (backward facing ports)
			BP_CLOCK_BEATS            : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci贸n en esta etapa.
			BP_PC_VALUE               : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci贸n en esta etapa.
			BP_INSTR                  : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucci贸n a la instrucci贸n en esta etapa.

			-- Puertos frontales  (forward facing ports)
			FP_CLOCK_BEATS            : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
			FP_PC_VALUE               : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.  
			FP_INSTR                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

			FP_OP_IS_ALU_RR           : out STD_LOGIC; -- La clase de instrucci贸n es ALU registro-registro
			FP_OP_IS_ALU_RI           : out STD_LOGIC; -- La clase de instrucci贸n es ALU registro-inmediato
			FP_OP_IS_LOAD_STORE       : out STD_LOGIC; -- La clase de instrucci贸n es LOAD o es STORE
			FP_OP_IS_LOAD             : out STD_LOGIC; -- La clase de instrucci贸n es LOAD.
			FP_OP_IS_STORE            : out STD_LOGIC; -- La clase de instrucci贸n es STORE.
			FP_OP_IS_CONDITIONAL_JUMP : out STD_LOGIC; -- La clase de instrucci贸n es JUMP.

			FP_ALU_OPERATION_CODE     : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c贸digo de operaci贸n ALU (si corresponde).
			FP_JUMP_CONDITION_CODE    : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c贸digo de condici贸n de salto condicional (si corresponde).

			FP_OP_DESTINATION_REG_ID  : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
			FP_OP_OPERAND_1_REG_ID    : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
			FP_OP_OPERAND_2_REG_ID    : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

			FP_OP_OPERAND_1_VALUE     : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
			FP_OP_OPERAND_2_VALUE     : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros. 
			FP_OP_IMM_VALUE           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0) -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucci贸n). 
		);
	end component C_STAGE_DECODE;

	component C_STAGE_EXECUTE is
		port(
			-- Puertos laterales (side ports)

			-- ** Esta etapa no tiene **

			-- Puertos anteriores (backward facing ports)
			BP_CLOCK_BEATS                : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci贸n en esta etapa.
			BP_PC_VALUE                   : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci贸n en esta etapa.
			BP_INSTR                      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucci贸n a la instrucci贸n en esta etapa.

			BP_OP_IS_ALU_RR               : in  STD_LOGIC; -- La clase de instrucci贸n es ALU registro-registro
			BP_OP_IS_ALU_RI               : in  STD_LOGIC; -- La clase de instrucci贸n es ALU registro-inmediato
			BP_OP_IS_LOAD_STORE           : in  STD_LOGIC; -- La clase de instrucci贸n es LOAD o es STORE
			BP_OP_IS_LOAD                 : in  STD_LOGIC; -- La clase de instrucci贸n es LOAD.
			BP_OP_IS_STORE                : in  STD_LOGIC; -- La clase de instrucci贸n es STORE.
			BP_OP_IS_CONDITIONAL_JUMP     : in  STD_LOGIC; -- La clase de instrucci贸n es JUMP.

			BP_ALU_OPERATION_CODE         : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c贸digo de operaci贸n ALU (si corresponde).
			BP_JUMP_CONDITION_CODE        : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0); -- Valor del c贸digo de condici贸n de salto condicional (si corresponde).

			BP_OP_DESTINATION_REG_ID      : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
			BP_OP_OPERAND_1_REG_ID        : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
			BP_OP_OPERAND_2_REG_ID        : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

			BP_OP_OPERAND_1_VALUE         : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.
			BP_OP_OPERAND_2_VALUE         : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RT en el banco de registros.
			BP_OP_IMM_VALUE               : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del operando inmediato o desplazamiento de LOAD/STORE (depende de la clase de instrucci贸n).

			-- Puertos frontales  (forward facing ports)
			FP_CLOCK_BEATS                : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
			FP_PC_VALUE                   : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
			FP_INSTR                      : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

			FP_OP_IS_ALU_RR               : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
			FP_OP_IS_ALU_RI               : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
			FP_OP_IS_LOAD_STORE           : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
			FP_OP_IS_LOAD                 : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
			FP_OP_IS_STORE                : out STD_LOGIC; -- Valor acarreado de la etapa anterior.
			FP_OP_IS_CONDITIONAL_JUMP     : out STD_LOGIC; -- Valor acarreado de la etapa anterior.

			FP_OP_DESTINATION_REG_ID      : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
			FP_OP_OPERAND_1_REG_ID        : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.
			FP_OP_OPERAND_2_REG_ID        : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

			FP_OP_OPERAND_1_VALUE         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor acarreado de la etapa anterior.

			FP_ALU_RESULT                 : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Resultado de la operaci贸n realizada en la ALU.

			FP_BRANCH_TAKEN               : out STD_LOGIC; -- Resultado de evaluar la condici贸n de salto (si vale 1, se debe tomar la bifurcaci贸n).
			FP_OP_BRANCH_DESTINATION_ADDR : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0)
		);
	end component C_STAGE_EXECUTE;

	component C_STAGE_WRITEBACK is
		port(
			-- Puertos laterales (side ports)
			SP_RAM_READ_ADDR_BUS       : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones LOADs desde RAM.
			SP_RAM_READ_DATA_BUS       : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para LOADs desde RAM.

			SP_RAM_WRITE_ADDR_BUS      : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones para STOREs a la RAM.
			SP_RAM_WRITE_DATA_BUS      : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para STOREs a la RAM. 
			SP_RAM_WRITE_ENABLE        : out STD_LOGIC; -- Se帽al de habilitaci贸n de escritura para STOREs a la RAM.

			SP_REGISTER_WRITE_SELECTOR : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Selector del registro para escrituras al banco de registros. 
			SP_REGISTER_WRITE_DATA     : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus da datos para escrituras al banco de registros.
			SP_REGISTER_WRITE_ENABLE   : out STD_LOGIC; -- Habilitaci贸n de escritura al banco de registros.

			-- Puertos anteriores (backward facing ports)
			BP_CLOCK_BEATS             : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Ciclos de reloj correspondientes al a instrucci贸n en esta etapa.
			BP_PC_VALUE                : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor del PC correspondiente a la instrucci贸n en esta etapa.
			BP_INSTR                   : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Palabra de instrucci贸n a la instrucci贸n en esta etapa.

			BP_OP_IS_ALU_RR            : in  STD_LOGIC; -- La clase de instrucci贸n es ALU registro-registro
			BP_OP_IS_ALU_RI            : in  STD_LOGIC; -- La clase de instrucci贸n es ALU registro-inmediato
			BP_OP_IS_LOAD_STORE        : in  STD_LOGIC; -- La clase de instrucci贸n es LOAD o es STORE
			BP_OP_IS_LOAD              : in  STD_LOGIC; -- La clase de instrucci贸n es LOAD.
			BP_OP_IS_STORE             : in  STD_LOGIC; -- La clase de instrucci贸n es STORE.
			BP_OP_IS_CONDITIONAL_JUMP  : in  STD_LOGIC; -- La clase de instrucci贸n es JUMP.

			BP_OP_DESTINATION_REG_ID   : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RD
			BP_OP_OPERAND_1_REG_ID     : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RS
			BP_OP_OPERAND_2_REG_ID     : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0); -- Identificador del registro RT

			BP_OP_OPERAND_1_VALUE      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Valor del registro RS en el banco de registros.

			BP_ALU_RESULT              : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Resultado de la operaci贸n realizada en la ALU.
			BP_BRANCH_TAKEN            : in  STD_LOGIC; -- Resultado de evaluar la condici贸n de salto (si vale 1, se debe tomar la bifurcaci贸n).

			-- Puertos frontales  (forward facing ports)
			FP_CLOCK_BEATS             : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			FP_PC_VALUE                : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			FP_INSTR                   : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
		);
	end component C_STAGE_WRITEBACK;

	component C_REGISTER_FEDE is
		port(
			-- Puertos laterales (side ports)
			SP_SYNC_STALL         : in  STD_LOGIC; -- Inhibici贸n de avance.
			SP_SYNC_INSERT_BUBBLE : in  STD_LOGIC; -- Clear sincr贸nico.
			SP_ASYNC_RESET        : in  STD_LOGIC; -- Reset asincr贸nico.
			SP_CLOCK              : in  STD_LOGIC; -- Se帽al de reloj, activo en flanco ascendente.

			-- Puertos hacia la etapa de FETCH
			BP_CLOCK_BEATS        : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0); -- Valor acarreado a la etapa siguiente, corresponde a BP_CLOCK_BEATS
			BP_PC_VALUE           : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0); -- Valor acarreado a la etapa siguiente, corresponde a BP_PC_VALUE
			BP_INSTR              : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Instrucci贸n a ejecutar.

			-- Puertos hacia la etapa de DECODE
			FP_CLOCK_BEATS        : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			FP_PC_VALUE           : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			FP_INSTR              : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
		);
	end component C_REGISTER_FEDE;

	component C_REGISTER_DEEX is
		port(
			-- Puertos laterales (side ports)
			SP_PREVIOUS_STAGE_OP_IS_ALU_RR           : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_ALU_RI           : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_LOAD_STORE       : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_LOAD             : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_STORE            : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP : out STD_LOGIC;

			SP_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			SP_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID    : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			SP_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID    : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);

			SP_FORWARD_OPERAND_1                     : in  STD_LOGIC; -- Adelantar RS 
			SP_FORWARD_OPERAND_2                     : in  STD_LOGIC; -- Adelantar RT

			SP_FORWARDED_VALUE                       : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

			SP_SYNC_STALL                            : in  STD_LOGIC; -- Inhibici贸n de avance.
			SP_SYNC_INSERT_BUBBLE                    : in  STD_LOGIC; -- Clear sincr贸nico.

			SP_ASYNC_RESET                           : in  STD_LOGIC; -- Reset asincr贸nico.
			SP_CLOCK                                 : in  STD_LOGIC; -- Se帽al de reloj, activo en flanco ascendente.

			-- Puertos hacia la etapa de DECODE
			BP_CLOCK_BEATS                           : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			BP_PC_VALUE                              : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			BP_INSTR                                 : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			BP_OP_IS_ALU_RR                          : in  STD_LOGIC;
			BP_OP_IS_ALU_RI                          : in  STD_LOGIC;
			BP_OP_IS_LOAD_STORE                      : in  STD_LOGIC;
			BP_OP_IS_LOAD                            : in  STD_LOGIC;
			BP_OP_IS_STORE                           : in  STD_LOGIC;
			BP_OP_IS_CONDITIONAL_JUMP                : in  STD_LOGIC;
			BP_ALU_OPERATION_CODE                    : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
			BP_JUMP_CONDITION_CODE                   : in  STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
			BP_OP_DESTINATION_REG_ID                 : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			BP_OP_OPERAND_1_REG_ID                   : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			BP_OP_OPERAND_2_REG_ID                   : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			BP_OP_OPERAND_1_VALUE                    : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			BP_OP_OPERAND_2_VALUE                    : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			BP_OP_IMM_VALUE                          : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

			-- Puertos hacia la etapa de EXECUTE
			FP_CLOCK_BEATS                           : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			FP_PC_VALUE                              : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			FP_INSTR                                 : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			FP_OP_IS_ALU_RR                          : out STD_LOGIC;
			FP_OP_IS_ALU_RI                          : out STD_LOGIC;
			FP_OP_IS_LOAD_STORE                      : out STD_LOGIC;
			FP_OP_IS_LOAD                            : out STD_LOGIC;
			FP_OP_IS_STORE                           : out STD_LOGIC;
			FP_OP_IS_CONDITIONAL_JUMP                : out STD_LOGIC;
			FP_ALU_OPERATION_CODE                    : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
			FP_JUMP_CONDITION_CODE                   : out STD_LOGIC_VECTOR((CODE_FIELD_SIZE - 1) downto 0);
			FP_OP_DESTINATION_REG_ID                 : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			FP_OP_OPERAND_1_REG_ID                   : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			FP_OP_OPERAND_2_REG_ID                   : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			FP_OP_OPERAND_1_VALUE                    : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			FP_OP_OPERAND_2_VALUE                    : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			FP_OP_IMM_VALUE                          : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
		);
	end component C_REGISTER_DEEX;

	component C_REGISTER_EXWR is
		port(
			-- Puertos laterales (side ports)
			SP_PREVIOUS_STAGE_OP_IS_ALU_RR               : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_ALU_RI               : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_LOAD_STORE           : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_LOAD                 : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_STORE                : out STD_LOGIC;
			SP_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP     : out STD_LOGIC;

			SP_PREVIOUS_STAGE_OP_DESTINATION_REG_ID      : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			SP_PREVIOUS_STAGE_OP_BRANCH_TAKEN            : out STD_LOGIC;

			SP_PREVIOUS_STAGE_OP_BRANCH_DESTINATION_ADDR : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

			SP_PREVIOUS_STAGE_OP_ALU_RESULT              : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

			SP_SYNC_STALL                                : in  STD_LOGIC; -- Inhibici贸n de avance.
			SP_SYNC_INSERT_BUBBLE                        : in  STD_LOGIC; -- Clear sincr贸nico.

			SP_ASYNC_RESET                               : in  STD_LOGIC; -- Reset asincr贸nico.
			SP_CLOCK                                     : in  STD_LOGIC; -- Se帽al de reloj, activo en flanco ascendente.

			-- Puertos hacia la etapa de EXECUTE
			BP_CLOCK_BEATS                               : in  STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			BP_PC_VALUE                                  : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			BP_INSTR                                     : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			BP_OP_IS_ALU_RR                              : in  STD_LOGIC;
			BP_OP_IS_ALU_RI                              : in  STD_LOGIC;
			BP_OP_IS_LOAD_STORE                          : in  STD_LOGIC;
			BP_OP_IS_LOAD                                : in  STD_LOGIC;
			BP_OP_IS_STORE                               : in  STD_LOGIC;
			BP_OP_IS_CONDITIONAL_JUMP                    : in  STD_LOGIC;
			BP_OP_DESTINATION_REG_ID                     : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			BP_OP_OPERAND_1_REG_ID                       : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			BP_OP_OPERAND_2_REG_ID                       : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			BP_OP_OPERAND_1_VALUE                        : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			BP_ALU_RESULT                                : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			BP_OP_BRANCH_TAKEN                           : in  STD_LOGIC;
			BP_OP_BRANCH_DESTINATION_ADDR                : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

			-- Puertos hacia la etapa de WRITEBACK
			FP_CLOCK_BEATS                               : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			FP_PC_VALUE                                  : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			FP_INSTR                                     : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			FP_OP_IS_ALU_RR                              : out STD_LOGIC;
			FP_OP_IS_ALU_RI                              : out STD_LOGIC;
			FP_OP_IS_LOAD_STORE                          : out STD_LOGIC;
			FP_OP_IS_LOAD                                : out STD_LOGIC;
			FP_OP_IS_STORE                               : out STD_LOGIC;
			FP_OP_IS_CONDITIONAL_JUMP                    : out STD_LOGIC;
			FP_OP_DESTINATION_REG_ID                     : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			FP_OP_OPERAND_1_REG_ID                       : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			FP_OP_OPERAND_2_REG_ID                       : out STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			FP_OP_OPERAND_1_VALUE                        : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			FP_ALU_RESULT                                : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			FP_JUMP_EVALUATION                           : out STD_LOGIC
		);
	end component C_REGISTER_EXWR;

	component C_PIPELINE_CONTROL is
		port(

			-- -----------------------------------
			-- Puertos hacia el PC COUNTER
			PCNT_STALL                                 : out STD_LOGIC; -- Inhibo el avance de la etapa.
			PCNT_SET_VALUE                             : out STD_LOGIC; -- Inserto un a burbuja en esta etapa.
			PCNT_NEW_VALUE                             : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

			-- -----------------------------------
			-- Puertos hacia el registro FEDE
			FEDE_STALL                                 : out STD_LOGIC; -- Inhibo el avance de la etapa.
			FEDE_INSERT_BUBBLE                         : out STD_LOGIC; -- Inserto un a burbuja en esta etapa.

			-- -----------------------------------
			-- Puertos hacia el registro DEEX
			DEEX_PREVIOUS_STAGE_OP_IS_ALU_RR           : in  STD_LOGIC;
			DEEX_PREVIOUS_STAGE_OP_IS_ALU_RI           : in  STD_LOGIC;
			DEEX_PREVIOUS_STAGE_OP_IS_LOAD_STORE       : in  STD_LOGIC;
			DEEX_PREVIOUS_STAGE_OP_IS_LOAD             : in  STD_LOGIC;
			DEEX_PREVIOUS_STAGE_OP_IS_STORE            : in  STD_LOGIC;
			DEEX_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP : in  STD_LOGIC;

			DEEX_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			DEEX_PREVIOUS_STAGE_OP_OPERAND_1_REG_ID    : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			DEEX_PREVIOUS_STAGE_OP_OPERAND_2_REG_ID    : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);

			DEEX_FORWARD_OPERAND_1                     : out STD_LOGIC; -- Adelantar RS 
			DEEX_FORWARD_OPERAND_2                     : out STD_LOGIC; -- Adelantar RT

			DEEX_FORWARDED_VALUE                       : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

			DEEX_STALL                                 : out STD_LOGIC; -- Inhibo el avance de la etapa.
			DEEX_INSERT_BUBBLE                         : out STD_LOGIC; -- Inserto un a burbuja en esta etapa.

			-- -----------------------------------
			-- Puertos hacia el registro EXWR
			EXWR_PREVIOUS_STAGE_OP_IS_ALU_RR           : in  STD_LOGIC;
			EXWR_PREVIOUS_STAGE_OP_IS_ALU_RI           : in  STD_LOGIC;
			EXWR_PREVIOUS_STAGE_OP_IS_LOAD_STORE       : in  STD_LOGIC;
			EXWR_PREVIOUS_STAGE_OP_IS_LOAD             : in  STD_LOGIC;
			EXWR_PREVIOUS_STAGE_OP_IS_STORE            : in  STD_LOGIC;
			EXWR_PREVIOUS_STAGE_OP_IS_CONDITIONAL_JUMP : in  STD_LOGIC;

			EXWR_PREVIOUS_STAGE_OP_DESTINATION_REG_ID  : in  STD_LOGIC_VECTOR((REG_SEL_SIZE - 1) downto 0);
			EXWR_PREVIOUS_STAGE_OP_BRANCH_TAKEN        : in  STD_LOGIC;

			EXWR_PREVIOUS_STAGE_OP_ALU_RESULT          : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

			EXWR_BRANCH_DESTINATION_ADDR               : in  STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);

			EXWR_STALL                                 : out STD_LOGIC; -- Inhibo el avance de la etapa.
			EXWR_INSERT_BUBBLE                         : out STD_LOGIC -- Inserto un a burbuja en esta etapa.

		);
	end component C_PIPELINE_CONTROL;

	component C_CPU_SIMPLE is
		port(
			-- Se馻les de control
			CLOCK                   : in  STD_LOGIC; -- Activo en flanco ascendente.
			ASYNC_RESET             : in  STD_LOGIC; -- Activo en nivel alto.

			-- Acceso a la memoria de programa
			PROG_MEM_ADDR_BUS       : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones de acceso a la RAM
			PROG_MEM_INSTR_BUS      : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos de acceso a la RAM, para leer la instrucci髇 siguiente. 

			-- Acceso de lectura a la memoria de datos
			DATA_MEM_READ_ADDR_BUS  : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones LOADs desde RAM.
			DATA_MEM_READ_DATA_BUS  : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para LOADs desde RAM.

			-- Acceso de escritura a la memoria de datos
			DATA_MEM_WRITE_ADDR_BUS : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones para STOREs a la RAM.
			DATA_MEM_WRITE_DATA_BUS : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para STOREs a la RAM. 
			DATA_MEM_WRITE_ENABLE   : out STD_LOGIC; -- Se馻l de habilitaci髇 de escritura para STOREs a la RAM.

			-- Se馻les para monitoreo de la ejecuci髇
			MONITOR_CLOCK_BEATS     : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			MONITOR_PC_VALUE        : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			MONITOR_INSTR           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

			-- Puertos de monitoreo de estado de los primeros 8 registros
			MONITOR_RB_R0           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R1           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R2           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R3           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R4           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R5           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R6           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R7           : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
		);
	end component C_CPU_SIMPLE;

	component C_CPU_SEGMENTED is
		port(
			-- Se馻les de control
			CLOCK                          : in  STD_LOGIC; -- Activo en flanco ascendente.
			ASYNC_RESET                    : in  STD_LOGIC; -- Activo en nivel alto.

			-- Acceso a la memoria de programa
			PROG_MEM_ADDR_BUS              : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones de acceso a la RAM
			PROG_MEM_INSTR_BUS             : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos de acceso a la RAM, para leer la instrucci髇 siguiente. 

			-- Acceso de lectura a la memoria de datos
			DATA_MEM_READ_ADDR_BUS         : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones LOADs desde RAM.
			DATA_MEM_READ_DATA_BUS         : in  STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para LOADs desde RAM.

			-- Acceso de escritura a la memoria de datos
			DATA_MEM_WRITE_ADDR_BUS        : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT); -- Bus de direcciones para STOREs a la RAM.
			DATA_MEM_WRITE_DATA_BUS        : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0); -- Bus de datos para STOREs a la RAM. 
			DATA_MEM_WRITE_ENABLE          : out STD_LOGIC; -- Se馻l de habilitaci髇 de escritura para STOREs a la RAM.

			-- Se馻les de depuraci髇
			MONITOR_STFE_CLOCK_BEATS       : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			MONITOR_STFE_PC_VALUE          : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			MONITOR_STFE_INSTR             : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_STFE_STALLED           : out STD_LOGIC;

			MONITOR_STDE_CLOCK_BEATS       : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			MONITOR_STDE_PC_VALUE          : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			MONITOR_STDE_INSTR             : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_STDE_STALLED           : out STD_LOGIC;
			MONITOR_STDE_FORWARD_OPERAND_1 : out STD_LOGIC;
			MONITOR_STDE_FORWARD_OPERAND_2 : out STD_LOGIC;

			MONITOR_STEX_CLOCK_BEATS       : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			MONITOR_STEX_PC_VALUE          : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			MONITOR_STEX_INSTR             : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_STEX_STALLED           : out STD_LOGIC;
			MONITOR_STEX_BRANCH_TAKEN      : out STD_LOGIC;

			MONITOR_STWR_CLOCK_BEATS       : out STD_LOGIC_VECTOR((BEATS_COUNTER_SIZE - 1) downto 0);
			MONITOR_STWR_PC_VALUE          : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
			MONITOR_STWR_INSTR             : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_STWR_STALLED           : out STD_LOGIC;

			-- Puertos de monitoreo de estado de los primeros 8 registros
			MONITOR_RB_R0                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R1                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R2                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R3                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R4                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R5                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R6                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
			MONITOR_RB_R7                  : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
		);
	end component C_CPU_SEGMENTED;

	component C_DATA_MEMORY is
		generic(
			WORD_WIDTH : integer := DATA_BUS_SIZE;
			ADDR_WIDTH : integer := ADDR_BUS_SIZE;
			DELAY      : time    := TIME_DELAY_MEMORY
		);
		port(
			-- Entradas de control
			CLOCK          : in  STD_LOGIC; -- Activo en flanco ascendente
			RESET          : in  STD_LOGIC; -- Activo en nivel alto.
			-- Puerto de lectura
			READ_ADDR_BUS  : in  STD_LOGIC_VECTOR((ADDR_WIDTH - 1) downto LOWEST_ADDR_BIT);
			READ_DATA_BUS  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			-- Puerto de escritura
			WRITE_ENABLE   : in  STD_LOGIC;
			WRITE_ADDR_BUS : in  STD_LOGIC_VECTOR((ADDR_WIDTH - 1) downto LOWEST_ADDR_BIT);
			WRITE_DATA_BUS : in  STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);

			-- Puertos de monitoreo de estado de las primeras 8 posiciones de memoria
			MONITOR_DM_M0  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_DM_M1  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_DM_M2  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_DM_M3  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_DM_M4  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_DM_M5  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_DM_M6  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
			MONITOR_DM_M7  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0)
		);
	end component C_DATA_MEMORY;

	component C_PROGRAM_MEMORY is
		generic(
			WORD_WIDTH : integer := DATA_BUS_SIZE;
			ADDR_WIDTH : integer := ADDR_BUS_SIZE;
			DELAY      : time    := TIME_DELAY_MEMORY
		);
		port(
			ADDR_BUS : in  STD_LOGIC_VECTOR((ADDR_WIDTH - 1) downto LOWEST_ADDR_BIT);
			DATA_BUS : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0)
		);
	end component C_PROGRAM_MEMORY;

end package NANO_PET_COMP_PKG; 
