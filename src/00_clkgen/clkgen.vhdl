--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_CLKGEN is
	generic(
		PERIOD : time := 1 us           -- Per�odo de la se�al de reloj a generar. 
	);
	port(
		CLKOUT : out STD_LOGIC          -- Se�al de reloj generada.
	);
end entity E_CLKGEN;

-- --------------------------------

architecture A_CLKGEN of E_CLKGEN is
begin
	-- Proceso para generar un reloj de onda cuadrada. 
	process
	begin
		CLKOUT <= '0';
		wait for (PERIOD / 2);

		CLKOUT <= '1';
		wait for (PERIOD / 2);
	end process;

end architecture A_CLKGEN;
