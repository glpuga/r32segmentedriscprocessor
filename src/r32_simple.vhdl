--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

entity E_R32_SIMPLE is
	port(
		MONITOR_RESET         : out STD_LOGIC;
		MONITOR_CLOCK         : out STD_LOGIC;
		--

		MONITOR_STWR_PC_VALUE : out STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto 0);
		MONITOR_STWR_INSTR    : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

		-- Puertos de monitoreo de estado de los primeros 8 registros
		MONITOR_RB_R0         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R1         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R2         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R3         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R4         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R5         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R6         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_RB_R7         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

		-- Puertos de monitoreo de estado de las primeras 8 posiciones de memoria
		MONITOR_DM_M0         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_DM_M1         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_DM_M2         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_DM_M3         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_DM_M4         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_DM_M5         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_DM_M6         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
		MONITOR_DM_M7         : out STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0)
	);
end E_R32_SIMPLE;

architecture A_R32_SIMPLE of E_R32_SIMPLE is
	for all : C_CPU_SIMPLE use entity E_CPU_SIMPLE;
	for all : C_DATA_MEMORY use entity E_DATA_MEMORY;
	for all : C_CLKGEN use entity E_CLKGEN;

	-- --
	--
	-- SELECCI�N DEL PROGRAMA A CORRER
	--
	-- El programa se elige seleccionando una arquitectura que implemente el contenido de 
	-- la memoria de programa deseado. 
	--

	for all : C_PROGRAM_MEMORY use entity E_PROGRAM_MEMORY(A_PROGRAM_FIBONACCI);
	-- for all : C_PROGRAM_MEMORY use entity E_PROGRAM_MEMORY(A_PROGRAM_HAILSTONE);
	-- for all : C_PROGRAM_MEMORY use entity E_PROGRAM_MEMORY(A_PROGRAM_TEST_PIPELINE);

	--
	-- --

	signal CLOCK : STD_LOGIC;
	signal RESET : STD_LOGIC;

	signal PROGRAM_ADDR_BUS : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT);
	signal PROGRAM_DATA_BUS : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	signal DATA_READ_ADDR_BUS : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT);
	signal DATA_READ_DATA_BUS : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);

	signal DATA_WRITE_ADDR_BUS : STD_LOGIC_VECTOR((ADDR_BUS_SIZE - 1) downto LOWEST_ADDR_BIT);
	signal DATA_WRITE_DATA_BUS : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	signal DATA_WRITE_ENABLE   : STD_LOGIC;

begin

	-- Inicio el sistema en estado de reset y luego lo libero.
	RESET <= '1', '0' after 2 us;

	-- Conecto las se�ales externas que sirven para monitorear el clock y el reset.
	MONITOR_RESET <= RESET;
	MONITOR_CLOCK <= CLOCK;

	cClockGenerator : C_CLKGEN
		generic map(PERIOD => 1 us)
		port map(CLKOUT => CLOCK);

	cProgramMemory : C_PROGRAM_MEMORY
		port map(ADDR_BUS => PROGRAM_ADDR_BUS,
			     DATA_BUS => PROGRAM_DATA_BUS);

	cDataMemory : C_DATA_MEMORY
		port map(CLOCK          => CLOCK,
			     RESET          => RESET,
			     --
			     READ_ADDR_BUS  => DATA_READ_ADDR_BUS,
			     READ_DATA_BUS  => DATA_READ_DATA_BUS,
			     --
			     WRITE_ENABLE   => DATA_WRITE_ENABLE,
			     WRITE_ADDR_BUS => DATA_WRITE_ADDR_BUS,
			     WRITE_DATA_BUS => DATA_WRITE_DATA_BUS,
			     --
			     MONITOR_DM_M0  => MONITOR_DM_M0,
			     MONITOR_DM_M1  => MONITOR_DM_M1,
			     MONITOR_DM_M2  => MONITOR_DM_M2,
			     MONITOR_DM_M3  => MONITOR_DM_M3,
			     MONITOR_DM_M4  => MONITOR_DM_M4,
			     MONITOR_DM_M5  => MONITOR_DM_M5,
			     MONITOR_DM_M6  => MONITOR_DM_M6,
			     MONITOR_DM_M7  => MONITOR_DM_M7);

	cCpu : C_CPU_SIMPLE
		port map(CLOCK                   => CLOCK,
			     ASYNC_RESET             => RESET,
			     --
			     PROG_MEM_ADDR_BUS       => PROGRAM_ADDR_BUS,
			     PROG_MEM_INSTR_BUS      => PROGRAM_DATA_BUS,
			     --
			     DATA_MEM_READ_ADDR_BUS  => DATA_READ_ADDR_BUS,
			     DATA_MEM_READ_DATA_BUS  => DATA_READ_DATA_BUS,
			     --
			     DATA_MEM_WRITE_ADDR_BUS => DATA_WRITE_ADDR_BUS,
			     DATA_MEM_WRITE_DATA_BUS => DATA_WRITE_DATA_BUS,
			     DATA_MEM_WRITE_ENABLE   => DATA_WRITE_ENABLE,
			     --
			     MONITOR_CLOCK_BEATS     => open, -- Esta se�al no aporta mucha informaci�n �til
			     MONITOR_PC_VALUE        => MONITOR_STWR_PC_VALUE,
			     MONITOR_INSTR           => MONITOR_STWR_INSTR,
			     --
			     MONITOR_RB_R0           => MONITOR_RB_R0,
			     MONITOR_RB_R1           => MONITOR_RB_R1,
			     MONITOR_RB_R2           => MONITOR_RB_R2,
			     MONITOR_RB_R3           => MONITOR_RB_R3,
			     MONITOR_RB_R4           => MONITOR_RB_R4,
			     MONITOR_RB_R5           => MONITOR_RB_R5,
			     MONITOR_RB_R6           => MONITOR_RB_R6,
			     MONITOR_RB_R7           => MONITOR_RB_R7);
end A_R32_SIMPLE;			