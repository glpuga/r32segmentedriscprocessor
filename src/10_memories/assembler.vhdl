--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

package ASSEMBLER_PKG is
	type INSTRUCTION is array ((DATA_BUS_SIZE - 1) downto 0) of STD_LOGIC;

	constant R0  : integer := 0;
	constant R1  : integer := 1;
	constant R2  : integer := 2;
	constant R3  : integer := 3;
	constant R4  : integer := 4;
	constant R5  : integer := 5;
	constant R6  : integer := 6;
	constant R7  : integer := 7;
	constant R8  : integer := 8;
	constant R9  : integer := 9;
	constant R10 : integer := 10;
	constant R11 : integer := 11;
	constant R12 : integer := 12;
	constant R13 : integer := 13;
	constant R14 : integer := 14;
	constant R15 : integer := 15;
	constant R16 : integer := 16;
	constant R17 : integer := 17;
	constant R18 : integer := 18;
	constant R19 : integer := 19;
	constant R20 : integer := 20;
	constant R21 : integer := 21;
	constant R22 : integer := 22;
	constant R23 : integer := 23;
	constant R24 : integer := 24;
	constant R25 : integer := 25;
	constant R26 : integer := 26;
	constant R27 : integer := 27;
	constant R28 : integer := 28;
	constant R29 : integer := 29;
	constant R30 : integer := 30;
	constant R31 : integer := 31;

	function assembleALURR(aluCode : integer; rd, rs, rt : integer) return INSTRUCTION;
	function assembleALURI(aluCode : integer; rd, rs : integer; imm : integer) return INSTRUCTION;
	function assembleLOAD(rd, rs : integer; displacement : integer) return INSTRUCTION;
	function assembleSTORE(rd, rs : integer; displacement : integer) return INSTRUCTION;
	function assembleJUMP(conditionCode, rs : integer; destinationAddr : integer) return INSTRUCTION;
	function assembleNOP return INSTRUCTION;

end package ASSEMBLER_PKG;

package body ASSEMBLER_PKG is
	function assembleALURR(aluCode : integer; rd, rs, rt : integer) return INSTRUCTION is
		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
	begin
		vInstruction :=                 -- Formato de Instrucci�nes ALU RR:
			"00"                        -- Class
			& to_unsigned(aluCode, 4)   -- C�digo de operaci�n
			& to_unsigned(rs, 5)        -- Registro operando 1
			& to_unsigned(rd, 5)        -- Registro destino
			& "0000" & "0000" & "000"   -- Relleno
			& to_unsigned(rt, 5);       -- Registro operando 2
		return INSTRUCTION(vInstruction);
	end function;

	function assembleALURI(aluCode : integer; rd, rs : integer; imm : integer) return INSTRUCTION is
		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
	begin
		vInstruction :=                 -- Formato de Instrucci�nes ALU RI:
			"01"                        -- Clase ALU RI
			& to_unsigned(aluCode, 4)   -- C�digo de operaci�n
			& to_unsigned(rs, 5)        -- Registro operando 1
			& to_unsigned(rd, 5)        -- Registro destino
			& to_unsigned(imm, 16);     -- Operando Inmediato

		return INSTRUCTION(vInstruction);
	end function;

	function assembleLOAD(rd, rs : integer; displacement : integer) return INSTRUCTION is
		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
	begin
		vInstruction :=                      -- Formato de Instrucci�nes LOAD/STORE:
			"10"                             -- Class LOAD/STORE
			& "0000"                         -- C�digo de LOAD
			& to_unsigned(rs, 5)             -- Registro origen
			& to_unsigned(rd, 5)             -- Registro destino
			& to_unsigned(displacement, 16); -- Desplazamiento relativo

		return INSTRUCTION(vInstruction);
	end function;

	function assembleSTORE(rd, rs : integer; displacement : integer) return INSTRUCTION is
		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
	begin
		vInstruction :=                      -- Formato de Instrucci�nes LOAD/STORE:
			"10"                             -- Class LOAD/STORE
			& "0001"                         -- C�digo de STORE
			& to_unsigned(rs, 5)             -- Registro origen
			& to_unsigned(rd, 5)             -- Registro destino
			& to_unsigned(displacement, 16); -- Desplazamiento relativo

		return INSTRUCTION(vInstruction);
	end function;

	function assembleJUMP(conditionCode, rs : integer; destinationAddr : integer) return INSTRUCTION is
		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
	begin
		vInstruction :=                         -- Formato de Instrucci�nes de Salto condicional:
			"11"                                -- Class JUMP
			& to_unsigned(conditionCode, 4)     -- C�digo de condici�n de salto
			& to_unsigned(rs, 5)                -- Registro origen
			& to_unsigned(destinationAddr, 21); -- Direcci�n de salto

		return INSTRUCTION(vInstruction);
	end function;

	function assembleNOP return INSTRUCTION is
		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
	begin
		-- Genero una instrucci�n NOP con una suma que guarda el resultado en R0
		vInstruction :=                 -- Formato de Instrucci�nes ALU RR:
			"00"                        -- Class
			& to_unsigned(0, 4)         -- C�digo de operaci�n
			& to_unsigned(R0, 5)        -- Registro operando 1
			& to_unsigned(R0, 5)        -- Registro destino
			& "0000" & "0000" & "000"   -- Relleno
			& to_unsigned(R0, 5);       -- Registro operando 2

		return INSTRUCTION(vInstruction);
	end function;

end package body ASSEMBLER_PKG;

--
-- -----------------------------------------------------------------
--
-- VERSI�N PARA LOS ALUMNOS, CON LAS FUNCIONES LISTAS PARA COMPLETAR
--
-- -----------------------------------------------------------------
--

--package body ASSEMBLER_PKG is
--	function assembleALURR(aluCode : integer; rd, rs, rt : integer) return INSTRUCTION is
--		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
--	begin
--		vInstruction :=                 -- Formato de Instrucci�nes ALU RR:
--			"00"                        -- Class
--			& to_unsigned(aluCode, 4)   -- C�digo de operaci�n
--			& to_unsigned(rs, 5)        -- Registro operando 1
--			& to_unsigned(rd, 5)        -- Registro destino
--			& "0000" & "0000" & "000"   -- Relleno
--			& to_unsigned(rt, 5);       -- Registro operando 2
--		return INSTRUCTION(vInstruction);
--	end function;
--
--	function assembleALURI(aluCode : integer; rd, rs : integer; imm : integer) return INSTRUCTION is
--		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
--	begin
--		-- --
--		-- COMPLETAR EL C�DIGO SIGUIENTE PARA QUE GENERE CORRECTAMENTE LAS INSTRUCCIONES ALURI
--		vInstruction := to_unsigned(0, DATA_BUS_SIZE);
--		-- --
--
--		return INSTRUCTION(vInstruction);
--	end function;
--
--	function assembleLOAD(rd, rs : integer; displacement : integer) return INSTRUCTION is
--		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
--	begin
--		-- --
--		-- COMPLETAR EL C�DIGO SIGUIENTE PARA QUE GENERE CORRECTAMENTE LAS INSTRUCCIONES LOAD
--		vInstruction := to_unsigned(0, DATA_BUS_SIZE);
--		-- --
--
--		return INSTRUCTION(vInstruction);
--	end function;
--
--	function assembleSTORE(rd, rs : integer; displacement : integer) return INSTRUCTION is
--		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
--	begin
--		-- --
--		-- COMPLETAR EL C�DIGO SIGUIENTE PARA QUE GENERE CORRECTAMENTE LAS INSTRUCCIONES STORE
--		vInstruction := to_unsigned(0, DATA_BUS_SIZE);
--		-- --
--
--		return INSTRUCTION(vInstruction);
--	end function;
--
--	function assembleJUMP(conditionCode, rs : integer; destinationAddr : integer) return INSTRUCTION is
--		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
--	begin
--		-- --
--		-- COMPLETAR EL C�DIGO SIGUIENTE PARA QUE GENERE CORRECTAMENTE LAS INSTRUCCIONES JUMP
--		vInstruction := to_unsigned(0, DATA_BUS_SIZE);
--		-- --
--
--		return INSTRUCTION(vInstruction);
--	end function;
--
--	function assembleNOP return INSTRUCTION is
--		variable vInstruction : UNSIGNED((DATA_BUS_SIZE - 1) downto 0);
--	begin
--		-- --
--		-- COMPLETAR EL C�DIGO SIGUIENTE PARA QUE GENERE CORRECTAMENTE LAS INSTRUCCIONES NOP
--		vInstruction := to_unsigned(0, DATA_BUS_SIZE);
--		-- --
--
--		return INSTRUCTION(vInstruction);
--	end function;
--
--end package body ASSEMBLER_PKG;
