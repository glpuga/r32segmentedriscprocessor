--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.assembler_pkg.all;
use work.all;

-- --------------------------------

entity E_PROGRAM_MEMORY is
	generic(
		WORD_WIDTH : integer := DATA_BUS_SIZE;
		ADDR_WIDTH : integer := ADDR_BUS_SIZE;
		DELAY      : time    := TIME_DELAY_MEMORY
	);
	port(
		ADDR_BUS : in  STD_LOGIC_VECTOR((ADDR_WIDTH - 1) downto LOWEST_ADDR_BIT); -- Direcci�n (notar que faltan los dos bits de abajo, los accesos son alineados a 4 bytes, siempre).
		DATA_BUS : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0)
	);
end entity E_PROGRAM_MEMORY;

-- --------------------------------
-- Definici�n de la Arquitectura --
-- --------------------------------

architecture A_PROGRAM_TEST_PIPELINE of E_PROGRAM_MEMORY is
begin
	process(ADDR_BUS)
		variable vProgramWord : INSTRUCTION;

		variable vFullAddress : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	begin
		vFullAddress := std_logic_vector(to_unsigned(0, DATA_BUS_SIZE - ADDR_BUS_SIZE)) & ADDR_BUS & std_logic_vector(to_unsigned(0, LOWEST_ADDR_BIT));

		--	--
		--
		-- La memoria de programa es generada mediante este case, que para cada posici�n de memoria devuelve 
		-- la palabra de instrucci�n correspondiente.
		--
		-- El formato es f�cil de deducir: 
		-- "when <direcci�n_mem> => vProgramWord := <PALABRA DE INSTRUCCI�N A DEVOLVER>." 

		case vFullAddress is
			when X"00000000" => vProgramWord := assembleNOP; -- NOP
			when X"00000004" => vProgramWord := assembleNOP; -- NOP
			when X"00000008" => vProgramWord := assembleNOP; -- NOP
			when X"0000000C" => vProgramWord := assembleALURI(0, R1, R0, 1);
			when X"00000010" => vProgramWord := assembleALURI(0, R2, R0, 2);
			when X"00000014" => vProgramWord := assembleALURI(0, R3, R0, 3);
			when X"00000018" => vProgramWord := assembleNOP; -- NOP
			when X"0000001C" => vProgramWord := assembleNOP; -- NOP
			when X"00000020" => vProgramWord := assembleNOP; -- NOP
			when X"00000024" => vProgramWord := assembleALURI(0, R5, R2, R3);
			when X"00000028" => vProgramWord := assembleALURI(0, R6, R5, R1); -- F1
			when X"0000002C" => vProgramWord := assembleALURI(0, R7, R1, R6); -- F2
			when X"00000030" => vProgramWord := assembleALURI(0, R8, R7, R7); -- F1 Y F2
			when X"00000034" => vProgramWord := assembleNOP; -- NOP
			when X"00000038" => vProgramWord := assembleNOP; -- NOP
			when X"0000003C" => vProgramWord := assembleNOP; -- NOP 
			when X"00000040" => vProgramWord := assembleALURI(0, R5, R2, 3);
			when X"00000044" => vProgramWord := assembleALURI(0, R6, R5, 1); -- F1
			when X"00000048" => vProgramWord := assembleNOP; -- NOP
			when X"0000004C" => vProgramWord := assembleNOP; -- NOP
			when X"00000050" => vProgramWord := assembleNOP; -- NOP 
			when X"00000054" => vProgramWord := assembleSTORE(R0, R1, 1);
			when X"00000058" => vProgramWord := assembleSTORE(R1, R2, 1);
			when X"0000005C" => vProgramWord := assembleSTORE(R2, R1, 0);
			when X"00000060" => vProgramWord := assembleSTORE(R2, R2, 0);
			when X"00000064" => vProgramWord := assembleNOP; -- NOP
			when X"00000068" => vProgramWord := assembleNOP; -- NOP
			when X"0000006C" => vProgramWord := assembleNOP; -- NOP
			when X"00000070" => vProgramWord := assembleLOAD(R1, R2, 0);
			when X"00000074" => vProgramWord := assembleLOAD(R1, R2, 0);
			when X"00000078" => vProgramWord := assembleLOAD(R2, R1, 0); -- F1
			when X"0000007C" => vProgramWord := assembleLOAD(R2, R2, 0); -- F1 y F2
			when X"00000080" => vProgramWord := assembleLOAD(R2, R1, 0); -- F2 
			when X"00000084" => vProgramWord := assembleNOP; -- NOP
			when X"00000088" => vProgramWord := assembleNOP; -- NOP
			when X"0000008C" => vProgramWord := assembleNOP; -- NOP 			
			when X"00000090" => vProgramWord := assembleALURR(0, R1, R3, R4);
			when X"00000094" => vProgramWord := assembleSTORE(R3, R4, 0);
			when X"00000098" => vProgramWord := assembleALURR(0, R1, R3, R4);
			when X"0000009C" => vProgramWord := assembleSTORE(R3, R1, 0); -- F1
			when X"000000A0" => vProgramWord := assembleALURR(0, R1, R3, R4);
			when X"000000A4" => vProgramWord := assembleSTORE(R1, R4, 0); -- F2
			when X"000000A8" => vProgramWord := assembleALURR(0, R1, R3, R4);
			when X"000000AC" => vProgramWord := assembleSTORE(R1, R1, 0); -- F1, F2
			when X"000000B0" => vProgramWord := assembleNOP; -- NOP
			when X"000000B4" => vProgramWord := assembleNOP; -- NOP
			when X"000000B8" => vProgramWord := assembleNOP; -- NOP 			
			when X"000000BC" => vProgramWord := assembleALURR(0, R1, R3, R4);
			when X"000000C0" => vProgramWord := assembleLOAD(R3, R4, 0);
			when X"000000C4" => vProgramWord := assembleALURR(0, R1, R3, R4); -- F1
			when X"000000C8" => vProgramWord := assembleLOAD(R3, R1, 0); -- F1
			when X"000000CC" => vProgramWord := assembleALURR(0, R1, R3, R4); -- F1
			when X"000000D0" => vProgramWord := assembleLOAD(R4, R1, 0); -- F2
			when X"000000D4" => vProgramWord := assembleALURR(0, R1, R3, R4); -- F2
			when X"000000D8" => vProgramWord := assembleLOAD(R1, R1, 0); -- F1 Y F2
			when X"000000DC" => vProgramWord := assembleNOP; -- NOP
			when X"000000E0" => vProgramWord := assembleNOP; -- NOP
			when X"000000E4" => vProgramWord := assembleNOP; -- NOP 			
			when X"000000E8" => vProgramWord := assembleALURI(0, R1, R3, 4);
			when X"000000EC" => vProgramWord := assembleSTORE(R3, R4, 0);
			when X"000000F0" => vProgramWord := assembleALURI(0, R1, R3, 4);
			when X"000000F4" => vProgramWord := assembleSTORE(R3, R1, 0); -- F1
			when X"000000F8" => vProgramWord := assembleALURI(0, R1, R3, 4);
			when X"000000FC" => vProgramWord := assembleSTORE(R1, R4, 0); -- F2
			when X"00000100" => vProgramWord := assembleALURI(0, R1, R3, 4);
			when X"00000104" => vProgramWord := assembleSTORE(R1, R1, 0); -- F1 Y F2
			when X"00000108" => vProgramWord := assembleNOP; -- NOP
			when X"0000010C" => vProgramWord := assembleNOP; -- NOP
			when X"00000110" => vProgramWord := assembleNOP; -- NOP 			
			when X"00000114" => vProgramWord := assembleALURI(0, R1, R3, 4);
			when X"00000118" => vProgramWord := assembleLOAD(R3, R4, 0);
			when X"0000011C" => vProgramWord := assembleALURI(0, R1, R3, 4); -- F1
			when X"00000120" => vProgramWord := assembleLOAD(R3, R1, 0); -- F1 
			when X"00000124" => vProgramWord := assembleALURI(0, R1, R3, 4); -- F1
			when X"00000128" => vProgramWord := assembleLOAD(R1, R4, 0); -- F2
			when X"0000012C" => vProgramWord := assembleALURI(0, R1, R3, 4); -- F2
			when X"00000130" => vProgramWord := assembleLOAD(R1, R1, 0); -- F1 y F2
			when X"00000134" => vProgramWord := assembleNOP; -- NOP
			when X"00000138" => vProgramWord := assembleNOP; -- NOP
			when X"0000013C" => vProgramWord := assembleNOP; -- NOP 			
			when X"00000140" => vProgramWord := assembleALURR(0, R1, R3, R4);
			when X"00000144" => vProgramWord := assembleJUMP(0, R3, 0);
			when X"00000148" => vProgramWord := assembleALURR(0, R1, R3, R4);
			when X"0000014C" => vProgramWord := assembleJUMP(0, R1, 0); -- F1
			when X"00000150" => vProgramWord := assembleNOP; -- NOP
			when X"00000154" => vProgramWord := assembleNOP; -- NOP
			when X"00000158" => vProgramWord := assembleNOP; -- NOP 			
			when X"0000015C" => vProgramWord := assembleALURI(0, R1, R3, 4);
			when X"00000160" => vProgramWord := assembleJUMP(0, R3, 0);
			when X"00000164" => vProgramWord := assembleALURI(0, R1, R3, 4);
			when X"00000168" => vProgramWord := assembleJUMP(0, R1, 0); -- F1		
			when X"00000170" => vProgramWord := assembleJUMP(2#0111#, R0, 16#00000170#);
			when others      => vProgramWord := X"00000000";
		end case;

		DATA_BUS <= std_logic_vector(vProgramWord) after DELAY;
	end process;

end architecture;

-- --------------------------------

architecture A_PROGRAM_FIBONACCI of E_PROGRAM_MEMORY is
begin
	process(ADDR_BUS)
		variable vProgramWord : INSTRUCTION;

		variable vFullAddress : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	begin
		vFullAddress := std_logic_vector(to_unsigned(0, DATA_BUS_SIZE - ADDR_BUS_SIZE)) & ADDR_BUS & "00";

		--	--
		--
		-- La memoria de programa es generada mediante este case, que para cada posici�n de memoria devuelve 
		-- la palabra de instrucci�n correspondiente.
		-- El formato es f�cil de deducir: 
		-- "when <direcci�n_mem> => vProgramWord := <PALABRA DE INSTRUCCI�N A DEVOLVER>." 

		case vFullAddress is
			when X"00000000" => vProgramWord := assembleALURI(0, R1, R0, 1);  -- add R1,R0,1  ;  Inicializo las dos primeras dos posiciones de la tabla Fibonacci.
			when X"00000004" => vProgramWord := assembleSTORE(R0, R1, 0);     -- st  0(R0),R1
			when X"00000008" => vProgramWord := assembleSTORE(R0, R1, 4);     -- st  4(R0),R1
			when X"0000000C" => vProgramWord := assembleALURI(0, R5, R0, 10); -- add R5,R0,10 ; Inicializo el contador de bucles en 10
			when X"00000010" => vProgramWord := assembleALURI(0, R4, R0, 0);  -- add R4,R0,0  ; Inicializo el puntero al comienzo de la tabla Fibonacci
			-- mainloop:                                                                      ; Comienzo del bucle principal 
			when X"00000014" => vProgramWord := assembleLOAD(R2, R4, 0);      -- ld  R2,0(R4) ; Cargo el n�mero f[n] de la secuencia
			when X"00000018" => vProgramWord := assembleLOAD(R3, R4, 4);      -- ld  R3,0(R4) ; Cargo el n�mero f[n+1] de la secuencia 
			when X"0000001C" => vProgramWord := assembleALURR(0, R3, R2, R3); -- add R3,R2,R3 ; Calculo el n�mero f[n+2] de la secuencia 
			when X"00000020" => vProgramWord := assembleSTORE(R4, R3, 8);     -- st  8(R4),R3 ; Guardo el nuevo valor en la tabla Fibonacci 
			when X"00000024" => vProgramWord := assembleALURI(0, R4, R4, 4);  -- add R4,R4,4  ; Incremento el puntero de la tabla Fibonacci para que apunte al elemento siguiente
			when X"00000028" => vProgramWord := assembleALURI(1, R5, R5, 1);  -- sub R5,R5,1  ; Decremento el contador del bucle
			when X"0000002C" => vProgramWord := assembleJUMP(2#0110#, R5, 16#00000014#); -- jnz R5,mainloop ;  Si no es cero, salto al comienzo del bucle 
			-- stoploop:                                                                      ; Bucle de detenci�n de programa
			when X"00000030" => vProgramWord := assembleJUMP(2#0111#, R0, 16#00000030#); -- ja R0,stoploop ; Termino el programa con un bucle cerrado en esta posici�n
			when X"00000034" => vProgramWord := X"00000000";                  -- nop
			when X"00000038" => vProgramWord := X"00000000";                  -- nop
			when X"0000003C" => vProgramWord := X"00000000";                  -- nop
			when others => vProgramWord := X"00000000";
		end case;

		DATA_BUS <= std_logic_vector(vProgramWord) after DELAY;
	end process;

end architecture;

architecture A_PROGRAM_HAILSTONE of E_PROGRAM_MEMORY is
begin
	process(ADDR_BUS)
		variable vProgramWord : INSTRUCTION;

		variable vFullAddress : STD_LOGIC_VECTOR((DATA_BUS_SIZE - 1) downto 0);
	begin
		vFullAddress := std_logic_vector(to_unsigned(0, DATA_BUS_SIZE - ADDR_BUS_SIZE)) & ADDR_BUS & "00";

		--	--
		--
		-- La memoria de programa es generada mediante este case, que para cada posici�n de memoria devuelve 
		-- la palabra de instrucci�n correspondiente.
		-- El formato es f�cil de deducir: 
		-- "when <direcci�n_mem> => vProgramWord := <PALABRA DE INSTRUCCI�N A DEVOLVER>." 

		case vFullAddress is
			when X"00000000" => vProgramWord := assembleALURI(2#0000#, R5, R0, 1);       -- add R5,R0,1
			when X"00000004" => vProgramWord := assembleALURI(2#0000#, R4, R0, 0);       -- add R4,R0,result 
			-- numloop:  
			when X"00000008" => vProgramWord := assembleALURR(2#0000#, R2, R0, R5);      -- add R2,R0,R5
			when X"0000000C" => vProgramWord := assembleALURR(2#0000#, R3, R0, R2);      -- add R3,R0,R2
			-- hailloop: 
			when X"00000010" => vProgramWord := assembleALURI(2#0100#, R1, R2, 1);       -- and R1,R2,1
			when X"00000014" => vProgramWord := assembleJUMP(2#0110#, R1, 16#00000020#); -- bnez R1,odd      ; Si es impar, saltar a "odd"
			-- even:
			when X"00000018" => vProgramWord := assembleALURI(2#1101#, R2, R2, 1);       -- srl R2,R2,1
			when X"0000001C" => vProgramWord := assembleJUMP(2#0111#, R0, 16#0000002C#); -- j anynumber
			-- odd: 
			when X"00000020" => vProgramWord := assembleALURI(2#1100#, R1, R2, 1);       -- sll R1,R2,1
			when X"00000024" => vProgramWord := assembleALURR(2#0000#, R2, R1, R2);      -- add R2,R1,R2
			when X"00000028" => vProgramWord := assembleALURI(2#0000#, R2, R2, 1);       -- add R2,R2,1
			-- anynumber:
			when X"0000002C" => vProgramWord := assembleALURR(2#0001#, R1, R3, R2);      -- sub R1,R3,R2     ; Almaceno la diferencia entre el n�mero y el m�ximo en R1 
			when X"00000030" => vProgramWord := assembleALURI(2#1101#, R1, R1, 31);      -- srl R1,R1,31     ; Dejo s�lo el bit de signo
			when X"00000034" => vProgramWord := assembleJUMP(2#0001#, R1, 16#0000003C#); -- jz R1,skipnewmax ; El m�ximo es mayor que el n�mero de la secuencia
			when X"00000038" => vProgramWord := assembleALURR(2#0000#, R3, R0, R2);      -- add R3,R0,R2     ; Reemplazo el m�ximo
			-- skipnewmax: 
			when X"0000003C" => vProgramWord := assembleALURI(2#0001#, R1, R2, 1);       -- add R1,R2,-1
			when X"00000040" => vProgramWord := assembleJUMP(2#0110#, R1, 16#00000010#); -- jnz R1,hailloop
			when X"00000044" => vProgramWord := assembleSTORE(R4, R3, 0);                -- sw  R3,0(R4)
			when X"00000048" => vProgramWord := assembleALURI(2#0000#, R4, R4, 4);       -- add R4,R4,4
			when X"0000004C" => vProgramWord := assembleALURI(2#0000#, R5, R5, 1);       -- add R5,R5,1
			when X"00000050" => vProgramWord := assembleALURI(2#0001#, R1, R5, 101);     -- sub R1,R5,101
			when X"00000054" => vProgramWord := assembleJUMP(2#0110#, R1, 16#00000008#); -- bnez R1,numloop
			-- stoploop:
			when X"00000058" => vProgramWord := assembleJUMP(2#0111#, R0, 16#00000058#); -- ja stoploop      ; Termino el programa con un bucle cerrado en esta posici�n
			when X"0000005C" => vProgramWord := X"00000000";
			when X"00000060" => vProgramWord := X"00000000";
			when others      => vProgramWord := X"00000000";
		end case;

		DATA_BUS <= std_logic_vector(vProgramWord) after DELAY;
	end process;

end architecture;