--
-- ***************************************************
-- ** R32 , microprocesador de 32 bits experimental **
-- ***************************************************
-- *                                                 
-- * Arquitectura de Computadores II                 
-- * Facultad de Ingenier�a, UNLP                    
-- *                                                 
-- * Gerardo L. Puga (gerardo.puga@ing.unlp.edu.ar)  
-- * A�o 2014                            
-- *                     
-- ***
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.nano_pet_const_pkg.all;
use work.nano_pet_comp_pkg.all;
use work.all;

-- --------------------------------

entity E_DATA_MEMORY is
	generic(
		WORD_WIDTH : integer := DATA_BUS_SIZE;
		ADDR_WIDTH : integer := ADDR_BUS_SIZE;
		DELAY      : time    := TIME_DELAY_MEMORY
	);
	port(
		-- Entradas de control
		CLOCK          : in  STD_LOGIC; -- Activo en flanco ascendente
		RESET          : in  STD_LOGIC; -- Activo en nivel alto.
		-- Puerto de lectura
		READ_ADDR_BUS  : in  STD_LOGIC_VECTOR((ADDR_WIDTH - 1) downto LOWEST_ADDR_BIT);
		READ_DATA_BUS  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		-- Puerto de escritura
		WRITE_ENABLE   : in  STD_LOGIC;
		WRITE_ADDR_BUS : in  STD_LOGIC_VECTOR((ADDR_WIDTH - 1) downto LOWEST_ADDR_BIT);
		WRITE_DATA_BUS : in  STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);

		-- Puertos de monitoreo de estado de las primeras 8 posiciones de memoria
		MONITOR_DM_M0  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_DM_M1  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_DM_M2  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_DM_M3  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_DM_M4  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_DM_M5  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_DM_M6  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);
		MONITOR_DM_M7  : out STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0)
	);
end entity E_DATA_MEMORY;

-- --------------------------------
-- Definici�n de la Arquitectura --
-- --------------------------------

architecture A_DATA_MEMORY of E_DATA_MEMORY is
	constant RegisterCount : integer                                     := (2 ** ADDR_WIDTH);
	constant RegisterZero  : STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0) := (others => '0');

	type MEMORY_TYPE is array ((RegisterCount - 1) downto 0) of STD_LOGIC_VECTOR((WORD_WIDTH - 1) downto 0);

begin
	process(RESET, READ_ADDR_BUS, CLOCK)

		-- Creo el banco de registro pre-inicializado
		variable register_bank : MEMORY_TYPE := (others => RegisterZero);

	begin
		if (RESET = '1') then

			-- Pongo todos los registros a cero
			for regindex in (RegisterCount - 1) downto 0 loop
				register_bank(regIndex) := std_logic_vector(to_unsigned(0, WORD_WIDTH));
			end loop;

		elsif (CLOCK'event and CLOCK = '1') then

			-- Si hay un flanco de reloj, verifico si hay que realizar una escritura.
			if (WRITE_ENABLE = '1') then
				register_bank(to_integer(unsigned(WRITE_ADDR_BUS))) := WRITE_DATA_BUS;

				report "Contenido de la DATA RAM:" severity note;
				for i in 0 to 7 loop
					report integer'image(to_integer(unsigned(register_bank(i)))) severity note;
				end loop;
				report "Fin de contenido de la DATA RAM:" severity note;

			end if;

		end if;

		-- Independientemente de todo, actualizo los puertos de lectura
		READ_DATA_BUS <= register_bank(to_integer(unsigned(READ_ADDR_BUS))) after DELAY;

		-- --
		--
		-- Se�ales de monitoreo para ver el contenido de las primeras 8 posiciones de memoria
		MONITOR_DM_M0 <= register_bank(0);
		MONITOR_DM_M1 <= register_bank(1);
		MONITOR_DM_M2 <= register_bank(2);
		MONITOR_DM_M3 <= register_bank(3);
		MONITOR_DM_M4 <= register_bank(4);
		MONITOR_DM_M5 <= register_bank(5);
		MONITOR_DM_M6 <= register_bank(6);
		MONITOR_DM_M7 <= register_bank(7);
	end process;

end architecture A_DATA_MEMORY;
